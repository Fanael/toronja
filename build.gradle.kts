import kotlinx.atomicfu.plugin.gradle.AtomicFUPluginExtension
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.net.URI

buildscript { dependencies { classpath("org.jetbrains.kotlinx:atomicfu-gradle-plugin:0.17.1") } }

plugins {
    kotlin("jvm") version "1.6.10"
    id("io.gitlab.arturbosch.detekt") version "1.19.0"
    id("com.github.johnrengelman.shadow") version "7.1.2"
    id("org.owasp.dependencycheck") version "6.5.3"
}

apply { plugin("kotlinx-atomicfu") }

group = "invalid.domain"
version = "1.0-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11
java.targetCompatibility = JavaVersion.VERSION_11

repositories {
    mavenCentral()
    maven { url = URI("https://m2.dv8tion.net/releases") }
}

dependencies {
    fun dep(group: String, name: String, version: String): String = "$group:$name:$version"
    fun kotlinx(name: String, version: String): String = dep("org.jetbrains.kotlinx", "kotlinx-$name", version)

    implementation(kotlin("stdlib"))
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlinx("coroutines-core", "1.6.0"))
    implementation(kotlinx("coroutines-jdk8", "1.6.0"))
    implementation(dep("net.dv8tion", "JDA", "4.4.0_352")) { exclude(module = "opus-java") }
    constraints {
        implementation(dep("com.fasterxml.jackson.core", "jackson-databind", "2.13.2")) {
            because("Older versions have known vulnerabilities")
        }
    }
    implementation(dep("org.xerial", "sqlite-jdbc", "3.36.0.3"))
    implementation(dep("ch.qos.logback", "logback-classic", "1.2.11"))
    implementation(dep("org.slf4j", "slf4j-api", "1.7.36"))
    testImplementation(kotlin("test-junit5"))
    testImplementation(dep("io.mockk", "mockk", "1.12.3"))
    testImplementation(dep("org.junit.jupiter", "junit-jupiter-api", "5.8.2"))
    testRuntimeOnly(dep("org.junit.jupiter", "junit-jupiter-engine", "5.8.2"))
    detektPlugins(dep("io.gitlab.arturbosch.detekt", "detekt-formatting", "1.19.0"))
}

sourceSets {
    main { java.srcDir("src") }
    test { java.srcDir("test") }
}

with(extensions.getByName("atomicfu") as AtomicFUPluginExtension) {
    variant = "VH"
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = java.targetCompatibility.toString()
    kotlinOptions.freeCompilerArgs = listOf(
        "-progressive",
        "-Xassertions=jvm",
        "-Xlambdas=indy",
        "-Xopt-in=kotlin.RequiresOptIn"
    )
}

tasks.test { useJUnitPlatform() }

tasks.detekt {
    source = files("src").asFileTree
    config.setFrom(files("detekt.yml"))
    buildUponDefaultConfig = true
}

tasks.jar { manifest.attributes("Main-Class" to "toronja.MainKt") }

val testDatabasePath = "test-db.sqlite3"

val cleanTestDatabase = tasks.create("cleanTestDatabase") {
    doLast { delete(testDatabasePath, "$testDatabasePath-shm", "$testDatabasePath-wal") }
}
tasks.clean { dependsOn(cleanTestDatabase) }

val createTestDatabase = tasks.create("createTestDatabase") {
    dependsOn(cleanTestDatabase)
    doLast {
        file("schema.sql").inputStream().buffered().use {
            exec {
                commandLine("sqlite3", "-cmd", "PRAGMA synchronous = OFF", "-batch", testDatabasePath)
                standardInput = it
            }
        }
    }
}
tasks.processTestResources { dependsOn(createTestDatabase) }

CREATE TABLE enabled_modules (
    guild_id INTEGER NOT NULL,
    module_name TEXT NOT NULL,
    PRIMARY KEY(guild_id, module_name)
) WITHOUT ROWID;

CREATE TABLE guild_settings (
    guild_id INTEGER NOT NULL,
    name TEXT NOT NULL,
    value ANY NOT NULL,
    PRIMARY KEY(guild_id, name)
) WITHOUT ROWID;

CREATE TABLE saved_roles (
    guild_id INTEGER NOT NULL,
    user_id INTEGER NOT NULL,
    role_id INTEGER NOT NULL,
    PRIMARY KEY(guild_id, user_id, role_id)
) WITHOUT ROWID;

CREATE TABLE saved_nicknames (
    guild_id INTEGER NOT NULL,
    user_id INTEGER NOT NULL,
    nickname TEXT NOT NULL,
    PRIMARY KEY(guild_id, user_id)
) WITHOUT ROWID;

CREATE TABLE library_categories (
    category_id INTEGER NOT NULL PRIMARY KEY,
    guild_id INTEGER NOT NULL,
    category_name TEXT NOT NULL COLLATE NOCASE,
    UNIQUE(guild_id, category_name)
);

CREATE TABLE library_topics (
    category_id INTEGER NOT NULL REFERENCES library_categories(category_id) ON DELETE CASCADE,
    name TEXT NOT NULL COLLATE NOCASE,
    contents TEXT NOT NULL,
    PRIMARY KEY(category_id, name)
) WITHOUT ROWID;

CREATE TABLE assignable_role_categories (
    category_id INTEGER NOT NULL PRIMARY KEY,
    guild_id INTEGER NOT NULL,
    category_name TEXT NOT NULL COLLATE NOCASE,
    description TEXT NOT NULL,
    UNIQUE(guild_id, category_name)
);

CREATE TABLE assignable_roles (
    category_id INTEGER NOT NULL REFERENCES assignable_role_categories(category_id) ON DELETE CASCADE,
    role_name TEXT NOT NULL COLLATE NOCASE,
    role_id INTEGER NOT NULL,
    PRIMARY KEY(category_id, role_name)
) WITHOUT ROWID;

CREATE INDEX assignable_role_id ON assignable_roles(role_id);

CREATE TABLE message_events (
    event_id INTEGER NOT NULL PRIMARY KEY,
    guild_id INTEGER NOT NULL,
    channel_id INTEGER NOT NULL,
    message_id INTEGER NOT NULL,
    event_type INTEGER NOT NULL,
    event_timestamp INTEGER NOT NULL
);

-- Keep the hardcoded constant in sync with MessageEvent.Type.CREATE
CREATE INDEX message_creations ON message_events(guild_id, channel_id, message_id) WHERE event_type = 0;
CREATE INDEX message_ids ON message_events(guild_id, message_id);

CREATE TABLE message_event_contents (
    event_id INTEGER NOT NULL PRIMARY KEY REFERENCES message_events(event_id) ON DELETE CASCADE,
    author_id INTEGER NOT NULL,
    author_tag TEXT NOT NULL,
    message_text TEXT NOT NULL
);

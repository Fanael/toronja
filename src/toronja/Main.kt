// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja

import ch.qos.logback.classic.LoggerContext
import ch.qos.logback.classic.joran.JoranConfigurator
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.time.withTimeoutOrNull
import net.dv8tion.jda.api.JDABuilder
import net.dv8tion.jda.api.events.ShutdownEvent
import net.dv8tion.jda.api.requests.GatewayIntent
import net.dv8tion.jda.api.utils.ChunkingFilter
import net.dv8tion.jda.api.utils.MemberCachePolicy
import net.dv8tion.jda.api.utils.SessionControllerAdapter
import net.dv8tion.jda.api.utils.cache.CacheFlag
import org.slf4j.LoggerFactory
import toronja.bot.Bot
import toronja.bot.db.ConnectionPool
import toronja.bot.events.EventListener
import toronja.utils.makeLogger
import java.nio.file.Files
import java.nio.file.Path
import java.time.Duration
import java.util.EnumSet
import java.util.concurrent.CountDownLatch
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import java.util.concurrent.Semaphore
import java.util.concurrent.SynchronousQueue
import java.util.concurrent.ThreadFactory
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicLong
import kotlin.coroutines.CoroutineContext
import kotlin.system.exitProcess

fun main() {
    initializeLogger()
    val eventThreadPool = createEventThreadPool()
    val throttler = ThrottlingExecutor(eventThreadPool)
    val coroutineScope = createCoroutineScope(throttler)
    val connectionPool = ConnectionPool(DATABASE_PATH, coroutineScope)
    val shutdownHandler = ShutdownHandler(eventThreadPool, connectionPool, coroutineScope)
    shutdownHandler.register()

    val listener = EventListener(throttler)
    Bot(connectionPool, listener, coroutineScope)

    try {
        val tokens = loadTokens()
        val sessionController = SessionControllerAdapter()
        val requiredIntents = EnumSet.of(
            GatewayIntent.GUILD_MEMBERS,
            GatewayIntent.GUILD_BANS,
            GatewayIntent.GUILD_INVITES,
            GatewayIntent.GUILD_MESSAGES
        )
        for (token in tokens) {
            JDABuilder.createDefault(token)
                .setEnabledIntents(requiredIntents)
                .setMemberCachePolicy(MemberCachePolicy.ALL)
                .setChunkingFilter(ChunkingFilter.ALL)
                .disableCache(CacheFlag.ACTIVITY, CacheFlag.CLIENT_STATUS, CacheFlag.EMOTE, CacheFlag.VOICE_STATE)
                .setBulkDeleteSplittingEnabled(false)
                .setSessionController(sessionController)
                .setEventManager(listener)
                .build()
        }
        shutdownHandler.addJdaShutdownLatch(listener, tokens.size)
    } catch (@Suppress("TooGenericExceptionCaught") e: Exception) {
        makeLogger("Main").error(e) { "Error during startup" }
        eventThreadPool.shutdownNow()
        connectionPool.close()
        exitProcess(1)
    }
}

private const val DATABASE_PATH = "db.sqlite3"
private const val TOKEN_FILE_PATH = "conf/bot.token"
private const val LOG_CONFIG_PATH = "conf/logback.xml"
private const val SHUTDOWN_WAIT_MILLIS = 7_500L
private const val THREAD_KEEP_ALIVE_SECONDS = 30L
private const val THROTTLE_PAUSE_MILLIS = 5L

private fun createEventThreadPool(): ThreadPoolExecutor {
    val logger = makeLogger("EventThreadPool")
    val uncaughtExceptionHandler = Thread.UncaughtExceptionHandler { _, exception ->
        logger.error(exception) { "Uncaught exception in event handling thread" }
    }
    val threadIdGenerator = AtomicLong(0)
    val threadFactory = ThreadFactory { runnable ->
        val thread = Executors.defaultThreadFactory().newThread(runnable)
        val threadName = "EventThread-${threadIdGenerator.getAndIncrement().toULong()}"
        logger.info { "Spawning new thread named '$threadName' in event pool" }
        thread.name = threadName
        thread.uncaughtExceptionHandler = uncaughtExceptionHandler
        thread
    }
    return ThreadPoolExecutor(
        // We're doing a mix of computation and I/O, so it's useful to have
        // more threads than CPUs.
        2 * Runtime.getRuntime().availableProcessors() + 2,
        Int.MAX_VALUE,
        THREAD_KEEP_ALIVE_SECONDS,
        TimeUnit.SECONDS,
        SynchronousQueue(),
        threadFactory
    ).also { it.prestartAllCoreThreads() }
}

private class ThrottlingExecutor(private val threadPool: ThreadPoolExecutor) : Executor {
    override fun execute(command: Runnable) {
        var needRelease = semaphore.tryAcquire(THROTTLE_PAUSE_MILLIS, TimeUnit.MILLISECONDS)
        try {
            threadPool.execute(command.wrapWithReleaseIf(needRelease))
            // Now the scheduled runnable will take care of releasing.
            needRelease = false
        } finally {
            if (needRelease) semaphore.release()
        }
    }

    private fun Runnable.wrapWithReleaseIf(flag: Boolean): Runnable {
        if (!flag) return this
        val inner = this
        // Capture only the semaphore, avoid holding a reference to the whole
        // executor in the wrapped runnable.
        val sem = semaphore
        return Runnable {
            try {
                inner.run()
            } finally {
                sem.release()
            }
        }
    }

    private val semaphore = Semaphore(threadPool.corePoolSize)
}

// We want to always dispatch coroutines to our thread pool, but the dispatcher
// created by Executor.asCoroutineDispatcher doesn't guarantee that, so we're
// providing our own dispatcher.
private fun createCoroutineScope(executor: Executor): CoroutineScope =
    CoroutineScope(SupervisorJob() + object : CoroutineDispatcher() {
        override fun dispatch(context: CoroutineContext, block: Runnable): Unit = executor.execute(block)
    })

private class ShutdownHandler(
    private val eventThreadPool: ThreadPoolExecutor,
    private val connectionPool: ConnectionPool,
    private val coroutineScope: CoroutineScope
) : Runnable {
    fun register() {
        Runtime.getRuntime().addShutdownHook(Thread(this).apply { name = "ShutdownHandler" })
    }

    fun addJdaShutdownLatch(listener: EventListener, latchCount: Int) {
        check(latch == null) { "Shutdown handler already has a latch" }
        val newLatch = CountDownLatch(latchCount)
        listener.subscribe<ShutdownEvent> { newLatch.countDown() }
        latch = newLatch
    }

    override fun run() {
        waitForJdaShutdown()
        cancelCoroutineScope()
        shutdownThreadPool()
        closeConnectionPool()
        logger.info { "Shutdown complete" }
    }

    private fun waitForJdaShutdown() {
        val latch = this.latch ?: return
        logger.info { "Waiting for JDA instances to shut down" }
        if (!latch.await(SHUTDOWN_WAIT_MILLIS, TimeUnit.MILLISECONDS)) {
            logger.warn { "Waiting for JDA shutdown timed out" }
        }
    }

    private fun cancelCoroutineScope() {
        logger.info { "Cancelling coroutine scope" }
        coroutineScope.cancel(CancellationException("Shutting down"))
        runBlocking(NonCancellable) {
            val job = coroutineScope.coroutineContext[Job]!!
            when (withTimeoutOrNull(Duration.ofMillis(SHUTDOWN_WAIT_MILLIS)) { job.join() }) {
                null -> logger.warn { "Coroutine cancellation timed out" }
                else -> logger.info { "Coroutine scope cancelled successfully" }
            }
        }
    }

    private fun shutdownThreadPool() {
        logger.info { "Shutting down the event pool" }
        eventThreadPool.shutdown()
        if (eventThreadPool.awaitTermination(SHUTDOWN_WAIT_MILLIS, TimeUnit.MILLISECONDS)) {
            logger.info { "Pool shut down successfully" }
        } else {
            logger.warn { "Pool shutdown timed out, forcing anyway" }
            eventThreadPool.shutdownNow()
        }
    }

    private fun closeConnectionPool() {
        logger.info { "Closing the database connection pool" }
        connectionPool.close()
    }

    private val logger = makeLogger<ShutdownHandler>()

    @Volatile
    private var latch: CountDownLatch? = null
}

private fun loadTokens(): List<String> =
    Files.newBufferedReader(Path.of(TOKEN_FILE_PATH), Charsets.UTF_8).use { reader ->
        reader.lineSequence().filter(String::isNotBlank).map(String::trim).toList()
    }

private fun initializeLogger() {
    val context = LoggerFactory.getILoggerFactory() as LoggerContext
    val configurator = JoranConfigurator()
    configurator.context = context
    context.reset()
    configurator.doConfigure(LOG_CONFIG_PATH)
}

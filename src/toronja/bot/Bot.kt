// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot

import kotlinx.coroutines.CoroutineScope
import toronja.bot.commands.CommandProcessor
import toronja.bot.db.ConnectionPool
import toronja.bot.events.EventListener
import toronja.bot.events.EventSubscriber
import toronja.bot.modules.Greeter
import toronja.bot.modules.InviteTracker
import toronja.bot.modules.Library
import toronja.bot.modules.NicknameRestorer
import toronja.bot.modules.Presence
import toronja.bot.modules.RoleCategorizer
import toronja.bot.modules.RoleRestorer
import toronja.bot.modules.base.Module
import toronja.bot.modules.base.ToggleableModule
import toronja.bot.modules.message.MessageLog
import toronja.bot.settings.GuildSettings
import toronja.utils.collections.persistent.BTreeMap

@Suppress("Unused", "MemberVisibilityCanBePrivate")
internal class Bot(
    connectionPool: ConnectionPool, eventListener: EventListener, val coroutineScope: CoroutineScope
) : Module.AlwaysEnabled() {
    // Use a B-tree map here to have the modules sorted by name.
    val moduleNameMap: BTreeMap<String, ToggleableModule>

    init {
        Presence.registerEvents(eventListener)
    }

    val guildSettings = GuildSettings(connectionPool, eventListener)
    val commandProcessor = CommandProcessor(this)
    val roleRestorer = RoleRestorer(connectionPool, coroutineScope)
    val nicknameRestorer = NicknameRestorer(connectionPool, coroutineScope)
    val greeter = Greeter(guildSettings, roleRestorer)
    val library = Library(connectionPool)
    val roleCategorizer = RoleCategorizer(connectionPool, coroutineScope)
    val messageLog = MessageLog(guildSettings, connectionPool, coroutineScope)
    val inviteTracker = InviteTracker(guildSettings, coroutineScope)

    init {
        var map = BTreeMap<String, ToggleableModule>()
        for (field in javaClass.declaredFields) {
            val value = field.get(this)
            (value as? ToggleableModule)?.let { map += Pair(it.moduleName, it) }
            (value as? EventSubscriber)?.registerEvents(eventListener)
        }
        moduleNameMap = map
    }
}

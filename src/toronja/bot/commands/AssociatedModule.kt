// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands

import toronja.bot.Bot
import toronja.bot.modules.base.Module

internal fun interface AssociatedModule<M> {
    operator fun invoke(bot: Bot): M

    // NB: these functions are represented as objects to ensure they're
    // identity comparable.

    object None : AssociatedModule<Module.None> {
        override fun invoke(bot: Bot): Module.None = Module.None
    }

    object WholeBot : AssociatedModule<Bot> {
        override fun invoke(bot: Bot): Bot = bot
    }

    object GuildSettings : AssociatedModule<toronja.bot.settings.GuildSettings> {
        override fun invoke(bot: Bot): toronja.bot.settings.GuildSettings = bot.guildSettings
    }

    object Library : AssociatedModule<toronja.bot.modules.Library> {
        override fun invoke(bot: Bot): toronja.bot.modules.Library = bot.library
    }

    object RoleCategorizer : AssociatedModule<toronja.bot.modules.RoleCategorizer> {
        override fun invoke(bot: Bot): toronja.bot.modules.RoleCategorizer = bot.roleCategorizer
    }

    object MessageLog : AssociatedModule<toronja.bot.modules.message.MessageLog> {
        override fun invoke(bot: Bot): toronja.bot.modules.message.MessageLog = bot.messageLog
    }
}

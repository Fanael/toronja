// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands

import net.dv8tion.jda.api.MessageBuilder
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.TextChannel
import toronja.bot.Bot
import toronja.bot.modules.base.Module
import toronja.utils.Snowflake

internal typealias CommandParser<M> = Command.ParsingContext<M>.() -> Command.Parsed<M>

internal class Command<M : Module>(
    val isMemberAllowed: MemberFilter,
    val getAssociatedModule: AssociatedModule<M>,
    val parse: CommandParser<M>,
    // Short description, displayed when a command parsing error occurs.
    val shortDescription: String,
    // Long description, displayed by the help command.
    // The magic string \{COMMAND} will be replaced with the bot prefix and
    // the command name.
    val longDescription: String
) {
    fun isEnabledIn(guild: Snowflake<Guild>, bot: Bot): Boolean = getAssociatedModule(bot).isEnabledIn(guild)

    sealed class Parsed<in M : Module> {
        class Error(val message: String) : Parsed<Module>()

        class Action<in M : Module>(private val block: suspend ActionContext<M>.() -> Unit) : Parsed<M>() {
            suspend operator fun invoke(context: ActionContext<M>): Unit = context.block()
        }
    }

    @ContextMarker
    class ParsingContext<M : Module>(val module: M, val message: Message, val extractor: FieldExtractor) {
        // Neither method is strictly necessary, but they make returns just
        // look better in the source.
        fun action(block: suspend ActionContext<M>.() -> Unit): Parsed.Action<M> = Parsed.Action(block)

        fun error(errorMessage: String): Parsed.Error = Parsed.Error(errorMessage)

        fun String.stripMentions(): String = stripMentionsFrom(this, message.guild)
    }

    @ContextMarker
    class ActionContext<out M : Module>(val module: M, val channel: TextChannel, val authorMention: String) {
        fun String.stripMentions(): String = stripMentionsFrom(this, channel.guild)
    }

    @Target(AnnotationTarget.CLASS)
    @Retention(AnnotationRetention.SOURCE)
    @DslMarker
    private annotation class ContextMarker
}

internal fun stripMentionsFrom(string: String, guild: Guild): String =
    MessageBuilder(string).stripMentions(guild).stringBuilder.toString()

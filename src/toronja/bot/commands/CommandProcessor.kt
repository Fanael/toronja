// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands

import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.plus
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent
import toronja.bot.Bot
import toronja.bot.commands.library.LibraryAccess
import toronja.bot.commands.library.LibraryCategoryDefine
import toronja.bot.commands.library.LibraryCategoryRemove
import toronja.bot.commands.library.LibraryCategoryRename
import toronja.bot.commands.library.LibraryTopicRemove
import toronja.bot.commands.library.LibraryTopicRename
import toronja.bot.commands.library.LibraryTopicSet
import toronja.bot.commands.log.MessageLogGet
import toronja.bot.commands.misc.DumpAcl
import toronja.bot.commands.misc.Help
import toronja.bot.commands.misc.ModHelp
import toronja.bot.commands.misc.Ping
import toronja.bot.commands.misc.Source
import toronja.bot.commands.module.ModuleDisable
import toronja.bot.commands.module.ModuleEnable
import toronja.bot.commands.module.ModuleList
import toronja.bot.commands.role.RoleAdd
import toronja.bot.commands.role.RoleCategoryDefine
import toronja.bot.commands.role.RoleCategoryRemove
import toronja.bot.commands.role.RoleCategoryRename
import toronja.bot.commands.role.RoleForget
import toronja.bot.commands.role.RoleImport
import toronja.bot.commands.role.RoleList
import toronja.bot.commands.role.RoleToggle
import toronja.bot.commands.role.RoleToggleSimple
import toronja.bot.commands.setting.SettingAccess
import toronja.bot.commands.setting.SettingDescribe
import toronja.bot.commands.setting.SettingList
import toronja.bot.commands.setting.SettingUnset
import toronja.bot.events.EventListener
import toronja.bot.events.EventSubscriber
import toronja.bot.modules.base.Module
import toronja.bot.modules.base.ToggleableModule
import toronja.bot.settings.CommandPrefix
import toronja.bot.settings.InhibitUnknownCommandMessage
import toronja.utils.Snowflake
import toronja.utils.collections.persistent.BTreeMap
import toronja.utils.idTyped
import toronja.utils.launchUndispatched
import toronja.utils.makeLogger

internal class CommandProcessor(private val bot: Bot) : EventSubscriber {
    override fun registerEvents(listener: EventListener) {
        listener.subscribe<GuildMessageReceivedEvent> { processMessage(it.message) }
    }

    fun prefixIn(guild: Snowflake<Guild>): String = bot.guildSettings[guild, CommandPrefix] ?: FALLBACK_PREFIX

    private fun processMessage(message: Message) {
        // Ignore all bot messages.
        if (message.author.isBot) return
        val guild = message.textChannel.guild.idTyped
        val guildPrefix = prefixIn(guild)
        val messageContent = message.contentRaw
        if (!messageContent.startsWith(guildPrefix)) return
        val fieldExtractor = FieldExtractor(messageContent.substring(guildPrefix.length))
        val commandName = fieldExtractor.getNextField() ?: return
        when (val command = commandMap[commandName]) {
            null -> processUnknownCommand(guild, message, commandName)
            else -> executeCommand(message, command, fieldExtractor)
        }
    }

    private fun processUnknownCommand(guild: Snowflake<Guild>, message: Message, commandName: String) {
        if (bot.guildSettings[guild, InhibitUnknownCommandMessage] != null) {
            // The error message is inhibited in this server.
            return
        }
        if (commandName.codePoints().noneMatch { Character.isLetter(it) }) {
            // If the alleged command name doesn't contain any letters, it's
            // unlikely to be intended to be a command in the first place.
            return
        }
        val channel = message.textChannel
        val authorMention = message.author.asMention
        val response = "$authorMention, unknown command `${stripMentionsFrom(commandName, channel.guild)}`," +
            " try `$HELP_COMMAND_NAME` to get the list of known commands."
        channel.sendMessage(response).queue()
    }

    private fun <M : Module> executeCommand(message: Message, command: Command<M>, fieldExtractor: FieldExtractor) {
        val channel = message.textChannel
        val author = message.member!!
        val authorMention = author.asMention
        try {
            if (!command.isMemberAllowed(author)) {
                channel.sendMessage("$authorMention, you lack the required permissions for this action.").queue()
                return
            }
            val module = command.getAssociatedModule(bot)
            if (!module.isEnabledIn(channel.guild.idTyped)) {
                channel.sendMessage("$authorMention, ${getModuleDisabledMessage(module)}").queue()
                return
            }
            when (val parsedCommand = command.parse(Command.ParsingContext(module, message, fieldExtractor))) {
                is Command.Parsed.Action -> {
                    val actionContext = Command.ActionContext(module, channel, authorMention)
                    makeCoroutineScope(channel, authorMention).launchUndispatched { parsedCommand(actionContext) }
                }
                is Command.Parsed.Error -> {
                    val prefix = prefixIn(channel.guild.idTyped)
                    val response = "$authorMention, error: ${parsedCommand.message}" +
                        "\n\nShort description of the failed command: ${command.shortDescription}" +
                        "\n\nFor more details, try `$prefix$HELP_COMMAND_NAME <command name>`."
                    channel.sendMessage(response).queue()
                }
            }
        } catch (@Suppress("TooGenericExceptionCaught") e: Exception) {
            sendErrorMessage(e, authorMention, channel)
        }
    }

    private fun makeCoroutineScope(channel: TextChannel, authorMention: String): CoroutineScope =
        bot.coroutineScope + CoroutineExceptionHandler { _, e ->
            if (e !is Exception) throw e
            sendErrorMessage(e, authorMention, channel)
        }

    companion object {
        const val HELP_COMMAND_NAME = "help"
        const val MOD_HELP_COMMAND_NAME = "mod-help"
        const val SETTING_DESCRIBE_COMMAND_NAME = "setting-describe"

        val commands = arrayOf<Pair<String, Command<*>>>(
            // Miscellaneous commands
            HELP_COMMAND_NAME to Help,
            MOD_HELP_COMMAND_NAME to ModHelp,
            "ping" to Ping,
            "source" to Source,
            "dump-acl" to DumpAcl,
            // Module management
            "module-list" to ModuleList,
            "module-disable" to ModuleDisable,
            "module-enable" to ModuleEnable,
            // Setting management
            "setting" to SettingAccess,
            "setting-unset" to SettingUnset,
            "setting-list" to SettingList,
            SETTING_DESCRIBE_COMMAND_NAME to SettingDescribe,
            // Library commands
            "library-define-category" to LibraryCategoryDefine,
            "library-remove-category" to LibraryCategoryRemove,
            "library-rename-category" to LibraryCategoryRename,
            "library-set-topic" to LibraryTopicSet,
            "library-remove-topic" to LibraryTopicRemove,
            "library-rename-topic" to LibraryTopicRename,
            "library" to LibraryAccess,
            // Role commands
            "role-define-category" to RoleCategoryDefine,
            "role-rename-category" to RoleCategoryRename,
            "role-remove-category" to RoleCategoryRemove,
            "role-import" to RoleImport,
            "role-forget" to RoleForget,
            "role-add" to RoleAdd,
            "role-toggle" to RoleToggle,
            "role" to RoleToggleSimple,
            "rank" to RoleToggleSimple,
            "r" to RoleToggleSimple,
            "roles" to RoleList,
            "ranks" to RoleList,
            "category" to RoleList,
            // Message log commands
            "message-log-get" to MessageLogGet
        )

        val commandMap = commands.fold(BTreeMap(), BTreeMap<String, Command<*>>::plus)
    }
}

private fun sendErrorMessage(exception: Exception, authorMention: String, channel: TextChannel) {
    // Do not break thread interruption and coroutine cancellation.
    if (exception is InterruptedException || exception is CancellationException) throw exception

    logger.error(exception) { "Command processing error" }
    val errorMessage = stripMentionsFrom(exception.message ?: exception.javaClass.name, channel.guild)
    channel.sendMessage("$authorMention, error processing command: $errorMessage").queue()
}

private fun getModuleDisabledMessage(module: Module): String = when (module) {
    is ToggleableModule -> "the module `${module.moduleName}` is disabled on this server."
    else -> "this command is disabled on this server."
}

private const val FALLBACK_PREFIX = "toro!"

private val logger = makeLogger<CommandProcessor>()

// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands

internal class FieldExtractor(private val chars: CharSequence) {
    fun getNextField(): String? {
        skipWhitespace()
        val fieldStart = index
        skipNonWhitespace()
        val fieldEnd = index
        return when (fieldStart) {
            fieldEnd -> null
            else -> chars.substring(fieldStart, fieldEnd)
        }
    }

    val rest: String get() = chars.substring(index).trim()

    private fun skipWhitespace(): Unit = skipWhile(Char::isWhitespace)
    private fun skipNonWhitespace(): Unit = skipWhile { !it.isWhitespace() }

    private inline fun skipWhile(predicate: (Char) -> Boolean) {
        while (index < chars.length && predicate(chars[index])) {
            index += 1
        }
    }

    private var index = 0
}

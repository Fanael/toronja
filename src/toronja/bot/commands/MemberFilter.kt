// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands

import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.entities.Member

internal fun interface MemberFilter {
    operator fun invoke(member: Member): Boolean

    // NB: these functions are represented as objects to ensure they're
    // identity comparable.

    object Anyone : MemberFilter {
        override fun invoke(member: Member): Boolean = true
    }

    object CanManageServer : MemberFilter {
        override fun invoke(member: Member): Boolean = member.hasPermission(Permission.MANAGE_SERVER)
    }

    object CanManageRoles : MemberFilter {
        override fun invoke(member: Member): Boolean = member.hasPermission(Permission.MANAGE_ROLES)
    }

    object CanManageMessagesOrChannel : MemberFilter {
        override fun invoke(member: Member): Boolean = hasAny(member, permissionMask)

        private val permissionMask = Permission.MESSAGE_MANAGE.rawValue or Permission.MANAGE_CHANNEL.rawValue
    }

    object CanManageRolesOrChannel : MemberFilter {
        override fun invoke(member: Member): Boolean = hasAny(member, permissionMask)

        private val permissionMask = Permission.MANAGE_ROLES.rawValue or Permission.MANAGE_CHANNEL.rawValue
    }

    object IsModerator : MemberFilter {
        override fun invoke(member: Member): Boolean = hasAny(member, permissionMask)

        private val permissionMask = Permission.BAN_MEMBERS.rawValue or
            Permission.MANAGE_SERVER.rawValue or
            Permission.MANAGE_CHANNEL.rawValue or
            Permission.MANAGE_ROLES.rawValue
    }
}

private fun hasAny(member: Member, permissionMask: Long): Boolean =
    (Permission.getRaw(member.permissions) and permissionMask) != 0L

// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.library

import net.dv8tion.jda.api.entities.Guild
import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.MemberFilter
import toronja.bot.modules.Library
import toronja.utils.Snowflake
import toronja.utils.idTyped

internal val LibraryAccess = Command(
    MemberFilter.Anyone,
    AssociatedModule.Library,
    Command.ParsingContext<Library>::parse,
    shortDescription = "View or list library topics." +
        "\nWith no arguments, list all library categories;" +
        " with one argument, list all topics in the given category;" +
        " with two arguments, show the given topic in the given category.",
    longDescription = """View or list library topics.

With no arguments, list all library categories.
With a single argument `<category-name>`, list all topics in the given category.
With two arguments `<category-name> <topic-name>`, show the library entry of the given topic.

Example of use:
 - `\{COMMAND}`: list all categories.
 - `\{COMMAND} foo`: list all topics in category `foo`.
 - `\{COMMAND} foo bar`: show the entry `bar` in category `foo`."""
)

private fun Command.ParsingContext<Library>.parse(): Command.Parsed<Library> {
    val guild = message.guild.idTyped
    val categoryName = extractor.getNextField() ?: return action { listCategories(guild) }
    val topicName = extractor.getNextField() ?: return action { listTopics(guild, categoryName) }
    if (extractor.getNextField() != null) return error("Superfluous arguments passed.")
    return action { showTopic(guild, categoryName, topicName) }
}

private fun Command.ActionContext<Library>.listCategories(guild: Snowflake<Guild>) {
    val categoryNames = module.getCategoryNames(guild).joinToString(separator = "\n")
    channel.sendMessage("$authorMention, list of library categories: ```\n$categoryNames```").queue()
}

private fun Command.ActionContext<Library>.listTopics(guild: Snowflake<Guild>, categoryName: String) {
    val response = when (val result = module.getTopicNamesInCategory(guild, categoryName)) {
        is Library.TopicResult.Result -> {
            val topicNames = result.value.joinToString(separator = "\n")
            "$authorMention, list of topics in category `$categoryName`: ```\n$topicNames```"
        }
        else -> "$authorMention, library category `${categoryName.stripMentions()}` not found."
    }
    channel.sendMessage(response).queue()
}

private fun Command.ActionContext<Library>.showTopic(guild: Snowflake<Guild>, categoryName: String, topicName: String) {
    val response: String = when (val result = module.getTopic(guild, categoryName, topicName)) {
        is Library.TopicResult.Result -> result.value
            ?: "$authorMention, topic `${topicName.stripMentions()}` in category `$categoryName` not found."
        is Library.TopicResult.NoSuchCategory ->
            "$authorMention, library category `${categoryName.stripMentions()}` not found."
    }
    channel.sendMessage(response).queue()
}

// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.library

import net.dv8tion.jda.api.entities.Guild
import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.CommandParser
import toronja.bot.commands.MemberFilter
import toronja.bot.modules.Library
import toronja.utils.Snowflake
import toronja.utils.idTyped

internal val LibraryCategoryDefine = Command(
    MemberFilter.IsModerator,
    AssociatedModule.Library,
    makeParser(Command.ActionContext<Library>::defineCategory),
    shortDescription = "Create new library category." +
        "\nThis command takes one argument, the name of the category to create.",
    longDescription = """Create new library category.

This command accepts a single argument `<category-name>`, which is the name of the category to create.
Only one category with a given name can exist on the server.

Example of use:
 - `\{COMMAND} foo`: create a library category named `foo`."""
)

internal val LibraryCategoryRemove = Command(
    MemberFilter.IsModerator,
    AssociatedModule.Library,
    makeParser(Command.ActionContext<Library>::removeCategory),
    shortDescription = "Remove an existing library category." +
        "\nThis command takes one argument, the name of the category to remove.",
    longDescription = """Remove an existing library category.

This command accepts a single argument `<category-name>`, which is the name of the category to remove.
All topics in that category will be irrecoverably lost!

Example of use:
 - `\{COMMAND} foo`: remove the library category named `foo`."""
)

private fun makeParser(
    function: suspend Command.ActionContext<Library>.(Snowflake<Guild>, String) -> Unit
): CommandParser<Library> = parser@{
    val categoryName = extractor.getNextField() ?: return@parser error("Category name is required.")
    if (extractor.getNextField() != null) return@parser error("This command takes exactly one argument.")
    val guild = message.guild.idTyped
    action { function(guild, categoryName) }
}

private suspend fun Command.ActionContext<Library>.defineCategory(guild: Snowflake<Guild>, categoryName: String) {
    val response = if (module.defineCategory(guild, categoryName)) {
        "$authorMention, library category `$categoryName` created successfully."
    } else {
        "$authorMention, library category `$categoryName` already exists."
    }
    channel.sendMessage(response).queue()
}

private suspend fun Command.ActionContext<Library>.removeCategory(guild: Snowflake<Guild>, categoryName: String) {
    val response = if (module.removeCategory(guild, categoryName)) {
        "$authorMention, library category `$categoryName` removed successfully."
    } else {
        "$authorMention, library category `$categoryName` was not found."
    }
    channel.sendMessage(response).queue()
}

// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.library

import net.dv8tion.jda.api.entities.Guild
import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.MemberFilter
import toronja.bot.modules.Library
import toronja.utils.Snowflake
import toronja.utils.idTyped

internal val LibraryCategoryRename = Command(
    MemberFilter.IsModerator,
    AssociatedModule.Library,
    Command.ParsingContext<Library>::parse,
    shortDescription = "Rename an existing library category." +
        "\nThis command takes two arguments, the current name and the new name of the category.",
    longDescription = """Rename an existing library category.

This command accepts two arguments `<category-name> <new-name>`, where `<category-name>` is the current
name of the category to rename and `<new-name>` is the new name of that category.
Only one category with a given name can exist on the server.

Example of use:
 - `\{COMMAND} foo bar`: rename the library category `foo` to `bar`."""
)

private fun Command.ParsingContext<Library>.parse(): Command.Parsed<Library> {
    val currentName = extractor.getNextField() ?: return error("Category name is required.")
    val newName = extractor.getNextField() ?: return error("New category name is required.")
    if (extractor.getNextField() != null) return error("This command takes exactly two arguments.")
    val guild = message.guild.idTyped
    return action { renameCategory(guild, currentName, newName) }
}

private suspend fun Command.ActionContext<Library>.renameCategory(
    guild: Snowflake<Guild>, categoryName: String, newName: String
) {
    val response = if (module.renameCategory(guild, categoryName, newName)) {
        "$authorMention, library category `$categoryName` renamed successfully to `$newName`."
    } else {
        "$authorMention, library category `$categoryName` not found."
    }
    channel.sendMessage(response).queue()
}

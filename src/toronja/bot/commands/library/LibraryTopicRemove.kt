// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.library

import net.dv8tion.jda.api.entities.Guild
import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.MemberFilter
import toronja.bot.modules.Library
import toronja.utils.Snowflake
import toronja.utils.idTyped

internal val LibraryTopicRemove = Command(
    MemberFilter.IsModerator,
    AssociatedModule.Library,
    Command.ParsingContext<Library>::parse,
    shortDescription = "Remove a library topic." +
        "\nThis command takes two arguments, the category name and the name of the topic to remove.",
    longDescription = """Remove a library topic.

This command accepts two arguments `<category-name> <topic-name>`.
`<category-name>` is the category in which to remove the given topic.
`<topic-name>` is the name of the topic to remove.

Example of use:
 - `\{COMMAND} foo test`: remove the topic `test` in category `foo`."""
)

private fun Command.ParsingContext<Library>.parse(): Command.Parsed<Library> {
    val categoryName = extractor.getNextField() ?: return error("Category name is required.")
    val topicName = extractor.getNextField() ?: return error("Topic name is required.")
    if (extractor.getNextField() != null) return error("This command takes exactly two arguments.")
    val guild = message.guild.idTyped
    return action { removeTopic(guild, categoryName, topicName) }
}

private suspend fun Command.ActionContext<Library>.removeTopic(
    guild: Snowflake<Guild>, categoryName: String, topicName: String
) {
    val response = when (val result = module.removeTopic(guild, categoryName, topicName)) {
        is Library.TopicResult.Result -> if (result.value) {
            "$authorMention, library topic `$topicName` in category `$categoryName` removed successfully."
        } else {
            "$authorMention, library topic `$topicName` was not found in category `$categoryName`."
        }
        else -> "$authorMention, library category `$categoryName` not found."
    }
    channel.sendMessage(response).queue()
}

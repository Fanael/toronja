// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.library

import net.dv8tion.jda.api.entities.Guild
import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.MemberFilter
import toronja.bot.modules.Library
import toronja.utils.Snowflake
import toronja.utils.idTyped

internal val LibraryTopicRename = Command(
    MemberFilter.IsModerator,
    AssociatedModule.Library,
    Command.ParsingContext<Library>::parse,
    shortDescription = "Rename a library topic." +
        "\nThis command takes three arguments: category name, current topic name and new topic name.",
    longDescription = """Rename a library topic.

This command accepts three arguments `<category-name> <topic-name> <new-name>`.
`<category-name>` is the library category in which to rename a topic.
`<topic-name>` is the *current* name of the topic to rename.
`<new-name>` is the *new* name of the topic.

Example of use:
 - `\{COMMAND} foo test blah`: rename the topic `test` to `blah` in category `foo`."""
)

private fun Command.ParsingContext<Library>.parse(): Command.Parsed<Library> {
    val categoryName = extractor.getNextField() ?: return error("Category name is required.")
    val topicName = extractor.getNextField() ?: return error("Current topic name is required.")
    val newName = extractor.getNextField() ?: return error("New topic name is required.")
    if (extractor.getNextField() != null) return error("This command takes exactly three arguments.")
    val guild = message.guild.idTyped
    return action { renameTopic(guild, categoryName, topicName, newName) }
}

private suspend fun Command.ActionContext<Library>.renameTopic(
    guild: Snowflake<Guild>, categoryName: String, topicName: String, newName: String
) {
    val response = when (val result = module.renameTopic(guild, categoryName, topicName, newName)) {
        is Library.TopicResult.Result -> if (result.value) {
            "$authorMention, library topic `$topicName` in category `$categoryName` renamed successfully to `$newName`."
        } else {
            "$authorMention, topic `$topicName` in category `$categoryName` not found."
        }
        is Library.TopicResult.NoSuchCategory -> "$authorMention, library category `$categoryName` not found."
    }
    channel.sendMessage(response).queue()
}

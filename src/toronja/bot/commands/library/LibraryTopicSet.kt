// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.library

import net.dv8tion.jda.api.entities.Guild
import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.MemberFilter
import toronja.bot.modules.Library
import toronja.utils.Snowflake
import toronja.utils.idTyped

internal val LibraryTopicSet = Command(
    MemberFilter.IsModerator,
    AssociatedModule.Library,
    Command.ParsingContext<Library>::parse,
    shortDescription = "Set the contents of a library topic." +
        "\nThis command accepts three arguments: category name, topic name and new contents.",
    longDescription = """Set the contents of a library topic.

This command accepts three arguments `<category-name> <topic-name> <new-content>`.
`<category-name>` is the category in which to create or update the topic.
`<topic-name>` is the name of the topic to update.
`<new-content>` is the new content to show when this topic is shown using the `library` command.

Example of use:
 - `\{COMMAND} foo test This is an example entry`: update the content of topic `test` in category `foo`."""
)

private fun Command.ParsingContext<Library>.parse(): Command.Parsed<Library> {
    val categoryName = extractor.getNextField() ?: return error("Category name is required.")
    val topicName = extractor.getNextField() ?: return error("Topic name is required.")
    val newContents = extractor.rest
    if (newContents.isEmpty()) return error("Contents cannot be empty.")
    val guild = message.guild.idTyped
    return action { updateTopic(guild, categoryName, topicName, newContents) }
}

private suspend fun Command.ActionContext<Library>.updateTopic(
    guild: Snowflake<Guild>, categoryName: String, topicName: String, newContents: String
) {
    val response = when (module.updateTopic(guild, categoryName, topicName, newContents)) {
        is Library.TopicResult.Result ->
            "$authorMention, library topic `$topicName` in category `$categoryName` updated successfully."
        else -> "$authorMention, library category `$categoryName` not found."
    }
    channel.sendMessage(response).queue()
}

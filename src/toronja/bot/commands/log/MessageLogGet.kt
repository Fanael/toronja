// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.log

import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.MemberFilter
import toronja.bot.modules.message.MessageLog
import toronja.bot.modules.message.MessageRange
import toronja.bot.modules.message.formatters.HtmlFormatter
import toronja.utils.Snowflake
import java.io.ByteArrayOutputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

internal val MessageLogGet = Command(
    MemberFilter.CanManageMessagesOrChannel,
    AssociatedModule.MessageLog,
    Command.ParsingContext<MessageLog>::parse,
    shortDescription = "Get messages from the message log.\n" +
        "This command takes three arguments: the ID of the oldest message to include, the ID of the newest message" +
        "to include, and a *mention* of the channel to get the log of",
    longDescription = """Get messages from the message log.

The message log contains all message actions, including edits and removals.

This command accepts three arguments `<oldest-message-id> <newest-message-id> <channel-mention>`.
`<oldest-message-id>` is the ID of the oldest message to include in the log, *inclusive*.
`<newest-message-id>` is the ID of the newest message to include in the log, *inclusive*.
`<channel-mention>` is a *mention* of the channel to get the message log of.

The archive is returned as a compressed HTML file containing a "pretty" log of message events.

Note that the message log is maintained by recording every message sent/edited/deleted as it happens,
so the message log will not see events that are happening when it's disabled, or during bot's downtime.

Example of use:
 - `\{COMMAND} 123456789 123456999 #foo`: get the log of messages, including edits and deletes, of the
 channel #foo. Only messages whose IDs are in the range [123456789, 123456999] will be included in the log."""
)

private fun Command.ParsingContext<MessageLog>.parse(): Command.Parsed<MessageLog> {
    val messageRangeStartString = extractor.getNextField() ?: return error("Range start is required.")
    val messageRangeStart = messageRangeStartString.toLongOrNull() ?: return error(
        "`${messageRangeStartString.stripMentions()}` doesn't seem to be a valid message ID."
    )
    val messageRangeEndString = extractor.getNextField() ?: return error("Range end is required.")
    val messageRangeEnd = messageRangeEndString.toLongOrNull() ?: return error(
        "`${messageRangeEndString.stripMentions()}` doesn't seem to be a valid message ID."
    )
    val channels = message.mentionedChannels
    val channel = when (channels.size) {
        0 -> return error("You need to mention a channel to get a log of.")
        1 -> channels[0]
        else -> return error("Only one channel mention is allowed.")
    }
    val range = MessageRange(channel, Snowflake(messageRangeStart), Snowflake(messageRangeEnd))
    return action { showLog(range) }
}

private fun Command.ActionContext<MessageLog>.showLog(messageRange: MessageRange) {
    val messageEvents = module.retrieve(messageRange)
    if (messageEvents.isEmpty()) {
        channel.sendMessage("$authorMention, the specified range contains no logged messages.").queue()
        return
    }
    val formatter = HtmlFormatter()
    messageEvents.forEach(formatter::formatNextEvent)
    val byteStream = ByteArrayOutputStream()
    ZipOutputStream(byteStream).use { zipStream ->
        zipStream.putNextEntry(ZipEntry("message-log.html"))
        formatter.writeResult(zipStream)
        zipStream.closeEntry()
    }
    channel.sendFile(byteStream.toByteArray(), "message-log.zip").queue()
}

// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.misc

import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.entities.GuildChannel
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.Role
import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.MemberFilter
import toronja.bot.modules.base.Module
import toronja.utils.idTyped

internal val DumpAcl = Command(
    MemberFilter.CanManageRolesOrChannel,
    AssociatedModule.None,
    Command.ParsingContext<Module.None>::parse,
    shortDescription = "Dump all access control lists on the server." +
        "\nThis command takes no arguments.",
    longDescription = """Dump all access control lists on the server.

This commands creates a user-"readable" dump of all channel, role and user permissions on this server.
The output is a series of SQL statements intended for manual querying using SQLite shell."""
)

private fun Command.ParsingContext<Module.None>.parse(): Command.Parsed<Module.None> {
    if (extractor.getNextField() != null) return error("This command takes no arguments.")
    return action {
        val guild = channel.guild
        val dumpBytes = buildString(capacity = 10_000) {
            append("PRAGMA foreign_keys = ON;\n")
            append("BEGIN;\n")
            createTables()
            append('\n')
            dumpRoles(guild.roles)
            dumpMembers(guild.members)
            dumpChannels(guild.channels)
            append("COMMIT;\n")
        }.toByteArray()
        channel.sendFile(dumpBytes, "acl-dump.sql").queue()
    }
}

private fun StringBuilder.dumpRoles(roles: List<Role>) {
    for (role in roles) {
        val roleId = role.idTyped
        append("INSERT INTO principals VALUES (${roleId.rawValue}, '${role.name.sqlEscape()}', 2);\n")
        append("INSERT INTO role_permissions VALUES (${roleId.rawValue}, ${role.permissionsRaw});\n")
    }
}

private fun StringBuilder.dumpMembers(members: List<Member>) {
    for (member in members) {
        val memberId = member.user.idTyped
        append("INSERT INTO principals VALUES (${memberId.rawValue}, '${member.effectiveName.sqlEscape()}', 1);\n")
        for (role in member.roles) {
            append("INSERT INTO member_roles VALUES (${memberId.rawValue}, ${role.idLong});\n")
        }
    }
}

private fun StringBuilder.dumpChannels(channels: List<GuildChannel>) {
    for (channel in channels) {
        val channelId = channel.idTyped
        append("INSERT INTO principals VALUES (${channelId.rawValue}, '${channel.name.sqlEscape()}', 3);\n")
        for (override in channel.permissionOverrides) {
            val ownerId = override.role?.idTyped ?: override.member!!.user.idTyped
            append(
                "INSERT INTO channel_overrides VALUES" +
                    "(${channelId.rawValue}, ${ownerId.rawValue}, ${override.allowedRaw}, ${override.deniedRaw});\n"
            )
        }
    }
}

private fun StringBuilder.createTables() {
    append(
        """
CREATE TABLE permissions (
    permission_bit INTEGER NOT NULL PRIMARY KEY,
    permission_name TEXT NOT NULL
);
"""
    )
    for (permission in Permission.values()) {
        append(
            "INSERT INTO permissions VALUES (${permission.rawValue}, '${permission.name}')" +
                " ON CONFLICT (permission_bit) DO NOTHING;\n"
        )
    }
    append(
        """
CREATE TABLE principal_types (
    type_id INTEGER NOT NULL PRIMARY KEY,
    type_name TEXT NOT NULL
);
INSERT INTO principal_types VALUES (1, 'member');
INSERT INTO principal_types VALUES (2, 'role');
INSERT INTO principal_types VALUES (3, 'channel');

CREATE TABLE principals (
    principal_id INTEGER NOT NULL PRIMARY KEY,
    principal_name TEXT NOT NULL,
    type_id INTEGER NOT NULL REFERENCES principal_types (type_id)
);

CREATE TABLE role_permissions (
    role_id INTEGER NOT NULL PRIMARY KEY REFERENCES principals (principal_id),
    allowed_mask INTEGER NOT NULL
);

CREATE TABLE member_roles (
    member_id INTEGER NOT NULL REFERENCES principals (principal_id),
    role_id INTEGER NOT NULL REFERENCES role_permissions (role_id),
    PRIMARY KEY (member_id, role_id)
) WITHOUT ROWID;

CREATE TABLE channel_overrides (
    channel_id INTEGER NOT NULL REFERENCES principals (principal_id),
    override_owner INTEGER NOT NULL REFERENCES principals (principal_id),
    allowed_mask INTEGER NOT NULL,
    denied_mask INTEGER NOT NULL,
    PRIMARY KEY (channel_id, override_owner)
) WITHOUT ROWID;
"""
    )
}

private fun String.sqlEscape(): String = replace("'", "''")

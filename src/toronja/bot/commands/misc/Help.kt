// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.misc

import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import toronja.bot.Bot
import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.CommandProcessor
import toronja.bot.commands.MemberFilter
import toronja.utils.Snowflake
import toronja.utils.idTyped

internal val Help = Command(
    MemberFilter.Anyone,
    AssociatedModule.WholeBot,
    Command.ParsingContext<Bot>::parse,
    shortDescription = """Show help message.
With no arguments, show the list of commands; with one argument, show detailed help about the given command.""",
    longDescription = """Show help message.

With no arguments, show the list of commands available to everyone on this server.

With a single argument `<command-name>`, show detailed help on the given command.

Examples of use:
 - `\{COMMAND}`: list all commands.
 - `\{COMMAND} ping`: show the detailed help on the `ping` command."""
)

private fun Command.ParsingContext<Bot>.parse(): Command.Parsed<Bot> {
    val guild = message.guild.idTyped
    val commandPrefix = module.commandProcessor.prefixIn(guild)
    val commandName = extractor.getNextField() ?: return action { listCommands(guild, commandPrefix) }
    if (extractor.getNextField() != null) return error("Only one command name allowed.")
    val command = CommandProcessor.commandMap[commandName]
        ?: return error("Unknown command name `${commandName.stripMentions()}`.")
    val caller = message.member!!
    return action { showHelpForCommand(commandPrefix, commandName, command, caller) }
}

private fun Command.ActionContext<Bot>.listCommands(guild: Snowflake<Guild>, commandPrefix: String) {
    val commandNames = CommandProcessor.commands.filter { shouldShowCommand(it.second, guild, module) }
        .map(Pair<String, Command<*>>::first)
        .joinToString(separator = "\n")
    val response = "$authorMention, list of available commands: ```\n$commandNames```" +
        "Use `$commandPrefix${CommandProcessor.HELP_COMMAND_NAME} <command-name>`" +
        " to show a detailed description of a command." +
        "\n\nTo get the list of moderator-only commands, use" +
        " `$commandPrefix${CommandProcessor.MOD_HELP_COMMAND_NAME}`."
    channel.sendMessage(response).queue()
}

private fun Command.ActionContext<Bot>.showHelpForCommand(
    commandPrefix: String, commandName: String, command: Command<*>, caller: Member
) {
    val response = if (command.isMemberAllowed(caller)) {
        val prefixedName = commandPrefix + commandName
        val effectiveDescription = command.longDescription.replace("\\{COMMAND}", prefixedName)
        "$authorMention, help for `$commandName`:\n$effectiveDescription"
    } else {
        "$authorMention, you lack the required permissions to use `$commandName`."
    }
    channel.sendMessage(response).queue()
}

private fun shouldShowCommand(command: Command<*>, guild: Snowflake<Guild>, bot: Bot): Boolean =
    command.isMemberAllowed === MemberFilter.Anyone && command.isEnabledIn(guild, bot)

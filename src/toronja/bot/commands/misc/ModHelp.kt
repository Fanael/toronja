// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.misc

import net.dv8tion.jda.api.entities.Guild
import toronja.bot.Bot
import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.CommandProcessor
import toronja.bot.commands.MemberFilter
import toronja.utils.Snowflake
import toronja.utils.idTyped

internal val ModHelp = Command(
    MemberFilter.IsModerator,
    AssociatedModule.WholeBot,
    Command.ParsingContext<Bot>::parse,
    shortDescription = """List the moderator-only commands.
This command takes no arguments; to get detailed information on a moderator-only command,
use the regular help command.""",
    longDescription = """List the moderator-only commands.

This commands takes no arguments, it just lists the commands.
To get detailed information on a moderator-only command, use the regular help command."""
)

private fun Command.ParsingContext<Bot>.parse(): Command.Parsed<Bot> {
    if (extractor.getNextField() != null) return error("This command takes no arguments.")
    val guild = message.guild.idTyped
    val commandPrefix = module.commandProcessor.prefixIn(guild)
    return action { listCommands(guild, commandPrefix) }
}

private fun Command.ActionContext<Bot>.listCommands(guild: Snowflake<Guild>, commandPrefix: String) {
    val commandNames = CommandProcessor.commands.filter { shouldShowCommand(it.second, guild, module) }
        .map(Pair<String, Command<*>>::first)
        .joinToString(separator = "\n")
    val response = "$authorMention, list of available moderator-only commands: ```\n" +
        "$commandNames```Use `$commandPrefix${CommandProcessor.HELP_COMMAND_NAME}" +
        " <command-name>` to show a detailed description of a command."
    channel.sendMessage(response).queue()
}

private fun shouldShowCommand(command: Command<*>, guild: Snowflake<Guild>, bot: Bot): Boolean =
    command.isMemberAllowed !== MemberFilter.Anyone && command.isEnabledIn(guild, bot)

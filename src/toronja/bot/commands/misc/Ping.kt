// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.misc

import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.MemberFilter
import toronja.bot.modules.base.Module

internal val Ping = Command(
    MemberFilter.Anyone,
    AssociatedModule.None,
    Command.ParsingContext<Module.None>::parse,
    shortDescription = """Check if the bot is online.
Can be called with either no arguments, or an arbitrary text to send back.""",
    longDescription = """Check if the bot is online.

Optionally, can be followed by an arbitrary text that will be send back in response."""
)

private fun Command.ParsingContext<Module.None>.parse(): Command.Parsed<Module.None> {
    val msg = message
    val text = extractor.rest.stripMentions()
    return action {
        val webSocketPing = msg.jda.gatewayPing
        val estimatedLatency = System.currentTimeMillis() - msg.timeCreated.toInstant().toEpochMilli()
        val response = if (text.isEmpty()) {
            "$authorMention, pong (took $estimatedLatency ms, WebSocket ping $webSocketPing ms)."
        } else {
            "$authorMention, pong (took $estimatedLatency ms, WebSocket ping $webSocketPing ms): $text"
        }
        channel.sendMessage(response).queue()
    }
}

// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.module

import net.dv8tion.jda.api.entities.Guild
import toronja.bot.Bot
import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.CommandParser
import toronja.bot.commands.MemberFilter
import toronja.bot.modules.base.ModuleStateChanged
import toronja.bot.modules.base.ToggleableModule

internal val ModuleEnable = makeCommand("enable", ToggleableModule::enableIn)
internal val ModuleDisable = makeCommand("disable", ToggleableModule::disableIn)

private fun makeCommand(
    verb: String, operation: suspend (ToggleableModule, Guild) -> ModuleStateChanged
): Command<Bot> = Command(
    MemberFilter.CanManageServer,
    AssociatedModule.WholeBot,
    makeParser(verb, operation),
    makeShortDescription(verb),
    makeLongDescription(verb)
)

private fun makeParser(
    verb: String, operation: suspend (ToggleableModule, Guild) -> ModuleStateChanged
): CommandParser<Bot> = parser@{
    val moduleName = extractor.getNextField() ?: return@parser error("Module name is required.")
    if (extractor.getNextField() != null) return@parser error("This command takes exactly one argument.")
    val moduleToChange = module.moduleNameMap[moduleName]
        ?: return@parser error("Module ${moduleName.stripMentions()} not found.")
    val guild = message.guild
    action {
        val response = when (operation(moduleToChange, guild)) {
            ModuleStateChanged.YES -> "$authorMention, module `$moduleName` ${verb}d successfully."
            ModuleStateChanged.NO -> "$authorMention, module `$moduleName` is already ${verb}d."
        }
        channel.sendMessage(response).queue()
    }
}

private fun makeShortDescription(verb: String): String =
    """${verb.replaceFirstChar(Character::toUpperCase)} the specified module in this server.
This command takes one argument, the name of the module to $verb."""

private fun makeLongDescription(verb: String): String =
    """${verb.replaceFirstChar(Character::toUpperCase)} the specified module in this server.

This command takes exactly one argument, which is the name of the module to $verb.

To see the modules and their current state, use the `module-list` command.

Example of use:
 - `\{COMMAND} role-restorer`: ${verb}s the `role-restorer` module in this server."""

// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.module

import net.dv8tion.jda.api.entities.Guild
import toronja.bot.Bot
import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.MemberFilter
import toronja.utils.Snowflake
import toronja.utils.idTyped

internal val ModuleList = Command(
    MemberFilter.CanManageServer,
    AssociatedModule.WholeBot,
    Command.ParsingContext<Bot>::parse,
    shortDescription = """List bot modules and their status in this server.
This command takes no arguments.""",
    longDescription = """List bot modules and their status in this server.

Enabled modules are shown as 1, disabled ones as 0.

This command takes no arguments."""
)

private fun Command.ParsingContext<Bot>.parse(): Command.Parsed<Bot> {
    if (extractor.getNextField() != null) return error("This command takes no arguments.")
    val guild = message.guild.idTyped
    return action {
        val formattedOutput = formatModuleList(module, guild)
        channel.sendMessage("$authorMention, module list: $formattedOutput").queue()
    }
}

private fun formatModuleList(bot: Bot, guild: Snowflake<Guild>): String = buildString(capacity = 200) {
    append("```\n")
    for ((name, module) in bot.moduleNameMap) {
        append(if (module.isEnabledIn(guild)) '1' else '0')
        append(": ")
        append(name)
        append('\n')
    }
    append("```")
}

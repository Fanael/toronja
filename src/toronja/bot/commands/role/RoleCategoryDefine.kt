// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.role

import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.MemberFilter
import toronja.bot.modules.RoleCategorizer
import toronja.utils.idTyped

internal val RoleCategoryDefine = Command(
    MemberFilter.CanManageRoles,
    AssociatedModule.RoleCategorizer,
    Command.ParsingContext<RoleCategorizer>::parse,
    shortDescription = "Create a new role category, or change the description of an existing one." +
        "\nThis command takes two arguments `<category-name> <description>`.",
    longDescription = """Create a new role category, or change the description of an existing one.

This command takes two arguments `<category-name> <description>`.
If `<category-name>` refers to a category that already exists, its description is updated.
Category description is allowed to be empty.

Example of use:
 - `\{COMMAND} color Colors for your nickname`: creates, or changes description of, the role category `color`."""
)

private fun Command.ParsingContext<RoleCategorizer>.parse(): Command.Parsed<RoleCategorizer> {
    val categoryName = extractor.getNextField() ?: return error("Category name is required.")
    val description = extractor.rest
    val guild = message.guild.idTyped
    return action {
        module.defineCategory(guild, categoryName, description)
        channel.sendMessage("$authorMention, role category `$categoryName` defined successfully.").queue()
    }
}

// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.role

import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.MemberFilter
import toronja.bot.modules.RoleCategorizer
import toronja.utils.idTyped

internal val RoleCategoryRemove = Command(
    MemberFilter.CanManageRoles,
    AssociatedModule.RoleCategorizer,
    Command.ParsingContext<RoleCategorizer>::parse,
    shortDescription = "Remove a role category." +
        "\nThis command takes a single argument, the name of the category to remove.",
    longDescription = """Remove a role category.

This command takes a single argument `<category-name>`, being the name of the role category to remove.

The roles in the category are forgotten as if by `forget-role`, they remain on the server.

Example of use:
 - `\{COMMAND} color`: remove the role category `color`, forgetting all roles in it."""
)

private fun Command.ParsingContext<RoleCategorizer>.parse(): Command.Parsed<RoleCategorizer> {
    val categoryName = extractor.getNextField() ?: return error("Category name is required.")
    if (extractor.getNextField() != null) return error("This command takes exactly one argument.")
    val guild = message.guild.idTyped
    return action {
        val response = if (module.removeCategory(guild, categoryName)) {
            "$authorMention, role category `$categoryName` removed successfully."
        } else {
            "$authorMention, role category `${categoryName.stripMentions()}` was not found."
        }
        channel.sendMessage(response).queue()
    }
}

// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.role

import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.MemberFilter
import toronja.bot.modules.RoleCategorizer
import toronja.utils.idTyped

internal val RoleCategoryRename = Command(
    MemberFilter.CanManageRoles,
    AssociatedModule.RoleCategorizer,
    Command.ParsingContext<RoleCategorizer>::parse,
    shortDescription = "Rename an existing role category." +
        "\nThis command takes exactly two arguments, the current name of a role category and a new name.",
    longDescription = """Rename an existing role category.

This command takes two arguments `<category-name> <new-name>`.
`<category-name>` is the *current* name of the role category to rename.
`<new-name>` is the *new* name of the role category.

Example of use:
 - `\{COMMAND} color colour`: rename the `color` role category to `colour`."""
)

private fun Command.ParsingContext<RoleCategorizer>.parse(): Command.Parsed<RoleCategorizer> {
    val categoryName = extractor.getNextField() ?: return error("Current category name is required.")
    val newName = extractor.getNextField() ?: return error("New category name is required.")
    if (extractor.getNextField() != null) return error("This command takes exactly two arguments.")
    val guild = message.guild.idTyped
    return action {
        val response = if (module.renameCategory(guild, categoryName, newName)) {
            "authorMention, role category `$categoryName` successfully renamed to `$newName."
        } else {
            "$authorMention role category `${categoryName.stripMentions()}` not found."
        }
        channel.sendMessage(response).queue()
    }
}

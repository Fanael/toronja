// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.role

import net.dv8tion.jda.api.entities.Guild
import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.MemberFilter
import toronja.bot.modules.RoleCategorizer
import toronja.utils.Snowflake
import toronja.utils.idTyped

internal val RoleList = Command(
    MemberFilter.Anyone,
    AssociatedModule.RoleCategorizer,
    Command.ParsingContext<RoleCategorizer>::parse,
    shortDescription = "List self-assignable role categories or roles in a category." +
        "\nIf called with no arguments, list the role categories." +
        "\nIf called with one argument, list the roles in the given category.",
    longDescription = """List self-assignable role categories or roles in a category.

With no arguments, show a list of all role categories.
With a single argument `<role-category>`, show a list of roles in the given category.

Example of use:
 - `\{COMMAND}`: list all role categories on this server.
 - `\{COMMAND} foo`: list all roles in category `foo` on this server."""
)

private fun Command.ParsingContext<RoleCategorizer>.parse(): Command.Parsed<RoleCategorizer> {
    val guild = message.guild.idTyped
    val categoryName = extractor.getNextField() ?: return action { listCategories(guild) }
    if (extractor.getNextField() != null) return error("This command takes zero or one argument(s).")
    return action { listRolesInCategory(guild, categoryName) }
}

private fun Command.ActionContext<RoleCategorizer>.listCategories(guild: Snowflake<Guild>) {
    val response = buildString(capacity = 500) {
        append(authorMention).append(", existing role categories:\n")
        for ((name, description) in module.getCategories(guild)) {
            append('`').append(name)
            if (description.isNotEmpty()) {
                append("`: ").append(description).append('\n')
            } else {
                append("`\n")
            }
        }
    }
    channel.sendMessage(response).queue()
}

private fun Command.ActionContext<RoleCategorizer>.listRolesInCategory(guild: Snowflake<Guild>, categoryName: String) {
    val response = when (val result = module.getRolesInCategory(guild, categoryName)) {
        is RoleCategorizer.RoleResult.Result -> {
            val roles = result.value.joinToString(separator = "\n")
            "$authorMention, roles in category `$categoryName`: ```\n$roles```"
        }
        is RoleCategorizer.RoleResult.CategoryNotFound ->
            "$authorMention, role category `${categoryName.stripMentions()}` not found."
    }
    channel.sendMessage(response).queue()
}

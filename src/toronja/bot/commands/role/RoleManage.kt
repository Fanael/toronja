// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.role

import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Role
import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.CommandParser
import toronja.bot.commands.MemberFilter
import toronja.bot.modules.RoleCategorizer
import toronja.utils.completeSuspending
import toronja.utils.idTyped

internal val RoleImport = makeCommand(
    Command.ActionContext<RoleCategorizer>::importRole,
    shortDescription = "Import a preexisting role into a role category." +
        "\nThis command takes two arguments, a role category name and a role name.",
    longDescription = """Import a preexisting role into a role category.

This command takes two arguments, `<category-name> <role-name>`.
`<category-name>` is the name of the role category to add the role to.
`<role-name>` is the role name (not mention!) to import.

Example of use:
 - `\{COMMAND} color Red`: import the role `Red` into category `color`."""
)

internal val RoleForget = makeCommand(
    Command.ActionContext<RoleCategorizer>::forgetRole,
    shortDescription = "Drop a role from a category." +
        "\nThis command takes two arguments, a role category name and a role name.",
    longDescription = """Drop a role from a category.

This command takes two arguments, `<category-name> <role-name>`.
`<category-name>` is the name of the role category to remove the role from.
`<role-name>` is the role name (not mention!) to drop.

The role is only dropped from the bot's internal database, it will still exist on the server.

Example of use:
 - `\{COMMAND} color Yellow`: forget the role `Yellow` from category `color``."""
)

internal val RoleAdd = makeCommand(
    Command.ActionContext<RoleCategorizer>::addRole,
    shortDescription = "Create a new role on the server and add it into a role category." +
        "\nThis command takes two arguments, a role category name and a role name.",
    longDescription = """Create a new role on the server and add it into a role category.

This command takes two arguments, `<category-name> <role-name>`.
`<category-name>` is the name of the role category to add the role to.
`<role-name>` is the name of the role to create.

This command differs from `role-import` in that it *creates* a new role on the server.

Example of use:
 - `\{COMMAND} color Green`: create a new role named `Green` on the server and add it to category `color`."""
)

private suspend fun Command.ActionContext<RoleCategorizer>.importRole(args: Arguments) {
    val roles = args.guild.getRolesByName(args.roleName, /* ignoreCase = */ true)
    val response = when (roles.size) {
        0 -> "$authorMention, no role named `${args.roleName.stripMentions()}` found on this server."
        1 -> when (module.importRole(args.guild.idTyped, args.categoryName, roles[0])) {
            is RoleCategorizer.RoleResult.Result ->
                "$authorMention, role `${args.roleName.stripMentions()}` imported successfully."
            is RoleCategorizer.RoleResult.CategoryNotFound ->
                "$authorMention, role category `${args.categoryName.stripMentions()}` is not present."
        }
        else -> "$authorMention multiple roles named `${args.roleName.stripMentions()} found, cannot import."
    }
    channel.sendMessage(response).queue()
}

private suspend fun Command.ActionContext<RoleCategorizer>.forgetRole(args: Arguments) {
    val response = when (val result = module.forgetRole(args.guild.idTyped, args.categoryName, args.roleName)) {
        is RoleCategorizer.RoleResult.Result -> if (result.value) {
            "$authorMention role `${args.roleName}` in category `${args.categoryName}` forgotten successfully."
        } else {
            "$authorMention, no role named `${args.roleName.stripMentions()}` was found in `${args.categoryName}`."
        }
        is RoleCategorizer.RoleResult.CategoryNotFound ->
            "$authorMention, role category `${args.categoryName.stripMentions()}` is not present."
    }
    channel.sendMessage(response).queue()
}

private suspend fun Command.ActionContext<RoleCategorizer>.addRole(args: Arguments) {
    val role = args.guild.createRole()
        .setName(args.roleName)
        .reason("Requested by ${args.callerTag}")
        .completeSuspending()
    RoleAutoDeleter(role).use { roleAutoDeleter ->
        val response = when (module.importRole(args.guild.idTyped, args.categoryName, role)) {
            is RoleCategorizer.RoleResult.Result -> {
                roleAutoDeleter.roleToDelete = null
                "$authorMention, role `${args.roleName.stripMentions()}` created successfully."
            }
            is RoleCategorizer.RoleResult.CategoryNotFound ->
                "$authorMention, role category `${args.categoryName.stripMentions()}` is not present."
        }
        channel.sendMessage(response).queue()
    }
}

private class RoleAutoDeleter(var roleToDelete: Role?) : AutoCloseable {
    override fun close() {
        val role = roleToDelete ?: return
        role.delete().reason("Rolling back after import failure").queue()
    }
}

private typealias ActionBody = suspend Command.ActionContext<RoleCategorizer>.(args: Arguments) -> Unit

private fun makeCommand(
    actionBody: ActionBody, shortDescription: String, longDescription: String
): Command<RoleCategorizer> = Command(
    MemberFilter.CanManageRoles,
    AssociatedModule.RoleCategorizer,
    makeParser(actionBody),
    shortDescription,
    longDescription
)

private fun makeParser(actionBody: ActionBody): CommandParser<RoleCategorizer> = parser@{
    val categoryName = extractor.getNextField() ?: return@parser error("Category name is required.")
    val roleName = extractor.rest
    if (roleName.isEmpty()) return@parser error("Role name is required.")
    val args = Arguments(message.guild, categoryName, roleName, message.author.asTag)
    action { actionBody(args) }
}

private class Arguments(val guild: Guild, val categoryName: String, val roleName: String, val callerTag: String)

// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.role

import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.MemberFilter
import toronja.bot.modules.RoleCategorizer
import toronja.utils.idTyped

internal val RoleToggle = Command(
    MemberFilter.Anyone,
    AssociatedModule.RoleCategorizer,
    Command.ParsingContext<RoleCategorizer>::parse,
    shortDescription = "Toggle a role from the given category for the calling user." +
        "\nThis command takes two arguments, a role category name and a role name.",
    longDescription = """Toggle a role from the given category for the calling user.

This command expects two arguments, `<role-category> <role-name>`.
`<category-name>` is the name of the role category to search for the role in.
`<role-name>` is the role name (not mention!) to toggle.

The role is toggled, i.e. if the calling user already has that role, it will be removed, otherwise it will be added.

Normally this command is unnecessarily cumbersome to use, it serves only to disambiguate between roles of the same
name in different categories. Prefer using `role` instead.

Example of use:
 - `\{COMMAND} color Teal`: join or leave the role `Teal` found in category `color` on this server."""
)

private fun Command.ParsingContext<RoleCategorizer>.parse(): Command.Parsed<RoleCategorizer> {
    val categoryName = extractor.getNextField() ?: return error("Category name is required.")
    val roleName = extractor.rest
    if (roleName.isEmpty()) return error("Role name is required.")
    val callingMember = message.member!!
    val guild = callingMember.guild
    return action {
        val response = when (val findResult = module.findRoleInCategory(guild.idTyped, categoryName, roleName)) {
            is RoleCategorizer.RoleResult.Result -> findResult.value?.let { toggleRole(guild, callingMember, it) }
                ?: "authorMention, role `${roleName.stripMentions()}` not found in `$categoryName`."
            is RoleCategorizer.RoleResult.CategoryNotFound ->
                "$authorMention, role category `${categoryName.stripMentions()}` not found."
        }
        channel.sendMessage(response).queue()
    }
}

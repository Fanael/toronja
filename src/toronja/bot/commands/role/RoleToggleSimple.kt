// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.role

import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.Role
import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.MemberFilter
import toronja.bot.modules.RoleCategorizer
import toronja.utils.Snowflake
import toronja.utils.idTyped

internal val RoleToggleSimple = Command(
    MemberFilter.Anyone,
    AssociatedModule.RoleCategorizer,
    Command.ParsingContext<RoleCategorizer>::parse,
    shortDescription = "Toggle a role for the calling user." +
        "\nThis command takes a single argument, the name of the role to toggle.",
    longDescription = """Toggle a role for the calling user.

The command expects a single argument `<role-name>`, being the name of the role to toggle.

The role is toggled, i.e. if the calling user already has that role, it will be removed, otherwise it will be added.

This command requires the role name to be unique server-wide. For dealing with ambiguous role names,
prefer `role-toggle` instead.

Example of use:
 - `\{COMMAND} Teal`: make the sending user join or leave the role `Teal`."""
)

internal fun toggleRole(guild: Guild, member: Member, role: Snowflake<Role>): String {
    val actualRole = guild.getRoleById(role.rawValue)!!
    return when (member.roles.find { it.idTyped == role }) {
        null -> {
            guild.addRoleToMember(member, actualRole).reason("Requested via bot").queue()
            "${member.asMention}, you joined **${actualRole.name}**."
        }
        else -> {
            guild.removeRoleFromMember(member, actualRole).reason("Requested via bot").queue()
            "${member.asMention}, you left **${actualRole.name}**."
        }
    }
}

private fun Command.ParsingContext<RoleCategorizer>.parse(): Command.Parsed<RoleCategorizer> {
    val roleName = extractor.rest
    if (roleName.isEmpty()) return error("Role name is required.")
    val callingMember = message.member!!
    val guild = callingMember.guild
    return action {
        val roles = module.findRoleInGuild(guild.idTyped, roleName)
        val response = when (roles.size) {
            0 -> "$authorMention, role `${roleName.stripMentions()}` not found."
            1 -> toggleRole(guild, callingMember, roles[0])
            else -> "$authorMention, multiple roles named `$roleName` found, use `role-toggle` instead."
        }
        channel.sendMessage(response).queue()
    }
}

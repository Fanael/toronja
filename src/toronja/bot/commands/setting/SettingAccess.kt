// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.setting

import net.dv8tion.jda.api.entities.Guild
import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.CommandProcessor
import toronja.bot.commands.MemberFilter
import toronja.bot.settings.ChannelSetting
import toronja.bot.settings.GuildSetting
import toronja.bot.settings.GuildSettings
import toronja.bot.settings.Registry
import toronja.bot.settings.RoleSetting
import toronja.bot.settings.StringSetting
import toronja.bot.settings.UnitSetting
import toronja.utils.idTyped

internal val SettingAccess = Command(
    MemberFilter.CanManageServer,
    AssociatedModule.GuildSettings,
    Command.ParsingContext<GuildSettings>::parse,
    shortDescription = """Get or set a bot setting.
With one argument `<setting-name>`, show the current value of that setting.
With two arguments `<setting-name> <new-value>`, change the value of that setting.""",
    longDescription = """Get or set a bot setting.

With one argument `<setting-name>`, show the current value of the specified setting.

With two arguments `<setting-name> <new-value>`, set the value of the specified setting
to the given value.

To get detailed information on a setting, use the `${CommandProcessor.SETTING_DESCRIBE_COMMAND_NAME}` command.

Examples of use:
 - `\{COMMAND} command-prefix`: show the command prefix in this server.
 - `\{COMMAND} welcome-message Welcome, ${'$'}{USER}!`: change the welcome message in this server."""
)

private fun Command.ParsingContext<GuildSettings>.parse(): Command.Parsed<GuildSettings> {
    val settingName = extractor.getNextField() ?: return error("The setting name is required.")
    val setting = Registry.settingMap[settingName]
        ?: return error("Unknown setting name ${settingName.stripMentions()}")
    val newValue = extractor.rest
    return if (newValue.isNotEmpty()) {
        parseNewValue(setting, newValue)
    } else {
        val guild = message.guild
        action { showSetting(setting, guild) }
    }
}

private fun Command.ParsingContext<GuildSettings>.parseNewValue(
    setting: GuildSetting<*>, newValue: String
): Command.Parsed<GuildSettings> {
    val guild = message.guild
    return when (setting) {
        is StringSetting -> action { setNewValue(setting, newValue, guild) }
        is UnitSetting -> action { setNewValue(setting, Unit, guild) }
        is ChannelSetting -> {
            val mentionedChannels = message.mentionedChannels
            val channel = when (mentionedChannels.size) {
                0 -> return error("A channel mention is required as the new value.")
                1 -> mentionedChannels[0].idTyped
                else -> return error("Only one channel may be mentioned.")
            }
            action { setNewValue(setting, channel, guild) }
        }
        is RoleSetting -> {
            val roles = guild.getRolesByName(newValue, /* ignoreCase = */ true)
            val role = when (roles.size) {
                0 -> return error("Role ${newValue.stripMentions()} not found on the server.")
                1 -> roles[0].idTyped
                else -> return error(
                    "Multiple roles named ${newValue.stripMentions()} found." +
                        " Rename one of them to make the name unambiguous."
                )
            }
            action { setNewValue(setting, role, guild) }
        }
    }
}

private fun Command.ActionContext<GuildSettings>.showSetting(setting: GuildSetting<*>, guild: Guild) {
    val response = when (val value = getSettingValue(module, setting, guild)) {
        null -> "$authorMention, the setting `${setting.name}` is not set."
        else -> "$authorMention, the value of setting `${setting.name}` is: $value"
    }
    channel.sendMessage(response).queue()
}

private fun getSettingValue(guildSettings: GuildSettings, setting: GuildSetting<*>, guild: Guild): String? {
    val guildId = guild.idTyped
    return when (setting) {
        is StringSetting -> guildSettings[guildId, setting]
        is UnitSetting -> guildSettings[guildId, setting]?.let { "(present)" }
        is ChannelSetting -> {
            val channelId = guildSettings[guildId, setting]?.rawValue ?: return null
            "<#$channelId>"
        }
        is RoleSetting -> {
            val roleId = guildSettings[guildId, setting]?.rawValue ?: return null
            val role = guild.getRoleById(roleId) ?: return null
            "${role.name} (id $roleId)"
        }
    }
}

private suspend fun <T> Command.ActionContext<GuildSettings>.setNewValue(
    setting: GuildSetting<T>, newValue: T, guild: Guild
) {
    module.set(guild, setting, newValue)
    channel.sendMessage("$authorMention `${setting.name}` changed successfully.").queue()
}

// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.setting

import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.MemberFilter
import toronja.bot.modules.base.Module
import toronja.bot.settings.Registry

internal val SettingDescribe = Command(
    MemberFilter.CanManageServer,
    AssociatedModule.None,
    Command.ParsingContext<Module.None>::parse,
    shortDescription = """Show a description of a bot setting.
This command takes exactly one argument `<setting-name>`.""",
    longDescription = """Show a description of a bot setting.

This command takes exactly one argument `<setting-name>`, which is the setting to show
a description of.

Example of use: `\{COMMAND} auto-role`: show a description of the `auto-role` setting."""
)

private fun Command.ParsingContext<Module.None>.parse(): Command.Parsed<Module.None> {
    val settingName = extractor.getNextField() ?: return error("The setting name is required.")
    if (extractor.getNextField() != null) return error("This command takes exactly one argument.")
    val setting = Registry.settingMap[settingName]
        ?: return error("Unknown setting name ${settingName.stripMentions()}")
    return action {
        channel.sendMessage("$authorMention, description of `${setting.name}`: ${setting.description}").queue()
    }
}

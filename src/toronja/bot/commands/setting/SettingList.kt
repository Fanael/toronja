// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.setting

import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.CommandProcessor
import toronja.bot.commands.MemberFilter
import toronja.bot.modules.base.Module
import toronja.bot.settings.Registry

internal val SettingList = Command(
    MemberFilter.CanManageServer,
    AssociatedModule.None,
    Command.ParsingContext<Module.None>::parse,
    shortDescription = "List the names of bot settings.\nThis command takes no arguments.",
    longDescription = """List the names of bot settings.

This command takes no arguments.

To get detailed information on a setting, use the `${CommandProcessor.SETTING_DESCRIBE_COMMAND_NAME}` command."""
)

private fun Command.ParsingContext<Module.None>.parse(): Command.Parsed<Module.None> {
    if (extractor.getNextField() != null) return error("This command takes no arguments.")
    return action {
        val settingNames = Registry.settingMap.joinToString(separator = "\n") { it.first }
        val response = "$authorMention, list of known settings: ```\n$settingNames```" +
            "For more details on a setting, use the " +
            "`${CommandProcessor.SETTING_DESCRIBE_COMMAND_NAME}` command."
        channel.sendMessage(response).queue()
    }
}

// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.commands.setting

import net.dv8tion.jda.api.entities.Guild
import toronja.bot.commands.AssociatedModule
import toronja.bot.commands.Command
import toronja.bot.commands.MemberFilter
import toronja.bot.settings.GuildSetting
import toronja.bot.settings.GuildSettings
import toronja.bot.settings.Registry

internal val SettingUnset = Command(
    MemberFilter.CanManageServer,
    AssociatedModule.GuildSettings,
    Command.ParsingContext<GuildSettings>::parse,
    shortDescription = """Unset a bot setting in this server.
This command takes exactly one argument `<setting-name>`.""",
    longDescription = """Unset a bot setting in this server.

This command takes exactly one argument `<setting-name>`, which is the name of the setting
to unset.

Example of use: `\{COMMAND} auto-role`: unset the `auto-role` setting, thus disabling the
auto-role functionality."""
)

private fun Command.ParsingContext<GuildSettings>.parse(): Command.Parsed<GuildSettings> {
    val settingName = extractor.getNextField() ?: return error("The setting name is required.")
    if (extractor.getNextField() != null) return error("This command takes exactly one argument.")
    val setting = Registry.settingMap[settingName]
        ?: return error("Unknown setting name `${settingName.stripMentions()}`")
    val guild = message.guild
    return action { unsetSetting(guild, setting) }
}

private suspend fun Command.ActionContext<GuildSettings>.unsetSetting(guild: Guild, setting: GuildSetting<*>) {
    module.removeSetting(guild, setting)
    channel.sendMessage("$authorMention, setting `${setting.name}` unset successfully.").queue()
}

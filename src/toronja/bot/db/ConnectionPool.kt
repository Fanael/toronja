// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.db

import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import toronja.bot.db.utils.withQueryResult
import toronja.utils.makeLogger
import toronja.utils.runWithFixedDelay
import java.sql.SQLException
import java.time.Duration
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantReadWriteLock
import kotlin.concurrent.withLock

/**
 * A concurrency-aware connection pool for SQLite.
 *
 * It's written specifically to take advantage of the fact that SQLite in
 * WAL mode allows multiple readers and a single writer concurrently, without
 * blocking.
 *
 * Readers are only ever blocked by the periodic checkpoint, which is short
 * enough and rare enough that locking is not a big concern. Writers, however,
 * need to be serialized with one another, which can result in long pauses,
 * therefore writes are suspension points.
 */
internal class ConnectionPool(
    databaseFilePath: String, coroutineScope: CoroutineScope, safetyLevel: SafetyLevel = SafetyLevel.NORMAL
) : AutoCloseable {
    fun <T> read(request: Request<T>): T = lock.readLock().withLock {
        readerQueue.acquireLease().use { executeRequest(request, it.connection) }
    }

    suspend fun <T> write(request: Request<T>): T = suspendingWriterMutex.withLock { executeWriteRequest(request) }

    override fun close() {
        lock.writeLock().withLock {
            periodicCheckpointJob.cancel(CancellationException("Connection pool is closing"))
            writer.close()
            readerQueue.close()
        }
    }

    private fun <T> executeWriteRequest(request: Request<T>): T = chooseLockForRequest(request).withLock {
        executeRequest(request, writer)
    }

    private suspend fun runPeriodicCheckpoint(): Unit = coroutineScope {
        runWithFixedDelay(Duration.ofMinutes(PERIODIC_CHECKPOINT_MINUTES)) { write(PeriodicCheckpoint) }
    }

    private fun chooseLockForRequest(request: Request<*>): Lock = if (request === PeriodicCheckpoint) {
        lock.writeLock()
    } else {
        // Even though there's only one writer at any given moment, acquire
        // the lock anyway to ensure that closing the connection pool doesn't
        // kill a request halfway through.
        lock.readLock()
    }

    private val suspendingWriterMutex = Mutex()

    // The write part is only used for exclusive access, normal writers don't
    // need it because they're not supposed to block readers.
    private val lock = ReentrantReadWriteLock(/* fair = */ true)
    private val readerQueue = RawConnectionQueue { connectionId ->
        val name = "Reader-$connectionId"
        logger.info { "Creating new reader connection $name" }
        RawConnection(databaseFilePath, name, safetyLevel, readOnly = true)
    }
    private val writer = RawConnection(databaseFilePath, "Writer", safetyLevel, readOnly = false).also {
        // Force journal replay to happen now by accessing the database.
        it.prepareUncached("SELECT COUNT(*) FROM sqlite_master").use { stmt -> stmt.withQueryResult {} }
    }
    private val periodicCheckpointJob = coroutineScope.launch { runPeriodicCheckpoint() }
}

private const val PERIODIC_CHECKPOINT_MINUTES = 2L

private val logger = makeLogger<ConnectionPool>()

private fun <T> executeRequest(request: Request<T>, connection: RawConnection): T {
    logger.debug { "Executing request $request" }
    val shouldCommit = request.shouldCommit
    try {
        return request(connection).also { if (shouldCommit) connection.commit() }
    } catch (@Suppress("TooGenericExceptionCaught") e: Exception) {
        // We just rollback, log and rethrow the exception, so catching
        // Exception is fine.
        if (shouldCommit) {
            try {
                connection.rollback()
            } catch (sqlError: SQLException) {
                e.addSuppressed(sqlError)
            }
        }
        logger.error(e) { "Processing request $request failed with an exception" }
        throw e
    }
}

private object PeriodicCheckpoint : Request<Unit> {
    override fun invoke(connection: RawConnection): Unit = connection.checkpoint()

    override val shouldCommit: Boolean get() = false

    override fun toString(): String = "PeriodicCheckpoint"
}

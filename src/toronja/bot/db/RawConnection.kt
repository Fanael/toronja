// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.db

import org.sqlite.SQLiteConfig
import org.sqlite.SQLiteOpenMode
import org.sqlite.core.CoreStatement
import toronja.bot.db.utils.withQueryResult
import toronja.utils.collections.persistent.BTreeMap
import toronja.utils.makeLogger
import java.sql.Connection
import java.sql.PreparedStatement

/**
 * Raw, *thread-unsafe* database connection.
 *
 * Normally shouldn't be used directly except through a [ConnectionPool],
 * or possibly under an external lock.
 */
internal class RawConnection(
    databaseFilePath: String,
    val name: String,
    safetyLevel: SafetyLevel = SafetyLevel.NORMAL,
    readOnly: Boolean = false
) {
    fun prepare(sql: String): PreparedStatement {
        val statement = statementCache[sql]
        // Statements may get automatically closed if their execution throws
        // an exception, so we need to check if it's still a valid statement
        // by looking at internals.
        if (statement != null && (statement as CoreStatement).pointer != 0L) {
            return statement
        }
        logger.info { "Preparing statement '$sql' in connection $name" }
        return connection.prepareStatement(sql).also { statementCache += Pair(sql, it) }
    }

    fun prepareUncached(sql: String): PreparedStatement {
        logger.info { "Preparing uncached statement '$sql' in connection $name" }
        return connection.prepareStatement(sql)
    }

    fun commit(): Unit = connection.commit()
    fun rollback(): Unit = connection.rollback()
    fun close(): Unit = connection.close()
    fun checkpoint(): Unit = prepare("PRAGMA wal_checkpoint(RESTART)").withQueryResult {}

    private val connection = connect(databaseFilePath, safetyLevel, readOnly)
    private var statementCache = BTreeMap<String, PreparedStatement>()
}

internal enum class SafetyLevel {
    // NB: these identifiers correspond directly to the values allowed by
    // SQLite "synchronous" PRAGMA.
    NORMAL,
    OFF;
}

private val logger = makeLogger<RawConnection>()

private fun connect(databaseFilePath: String, safetyLevel: SafetyLevel, readOnly: Boolean): Connection {
    // Since this class is documented as thread-unsafe, we can tell
    // SQLite to skip its internal synchronization.
    val config = SQLiteConfig()
    config.setReadOnly(readOnly)
    config.setOpenMode(SQLiteOpenMode.NOMUTEX)
    return config.createConnection("jdbc:sqlite:$databaseFilePath").also {
        setConnectionSettings(it, safetyLevel, readOnly)
        it.autoCommit = false
    }
}

private fun setConnectionSettings(connection: Connection, safetyLevel: SafetyLevel, readOnly: Boolean) {
    connection.createStatement().use { stmt ->
        if (!readOnly) {
            stmt.execute("PRAGMA journal_mode = WAL")
        }
        stmt.execute("PRAGMA synchronous = ${safetyLevel.name}")
        stmt.execute("PRAGMA wal_autocheckpoint = 0")
        stmt.execute("PRAGMA cache_size = -16384")
        stmt.execute("PRAGMA foreign_keys = ON")
        stmt.execute("PRAGMA automatic_index = OFF")
        stmt.execute("PRAGMA secure_delete = OFF")
    }
}

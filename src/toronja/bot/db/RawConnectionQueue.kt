// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.db

import kotlinx.atomicfu.atomic
import java.util.concurrent.PriorityBlockingQueue

/**
 * A synchronized most-frequently-used priority queue for [RawConnection]s.
 *
 * When no connection is available, a new one is lazily created on demand by
 * calling [connectionFactory].
 */
internal class RawConnectionQueue(private val connectionFactory: ConnectionFactory) : AutoCloseable {
    interface Lease : AutoCloseable {
        val connection: RawConnection
    }

    fun interface ConnectionFactory {
        operator fun invoke(connectionId: Int): RawConnection
    }

    override fun close() {
        val entries = ArrayList<Entry>().also { heap.drainTo(it) }
        for (entry in entries) {
            entry.connection.close()
        }
    }

    fun acquireLease(): Lease = heap.poll()?.let { LeaseImpl(it) } ?: leaseNewConnection()

    private inner class LeaseImpl(private val entry: Entry) : Lease {
        override val connection: RawConnection get() = entry.connection

        override fun close() {
            heap += Entry(entry.timesUsed + 1, entry.connection)
        }
    }

    private class Entry(val timesUsed: Long, val connection: RawConnection) : Comparable<Entry> {
        // NB: compared in the opposite order to get a max heap.
        override fun compareTo(other: Entry): Int = other.timesUsed.compareTo(timesUsed)
    }

    private fun leaseNewConnection(): Lease {
        val newConnection = connectionFactory(nextConnectionId.incrementAndGet())
        return LeaseImpl(Entry(0, newConnection))
    }

    private val heap = PriorityBlockingQueue<Entry>()
    private val nextConnectionId = atomic(0)
}

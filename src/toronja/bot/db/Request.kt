// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.db

internal interface Request<T> {
    /**
     * Execute the request's SQL statements against a [RawConnection].
     *
     * Note that this is executed in the context of a [ConnectionPool], and
     * thus can safely use the connection without additional synchronization.
     */
    operator fun invoke(connection: RawConnection): T

    /**
     * Return true if the database transaction should be committed after
     * successful execution of the request.
     *
     * The default implementation always returns true, which is suitable for
     * most request types.
     */
    val shouldCommit: Boolean get() = true
}

internal inline fun <T> request(
    crossinline toString: () -> String, crossinline body: (connection: RawConnection) -> T
): Request<T> = object : Request<T> {
    override fun toString(): String = toString()
    override fun invoke(connection: RawConnection): T = body(connection)
}

// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.db

import toronja.bot.db.utils.bind
import toronja.utils.generateTemporaryId
import java.sql.PreparedStatement

internal class TemporaryIntegerTable(connection: RawConnection) : AutoCloseable {
    override fun close() {
        addIntegerStatement.close()
        dropTableStatement.execute()
        dropTableStatement.close()
    }

    operator fun plusAssign(integer: Long) {
        addIntegerStatement.bind()(integer)
        addIntegerStatement.executeUpdate()
    }

    val name = "temp.int_table_%016x".format(generateTemporaryId())
    private val dropTableStatement: PreparedStatement
    private val addIntegerStatement: PreparedStatement

    init {
        connection.prepareUncached("CREATE TABLE $name (value INTEGER NOT NULL PRIMARY KEY)")
            .use(PreparedStatement::execute)
        dropTableStatement = connection.prepareUncached("DROP TABLE $name")
        addIntegerStatement =
            connection.prepareUncached("INSERT OR FAIL INTO $name(value) VALUES (?) ON CONFLICT(value) DO NOTHING")
    }
}

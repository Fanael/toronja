// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.db.utils

import toronja.utils.Snowflake
import java.sql.PreparedStatement
import java.sql.ResultSet

internal fun PreparedStatement.getGeneratedRowid(): Long = generatedKeys.use { it.getLong(1) }

internal inline fun <T> PreparedStatement.withQueryResult(function: ResultSet.() -> T): T = executeQuery().use(function)

internal inline fun <T> PreparedStatement.forEachRow(function: ResultSet.() -> T): Unit = withQueryResult {
    while (next()) {
        function(this)
    }
}

internal inline fun <T> PreparedStatement.mapRows(startingCapacity: Int = 0, function: ResultSet.() -> T): List<T> =
    ArrayList<T>(startingCapacity).also { forEachRow { it += function(this) } }

internal inline fun <T> PreparedStatement.mapSingle(function: ResultSet.() -> T): T =
    mapSingleOrNull(function) ?: mapSingleNoRowsError()

internal inline fun <T> PreparedStatement.mapSingleOrNull(function: ResultSet.() -> T): T? =
    withQueryResult { if (next()) function(this) else null }

internal fun <T> ResultSet.getSnowflake(columnIndex: Int): Snowflake<T> = Snowflake(getLong(columnIndex))

internal fun ResultSet.getLongOrNull(columnIndex: Int): Long? = getLong(columnIndex).takeUnless { wasNull() }

internal fun <T> ResultSet.getSnowflakeOrNull(columnIndex: Int): Snowflake<T>? =
    getLongOrNull(columnIndex)?.let { Snowflake(it) }

private fun mapSingleNoRowsError(): Nothing =
    throw NoSuchElementException("ResultSet that should have rows doesn't have any")

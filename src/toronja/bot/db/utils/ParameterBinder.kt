// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.db.utils

import toronja.utils.Snowflake
import java.sql.PreparedStatement

internal class ParameterBinder(private val statement: PreparedStatement) {
    operator fun <T> invoke(snowflake: Snowflake<T>): ParameterBinder =
        bindOne { setLong(parameterIndex, snowflake.rawValue) }

    operator fun invoke(integer: Long): ParameterBinder = bindOne { setLong(parameterIndex, integer) }

    operator fun invoke(string: String): ParameterBinder = bindOne { setString(parameterIndex, string) }

    private inline fun bindOne(function: PreparedStatement.() -> Unit): ParameterBinder {
        function(statement)
        parameterIndex += 1
        return this
    }

    private var parameterIndex = 1
}

internal fun PreparedStatement.bind(): ParameterBinder = ParameterBinder(this)

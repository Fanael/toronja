// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.events

import net.dv8tion.jda.api.events.GenericEvent

internal interface EventFilter<in E : GenericEvent> {
    /**
     * Return false iff the event shouldn't even be dispatched to
     * the executor.
     *
     * If true is returned, the event handler is always called, without ever
     * consulting [shouldHandle].
     *
     * Since this method runs on the caller thread, which is almost always
     * a JDA websocket reader thread, it should avoid doing anything more
     * complicated than reading in-memory data, it should *never* block.
     * In particular, database accesses are disallowed.
     *
     * When in doubt, return null and implement actual filtering logic
     * in [shouldHandle].
     */
    fun shouldHandleFastPath(event: E): Boolean?

    /**
     * Return false iff the event handler shouldn't be called.
     *
     * This method runs on an executor thread, so it can safely
     * do arbitrarily complicated processing.
     *
     * It is called only if [shouldHandleFastPath] returned null.
     */
    fun shouldHandle(event: E): Boolean

    object Always : EventFilter<GenericEvent> {
        override fun shouldHandleFastPath(event: GenericEvent): Boolean? = true
        override fun shouldHandle(event: GenericEvent): Boolean = true
    }
}

// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.events

import net.dv8tion.jda.api.events.GenericEvent
import net.dv8tion.jda.api.hooks.IEventManager
import toronja.utils.makeLogger
import java.util.concurrent.Executor

internal fun interface EventCallback<E : GenericEvent> {
    operator fun invoke(event: E)
}

internal interface EventSubscriber {
    fun registerEvents(listener: EventListener)
}

/**
 * A JDA [IEventManager] that dispatches events to all observers that
 * subscribed to a given type of event, dynamically matching the runtime type
 * of an event against the registered type.
 *
 * The actual execution of the observer function is done under the given
 * [executor], to avoid doing too much work on JDA websocket reader threads.
 */
internal class EventListener(private val executor: Executor) : IEventManager {
    override fun register(ignored: Any): Unit = throw UnsupportedOperationException()
    override fun unregister(ignored: Any): Unit = throw UnsupportedOperationException()
    override fun getRegisteredListeners(): List<Any> = throw UnsupportedOperationException()

    override fun handle(event: GenericEvent) {
        try {
            EventTypeAncestors[event.javaClass].forEach { ancestor -> observers[ancestor]?.forEach { it(event) } }
        } catch (@Suppress("TooGenericExceptionCaught") e: Exception) {
            // Catching Exception is fine here because IEventManager::handle
            // should not throw exceptions.
            // NB: only catch Exception and let Error propagate into JDA,
            // because Error signifies an abnormal situation programs shouldn't
            // be expected to handle.
            logger.error(e) { "Uncaught exception when dispatching an event" }
        }
    }

    inline fun <reified E : GenericEvent> subscribe(callback: EventCallback<E>) {
        subscribe(E::class.java, EventFilter.Always, callback)
    }

    inline fun <reified E : GenericEvent> subscribe(filter: EventFilter<E>, callback: EventCallback<E>) {
        subscribe(E::class.java, filter, callback)
    }

    fun <E : GenericEvent> subscribe(clazz: Class<E>, filter: EventFilter<E>, callback: EventCallback<E>) {
        observers.getOrPut(clazz, ::ArrayList) += Observer(filter, callback)
    }

    private val observers = HashMap<Class<*>, ArrayList<Observer<*>>>()

    private inner class Observer<E : GenericEvent>(
        private val filter: EventFilter<E>, private val callback: EventCallback<E>
    ) {
        operator fun invoke(rawEvent: GenericEvent) {
            @Suppress("Unchecked_Cast")
            val event = rawEvent as E
            // NB: filter and callback are captured into locals to avoid
            // holding a reference to this Observer in the Runnable.
            val filter = this.filter
            val callback = this.callback
            when (filter.shouldHandleFastPath(event)) {
                null -> executor.execute { if (filter.shouldHandle(event)) callback(event) }
                true -> executor.execute { callback(event) }
                else -> {}
            }
        }
    }

    private object EventTypeAncestors : ClassValue<Array<out Class<*>>>() {
        override fun computeValue(clazz: Class<*>): Array<out Class<*>> {
            val ancestors = ArrayList<Class<*>>()
            var currentClass = clazz
            while (GenericEvent::class.java.isAssignableFrom(currentClass)) {
                ancestors += currentClass
                currentClass = currentClass.superclass ?: break
            }
            return ancestors.toTypedArray()
        }
    }
}

private val logger = makeLogger<EventListener>()

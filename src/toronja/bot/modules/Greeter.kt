// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.modules

import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.Role
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.events.guild.GuildBanEvent
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent
import net.dv8tion.jda.api.events.guild.member.GuildMemberRemoveEvent
import net.dv8tion.jda.api.exceptions.PermissionException
import toronja.bot.events.EventListener
import toronja.bot.events.EventSubscriber
import toronja.bot.settings.ArrivalChannel
import toronja.bot.settings.AutoRole
import toronja.bot.settings.BanMessage
import toronja.bot.settings.GuildSetting
import toronja.bot.settings.GuildSettings
import toronja.bot.settings.LeaveMessage
import toronja.bot.settings.WelcomeBackMessage
import toronja.bot.settings.WelcomeMessage
import toronja.utils.ReadMostlySyncReference
import toronja.utils.Snowflake
import toronja.utils.collections.persistent.BTreeMap
import toronja.utils.idTyped
import toronja.utils.makeLogger

internal class Greeter(
    private val guildSettings: GuildSettings, private val roleRestorer: RoleRestorer
) : EventSubscriber {
    override fun registerEvents(listener: EventListener) {
        listener.subscribe<GuildMemberJoinEvent> { greet(it.guild, it.member) }
        listener.subscribe<GuildMemberRemoveEvent> { sendLeaveMessage(it.guild, it.user) }
        listener.subscribe<GuildBanEvent> { sendBanMessage(it.guild, it.user) }
    }

    private fun greet(guild: Guild, member: Member) {
        // Ignore bots.
        if (member.user.isBot) return
        if (roleRestorer.isEnabledIn(guild.idTyped)) {
            restoreUserRoles(guild, member)
        } else {
            sendMessageInArrivalChannel(guild, WelcomeMessage, member.asMention)
        }
    }

    private fun sendLeaveMessage(guild: Guild, user: User) {
        // Ignore bots and banned users.
        if (user.isBot || recentBans.get()[guild.idTyped] == user.idTyped) return
        sendMessageInArrivalChannel(guild, LeaveMessage, user.asTag)
    }

    private fun sendBanMessage(guild: Guild, user: User) {
        sendMessageInArrivalChannel(guild, BanMessage, user.asTag)
        recentBans.update { it + Pair(guild.idTyped, user.idTyped) }
    }

    private fun restoreUserRoles(guild: Guild, member: Member) {
        val guildId = guild.idTyped
        val userId = member.user.idTyped
        val memberMention = member.asMention
        val savedRoles = roleRestorer.getSavedRoles(guildId, userId)
        if (savedRoles.isEmpty()) {
            addAutoRole(guild, member)
            sendMessageInArrivalChannel(guild, WelcomeMessage, memberMention)
        } else {
            val botMember = guild.selfMember
            val assignableRoles = savedRoles.mapNotNull { role ->
                guild.getRoleById(role.rawValue).takeIf { it != null && !it.isManaged && botMember.canInteract(it) }
            }
            val preservedManagedRoles = member.roles.filter(Role::isManaged)
            try {
                guild.modifyMemberRoles(member, assignableRoles + preservedManagedRoles)
                    .reason("Restoring old roles after rejoining")
                    .queue()
                sendMessageInArrivalChannel(guild, WelcomeBackMessage, memberMention)
            } catch (e: PermissionException) {
                logger.warn(e) { "Failed to restore roles for user $userId in guild $guildId" }
            }
        }
    }

    private fun sendMessageInArrivalChannel(guild: Guild, messageSetting: GuildSetting<String>, userMention: String) {
        val guildId = guild.idTyped
        val message = guildSettings[guildId, messageSetting] ?: return
        val arrivalChannelId = guildSettings[guildId, ArrivalChannel] ?: return
        val arrivalChannel = guild.getTextChannelById(arrivalChannelId.rawValue) ?: return
        arrivalChannel.sendMessage(message.replace("\${USER}", userMention)).queue()
    }

    private fun addAutoRole(guild: Guild, member: Member) {
        val autoRoleId = guildSettings[guild.idTyped, AutoRole] ?: return
        val autoRole = guild.getRoleById(autoRoleId.rawValue) ?: return
        guild.addRoleToMember(member, autoRole).reason("Auto-role").queue()
    }

    // Discord sends the ban event first, then a leave event for the same user.
    // To distinguish bans from leaves, store the last banned user in each
    // guild.
    private val recentBans = ReadMostlySyncReference(BTreeMap<Snowflake<Guild>, Snowflake<User>>())
}

private val logger = makeLogger<Greeter>()

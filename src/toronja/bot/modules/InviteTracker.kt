// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.modules

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.supervisorScope
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Invite
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.events.ReadyEvent
import net.dv8tion.jda.api.events.ReconnectedEvent
import net.dv8tion.jda.api.events.guild.invite.GuildInviteCreateEvent
import net.dv8tion.jda.api.events.guild.invite.GuildInviteDeleteEvent
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent
import net.dv8tion.jda.api.exceptions.PermissionException
import toronja.bot.events.EventListener
import toronja.bot.events.EventSubscriber
import toronja.bot.modules.base.SettingControlledModule
import toronja.bot.modules.base.ToggleableModule
import toronja.bot.modules.base.subscribeIfEnabled
import toronja.bot.settings.GuildSettings
import toronja.bot.settings.InviteTrackingChannel
import toronja.bot.settings.SettingChangeType
import toronja.utils.ReadMostlySyncReference
import toronja.utils.Snowflake
import toronja.utils.collections.persistent.BTreeMap
import toronja.utils.completeSuspending
import toronja.utils.idTyped
import toronja.utils.launchUndispatched
import toronja.utils.makeLogger
import toronja.utils.undispatchedAsync
import java.time.Instant
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

internal class InviteTracker(
    private val guildSettings: GuildSettings, private val coroutineScope: CoroutineScope
) : ToggleableModule by SettingControlledModule("invite-tracker", guildSettings, InviteTrackingChannel),
    EventSubscriber {
    override fun registerEvents(listener: EventListener) {
        listener.subscribe<ReadyEvent> { refreshInviteLists(it.jda.guilds) }
        listener.subscribe<ReconnectedEvent> { refreshInviteLists(it.jda.guilds) }
        listener.subscribe<InviteTrackingChannel.ChangeEvent> { updateStateAfterSettingChange(it) }
        listener.subscribeIfEnabled<GuildMemberJoinEvent>(this) { reportUsedInvite(it.member, it.guild) }
        listener.subscribeIfEnabled<GuildInviteCreateEvent>(this) { rememberCreatedInvite(it.guild, it.invite) }
        listener.subscribeIfEnabled<GuildInviteDeleteEvent>(this) { deleteRememberedInvite(it.guild, it.code) }
    }

    private fun reportUsedInvite(member: Member, guild: Guild) {
        // Check this first, to avoid doing useless work if it's not a valid
        // channel.
        val destinationChannel = getDestinationChannel(guild) ?: return
        val memberMention = member.asMention
        coroutineScope.launchUndispatched { reportUsedInviteImpl(memberMention, guild, destinationChannel) }
    }

    private suspend fun reportUsedInviteImpl(memberMention: String, guild: Guild, destinationChannel: TextChannel) {
        val guildId = guild.idTyped
        val oldKnownInvites = knownInvites.get()[guildId] ?: return
        val newKnownInvites = refreshGuildInvites(guild) ?: return
        val candidateInvites = findCandidateInvites(guildId, oldKnownInvites, newKnownInvites)
        val timestamp = Instant.now().atOffset(ZoneOffset.UTC).format(DateTimeFormatter.RFC_1123_DATE_TIME)
        val message = when (candidateInvites.size) {
            0 -> "[$timestamp] User $memberMention joined from an unknown invite (likely an expired one)"
            1 -> with(candidateInvites[0]) {
                val inviterTag = inviter!!.asTag
                val creationTime = timeCreated.format(DateTimeFormatter.RFC_1123_DATE_TIME)
                val useCountMessage = when (uses) {
                    0 -> "was a single use invite"
                    else -> "used $uses times total"
                }
                "[$timestamp] User $memberMention joined from: **$code**," +
                    " created by **$inviterTag** at $creationTime, $useCountMessage"
            }
            else -> {
                val inviteCodes = candidateInvites.joinToString(", ") { it.code }
                "[$timestamp] User $memberMention joined from one of: **$inviteCodes**"
            }
        }
        destinationChannel.sendMessage(message).queue()
    }

    private fun findCandidateInvites(
        guild: Snowflake<Guild>, oldKnownInvites: InviteMap, newKnownInvites: InviteMap
    ): List<Invite> {
        fun hasOneUseLeft(invite: Invite): Boolean = invite.uses + 1 == invite.maxUses

        val result = ArrayList<Invite>()
        newKnownInvites.mapNotNullTo(result) { (_, newInvite) ->
            when (val oldInvite = oldKnownInvites[newInvite.code]) {
                null -> newInvite.takeIf { it.uses > 0 }
                else -> newInvite.takeIf { it.uses > oldInvite.uses }
            }
        }
        oldKnownInvites.mapNotNullTo(result) { (_, invite) ->
            invite.takeIf { hasOneUseLeft(it) && newKnownInvites[invite.code] == null }
        }
        val recentlyDeleted = recentlyDeletedInvites.get()[guild]
        if (recentlyDeleted != null && hasOneUseLeft(recentlyDeleted)) {
            if (recentlyDeleted !in result) {
                result += recentlyDeleted
            }
            // Forget about that invite now that we found what (likely) caused
            // it to delete itself.
            recentlyDeletedInvites.update { it - guild }
        }
        return result
    }

    private fun getDestinationChannel(guild: Guild): TextChannel? {
        val channelId = guildSettings[guild.idTyped, InviteTrackingChannel] ?: return null
        return guild.getTextChannelById(channelId.rawValue)
    }

    private fun rememberCreatedInvite(guild: Guild, invite: Invite) {
        val guildId = guild.idTyped
        val inviteCode = invite.code
        logger.debug { "Remembering invite $inviteCode in guild $guildId" }
        knownInvites.update { map ->
            val knownGuildInvites = map[guildId] ?: return@update map
            map + Pair(guildId, knownGuildInvites + Pair(inviteCode, invite))
        }
    }

    private fun deleteRememberedInvite(guild: Guild, inviteCode: String) {
        val guildId = guild.idTyped
        // Check if we know the invite before modifying knownInvites to avoid
        // locking if we don't know it after all.
        val guildInvites = knownInvites.get()[guildId] ?: return
        val invite = guildInvites[inviteCode] ?: return
        logger.debug { "Removing remembered invite $inviteCode in guild $guildId" }
        knownInvites.update { map ->
            val knownGuildInvites = map[guildId] ?: return@update map
            map + Pair(guildId, knownGuildInvites - inviteCode)
        }
        // Move the invite to the recently-deleted list to allow its last use
        // to be reported even if we received the invite deletion event first.
        recentlyDeletedInvites.update { it + Pair(guildId, invite) }
    }

    private fun refreshInviteLists(guilds: List<Guild>) {
        val (enabledGuilds, disabledGuilds) = guilds.partition { isEnabledIn(it.idTyped) }
        knownInvites.update { removeGuilds(it, disabledGuilds) }
        logger.debug { "Refreshing invite lists of ${enabledGuilds.size} guilds" }
        coroutineScope.launchUndispatched {
            val newInvites = getKnownInvitesIn(enabledGuilds)
            knownInvites.update { oldKnownInvites ->
                newInvites.fold(oldKnownInvites) { map, guildInvites ->
                    map + Pair(guildInvites.guild, guildInvites.invitesByCode)
                }
            }
            logger.debug { "Invite list refresh finished" }
        }
    }

    private fun updateStateAfterSettingChange(event: InviteTrackingChannel.ChangeEvent) {
        val guild = event.guild
        val guildId = guild.idTyped
        when (event.changeType) {
            SettingChangeType.REMOVAL -> {
                knownInvites.update { it - guildId }
            }
            SettingChangeType.MODIFICATION -> {
                // Check if we know this guild first. No need to retrieve the
                // current invite list if we already know about this guild and
                // only the channel has changed.
                if (knownInvites.get()[guildId] != null) {
                    return
                }
                // We don't know this guild yet, need to retrieve the current
                // invite list.
                coroutineScope.launchUndispatched { refreshGuildInvites(guild) }
            }
        }
    }

    private suspend fun refreshGuildInvites(guild: Guild): InviteMap? {
        val guildId = guild.idTyped
        logger.debug { "Loading current invite list of guild $guildId" }
        val newKnownInvites = getInvitesOfGuild(guild) ?: return null
        logger.debug { "Loading of current invite list of guild $guildId finished" }
        knownInvites.update { it + Pair(guildId, newKnownInvites) }
        return newKnownInvites
    }

    private val knownInvites = ReadMostlySyncReference(GuildInviteMap())
    private val recentlyDeletedInvites = ReadMostlySyncReference(BTreeMap<Snowflake<Guild>, Invite>())
}

private class GuildInvites(val guild: Snowflake<Guild>, val invitesByCode: InviteMap)

private suspend fun getKnownInvitesIn(guilds: List<Guild>): List<GuildInvites> = supervisorScope {
    guilds.map { guild ->
        undispatchedAsync {
            getInvitesOfGuild(guild)?.let { GuildInvites(guild.idTyped, it) }
        }
    }.awaitAll().filterNotNull()
}

private suspend fun getInvitesOfGuild(guild: Guild): InviteMap? {
    val guildId = guild.idTyped
    return try {
        makeInviteMap(guild.retrieveInvites().completeSuspending())
    } catch (e: PermissionException) {
        // Protect against misconfigured guilds that don't grant the required
        // permissions to the bot by silently dropping them.
        logger.warn(e) { "Permission error while trying to get the invite list in guild $guildId" }
        null
    }
}

private fun makeInviteMap(invites: List<Invite>): InviteMap =
    invites.fold(InviteMap()) { map, invite -> map + Pair(invite.code, invite) }

private fun removeGuilds(guildInviteMap: GuildInviteMap, guilds: Iterable<Guild>): GuildInviteMap =
    guilds.fold(guildInviteMap) { map, guild -> map - guild.idTyped }

private typealias InviteMap = BTreeMap<String, Invite>
private typealias GuildInviteMap = BTreeMap<Snowflake<Guild>, InviteMap>

private val logger = makeLogger<InviteTracker>()

// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.modules

import net.dv8tion.jda.api.entities.Guild
import toronja.bot.db.ConnectionPool
import toronja.bot.db.RawConnection
import toronja.bot.db.request
import toronja.bot.db.utils.bind
import toronja.bot.db.utils.mapRows
import toronja.bot.db.utils.mapSingleOrNull
import toronja.bot.modules.base.PersistedModule
import toronja.bot.modules.base.ToggleableModule
import toronja.bot.modules.base.ToggleableModuleFactory
import toronja.utils.Snowflake
import toronja.utils.makeLogger

internal class Library(
    private val pool: ConnectionPool,
    toggleableModuleFactory: ToggleableModuleFactory = PersistedModule.Factory
) : ToggleableModule by toggleableModuleFactory(pool, "library") {
    suspend fun defineCategory(guild: Snowflake<Guild>, categoryName: String): Boolean {
        logger.info { "Creating library category $categoryName in guild $guild" }
        return pool.write(request({ "CreateLibraryCategory(guild=$guild, category='$categoryName')" }) { connection ->
            val statement = connection.prepare(
                "INSERT OR FAIL INTO library_categories(guild_id, category_name)" +
                    " VALUES (?, ?) ON CONFLICT (guild_id, category_name) DO NOTHING"
            )
            statement.bind()(guild)(categoryName)
            statement.executeUpdate() > 0
        })
    }

    suspend fun removeCategory(guild: Snowflake<Guild>, categoryName: String): Boolean {
        logger.info { "Removing library category $categoryName in guild $guild" }
        return pool.write(request({ "RemoveLibraryCategory(guild=$guild, category='$categoryName')" }) { connection ->
            val statement =
                connection.prepare("DELETE FROM library_categories WHERE guild_id = ? AND category_name = ?")
            statement.bind()(guild)(categoryName)
            statement.executeUpdate() > 0
        })
    }

    suspend fun renameCategory(guild: Snowflake<Guild>, categoryName: String, newName: String): Boolean {
        logger.info { "Renaming library category $categoryName in guild $guild to $newName " }
        return pool.write(request(
            { "RenameLibraryCategory(guild=$guild, category='$categoryName', to='$newName')" }
        ) { connection ->
            val statement = connection.prepare(
                "UPDATE library_categories SET category_name = ? WHERE guild_id = ? AND category_name = ?"
            )
            statement.bind()(newName)(guild)(categoryName)
            statement.executeUpdate() > 0
        })
    }

    fun getCategoryNames(guild: Snowflake<Guild>): List<String> =
        pool.read(request({ "GetLibraryCategories(guild=$guild)" }) { connection ->
            val statement = connection.prepare(
                "SELECT category_name FROM library_categories WHERE guild_id = ?" +
                    " ORDER BY category_name"
            )
            statement.bind()(guild)
            statement.mapRows(startingCapacity = 16) { getString(1) }
        })

    fun getTopicNamesInCategory(guild: Snowflake<Guild>, categoryName: String): TopicResult<List<String>> =
        pool.read(request({ "GetLibraryTopics(guild=$guild, category='$categoryName')" }) { connection ->
            val category = findCategory(guild, categoryName, connection) ?: return@request TopicResult.NoSuchCategory
            TopicResult.Result(category.getTopicNames())
        })

    suspend fun updateTopic(
        guild: Snowflake<Guild>, categoryName: String, topicName: String, newContents: String
    ): TopicResult<Unit> {
        logger.info { "Updating library topic $topicName in category $categoryName in guild $guild" }
        return pool.write(request(
            { "UpdateLibraryTopic(guild=$guild, category='$categoryName', topic='$topicName', …)" }
        ) { connection ->
            val category = findCategory(guild, categoryName, connection) ?: return@request TopicResult.NoSuchCategory
            TopicResult.Result(category.updateTopic(topicName, newContents))
        })
    }

    suspend fun removeTopic(guild: Snowflake<Guild>, categoryName: String, topicName: String): TopicResult<Boolean> {
        logger.info { "Removing library topic $topicName in category $categoryName in guild $guild" }
        return pool.write(request(
            { "RemoveLibraryTopic(guild=$guild, category='$categoryName', topic='$topicName')" }
        ) { connection ->
            val category = findCategory(guild, categoryName, connection) ?: return@request TopicResult.NoSuchCategory
            TopicResult.Result(category.removeTopic(topicName))
        })
    }

    suspend fun renameTopic(
        guild: Snowflake<Guild>, categoryName: String, topicName: String, newName: String
    ): TopicResult<Boolean> {
        logger.info { "Renaming library topic $topicName in category $categoryName in guild $guild to $newName" }
        return pool.write(request(
            { "RenameLibraryTopic(guild=$guild, category='$categoryName', topic='$topicName', to='$newName')" }
        ) { connection ->
            val category = findCategory(guild, categoryName, connection) ?: return@request TopicResult.NoSuchCategory
            TopicResult.Result(category.renameTopic(topicName, newName))
        })
    }

    fun getTopic(guild: Snowflake<Guild>, categoryName: String, topicName: String): TopicResult<String?> =
        pool.read(request(
            { "GetLibraryTopic(guild=$guild, category='$categoryName', topic='$topicName')" }
        ) { connection ->
            val category = findCategory(guild, categoryName, connection) ?: return@request TopicResult.NoSuchCategory
            TopicResult.Result(category.getTopic(topicName))
        })

    sealed class TopicResult<out T> {
        object NoSuchCategory : TopicResult<Nothing>()
        data class Result<T>(val value: T) : TopicResult<T>()
    }

    // Search for the category first, in a separate SQL statement, to be able
    // to tell apart a non-existing *topic* and a non-existing *category*.
    private fun findCategory(guild: Snowflake<Guild>, categoryName: String, connection: RawConnection): Category? {
        val statement =
            connection.prepare("SELECT category_id FROM library_categories WHERE guild_id = ? AND category_name = ?")
        statement.bind()(guild)(categoryName)
        return statement.mapSingleOrNull { getLong(1) }?.let { Category(it, connection) }
    }

    // This class should *never* be exposed to the outside world because it
    // contains a RawConnection.
    private class Category(private val categoryId: Long, private val connection: RawConnection) {
        fun updateTopic(topicName: String, newContents: String) {
            val statement = connection.prepare(
                "INSERT OR FAIL INTO library_topics(category_id, name, contents)" +
                    " VALUES (?, ?, ?) ON CONFLICT (category_id, name) DO UPDATE SET contents = excluded.contents"
            )
            statement.bind()(categoryId)(topicName)(newContents)
            statement.executeUpdate()
        }

        fun removeTopic(topicName: String): Boolean {
            val statement = connection.prepare("DELETE FROM library_topics WHERE category_id = ? AND name = ?")
            statement.bind()(categoryId)(topicName)
            return statement.executeUpdate() > 0
        }

        fun renameTopic(topicName: String, newName: String): Boolean {
            val statement = connection.prepare("UPDATE library_topics SET name = ? WHERE category_id = ? AND name = ?")
            statement.bind()(newName)(categoryId)(topicName)
            return statement.executeUpdate() > 0
        }

        fun getTopicNames(): List<String> {
            val statement = connection.prepare("SELECT name FROM library_topics WHERE category_id = ? ORDER BY name")
            statement.bind()(categoryId)
            return statement.mapRows(startingCapacity = 16) { getString(1) }
        }

        fun getTopic(topicName: String): String? {
            val statement = connection.prepare("SELECT contents FROM library_topics WHERE category_id = ? AND name = ?")
            statement.bind()(categoryId)(topicName)
            return statement.mapSingleOrNull { getString(1) }
        }
    }
}

private val logger = makeLogger<Library>()

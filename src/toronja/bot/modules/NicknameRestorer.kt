// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.modules

import kotlinx.coroutines.CoroutineScope
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.events.ReadyEvent
import net.dv8tion.jda.api.events.ReconnectedEvent
import net.dv8tion.jda.api.events.guild.GuildBanEvent
import net.dv8tion.jda.api.events.guild.GuildJoinEvent
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent
import net.dv8tion.jda.api.events.guild.member.update.GuildMemberUpdateNicknameEvent
import net.dv8tion.jda.api.exceptions.PermissionException
import toronja.bot.db.ConnectionPool
import toronja.bot.db.RawConnection
import toronja.bot.db.request
import toronja.bot.db.utils.bind
import toronja.bot.db.utils.mapSingleOrNull
import toronja.bot.events.EventListener
import toronja.bot.events.EventSubscriber
import toronja.bot.modules.base.ModuleStateChanged
import toronja.bot.modules.base.PersistedModule
import toronja.bot.modules.base.ToggleableModule
import toronja.bot.modules.base.ToggleableModuleFactory
import toronja.bot.modules.base.subscribeIfEnabled
import toronja.utils.Snowflake
import toronja.utils.idTyped
import toronja.utils.launchSupervisor
import toronja.utils.launchUndispatched
import toronja.utils.makeLogger

internal class NicknameRestorer private constructor(
    private val pool: ConnectionPool,
    private val coroutineScope: CoroutineScope,
    private val toggleableModuleImpl: ToggleableModule
) : EventSubscriber, ToggleableModule by toggleableModuleImpl {
    constructor(
        pool: ConnectionPool,
        coroutineScope: CoroutineScope,
        toggleableModuleFactory: ToggleableModuleFactory = PersistedModule.Factory
    ) : this(pool, coroutineScope, toggleableModuleFactory(pool, "nickname-restorer"))

    override fun registerEvents(listener: EventListener) {
        listener.subscribe<ReadyEvent> { updateGuilds(it.jda.guilds) }
        listener.subscribe<ReconnectedEvent> { updateGuilds(it.jda.guilds) }
        listener.subscribeIfEnabled<GuildJoinEvent>(this) { launchGuildSnapshotUpdate(it.guild) }
        listener.subscribeIfEnabled<GuildMemberJoinEvent>(this) { restoreNickname(it) }
        listener.subscribeIfEnabled<GuildMemberUpdateNicknameEvent>(this) { updateUserNickname(it) }
        listener.subscribeIfEnabled<GuildBanEvent>(this) { forgetMember(it) }
    }

    override suspend fun enableIn(guild: Guild): ModuleStateChanged = toggleableModuleImpl.enableIn(guild).also {
        if (it == ModuleStateChanged.NO) return@also
        updateGuildSnapshot(guild)
    }

    override suspend fun disableIn(guild: Guild): ModuleStateChanged = toggleableModuleImpl.disableIn(guild).also {
        if (it == ModuleStateChanged.NO) return@also
        val guildId = guild.idTyped
        logger.info { "Forgetting saved nicknames in guild $guildId" }
        pool.write(request<Unit>({ "ForgetNicknames(guild=$guildId)" }) { connection ->
            val statement = connection.prepare("DELETE FROM saved_nicknames WHERE guild_id = ?")
            statement.bind()(guildId)
            statement.executeUpdate()
        })
    }

    private fun restoreNickname(event: GuildMemberJoinEvent) {
        val guild = event.guild
        val guildId = guild.idTyped
        val user = event.user.idTyped
        val storedNickname = pool.read(request({ "GetSavedNickname(guild=$guildId, user=$user)" }) { connection ->
            val statement =
                connection.prepare("SELECT nickname FROM saved_nicknames WHERE guild_id = ? AND user_id = ?")
            statement.bind()(guildId)(user)
            statement.mapSingleOrNull { getString(1) }
        }) ?: return // If we have no nickname to restore, we're done.
        try {
            guild.modifyNickname(event.member, storedNickname).reason("Restoring old nickname after rejoining").queue()
        } catch (e: PermissionException) {
            logger.warn(e) { "Failed to restore nickname for user $user in guild $guildId" }
        }
    }

    private fun updateUserNickname(event: GuildMemberUpdateNicknameEvent) {
        val guild = event.guild.idTyped
        val user = event.user.idTyped
        val newNickname = event.newNickname ?: return forgetUserNickname(guild, user)
        logger.info { "Updating saved nickname of user $user in guild $guild to '$newNickname'" }
        coroutineScope.launchUndispatched {
            pool.write(request({ "UpdateSavedNickname(guild=$guild, user=$user, …)" }) { connection ->
                NicknameUpdater(connection)(guild, user, newNickname)
            })
        }
    }

    private fun forgetMember(event: GuildBanEvent): Unit = forgetUserNickname(event.guild.idTyped, event.user.idTyped)

    private fun forgetUserNickname(guild: Snowflake<Guild>, user: Snowflake<User>) {
        logger.info { "Forgetting nickname of user $user in guild $guild" }
        coroutineScope.launchUndispatched {
            pool.write(request({ "ForgetSavedNickname(guild=$guild, user=$user)" }) { connection ->
                NicknameDeleter(connection)(guild, user)
            })
        }
    }

    private suspend fun updateGuildSnapshot(guild: Guild) {
        val guildId = guild.idTyped
        val memberNicknames = guild.members.map { MemberNickname(it.user.idTyped, it.nickname) }
        logger.info { "Updating user nicknames in guild $guildId" }
        pool.write(request({ "SaveNicknameSnapshot(guild=$guildId, …)" }) { connection ->
            val updater = NicknameUpdater(connection)
            val deleter = NicknameDeleter(connection)
            for ((user, nickname) in memberNicknames) {
                when (nickname) {
                    null -> deleter(guildId, user)
                    else -> updater(guildId, user, nickname)
                }
            }
        })
    }

    private fun launchGuildSnapshotUpdate(guild: Guild) {
        coroutineScope.launchUndispatched { updateGuildSnapshot(guild) }
    }

    private fun updateGuilds(guilds: List<Guild>) {
        val enabledGuilds = guilds.mapNotNull { guild -> guild.takeIf { isEnabledIn(it.idTyped) } }
        coroutineScope.launchSupervisor { enabledGuilds.forEach { launchUndispatched { updateGuildSnapshot(it) } } }
    }

    private class NicknameUpdater(rawConnection: RawConnection) {
        operator fun invoke(guild: Snowflake<Guild>, user: Snowflake<User>, newNickname: String) {
            statement.bind()(guild)(user)(newNickname)
            statement.executeUpdate()
        }

        private val statement = rawConnection.prepare(
            "INSERT OR FAIL INTO saved_nicknames(guild_id, user_id, nickname)" +
                " VALUES (?, ?, ?) ON CONFLICT (guild_id, user_id) DO UPDATE SET nickname = excluded.nickname"
        )
    }

    private class NicknameDeleter(rawConnection: RawConnection) {
        operator fun invoke(guild: Snowflake<Guild>, user: Snowflake<User>) {
            statement.bind()(guild)(user)
            statement.executeUpdate()
        }

        private val statement = rawConnection.prepare("DELETE FROM saved_nicknames WHERE guild_id = ? AND user_id = ?")
    }
}

private data class MemberNickname(val user: Snowflake<User>, val nickname: String?)

private val logger = makeLogger<NicknameRestorer>()

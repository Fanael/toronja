// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.modules

import kotlinx.coroutines.CoroutineScope
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Role
import net.dv8tion.jda.api.events.ReadyEvent
import net.dv8tion.jda.api.events.ReconnectedEvent
import net.dv8tion.jda.api.events.guild.GuildJoinEvent
import net.dv8tion.jda.api.events.role.RoleDeleteEvent
import net.dv8tion.jda.api.events.role.update.RoleUpdateNameEvent
import toronja.bot.db.ConnectionPool
import toronja.bot.db.RawConnection
import toronja.bot.db.TemporaryIntegerTable
import toronja.bot.db.request
import toronja.bot.db.utils.bind
import toronja.bot.db.utils.getSnowflake
import toronja.bot.db.utils.mapRows
import toronja.bot.db.utils.mapSingleOrNull
import toronja.bot.events.EventListener
import toronja.bot.events.EventSubscriber
import toronja.bot.modules.base.ModuleStateChanged
import toronja.bot.modules.base.PersistedModule
import toronja.bot.modules.base.ToggleableModule
import toronja.bot.modules.base.ToggleableModuleFactory
import toronja.bot.modules.base.subscribeIfEnabled
import toronja.utils.Snowflake
import toronja.utils.idTyped
import toronja.utils.launchSupervisor
import toronja.utils.launchUndispatched
import toronja.utils.makeLogger

internal class RoleCategorizer private constructor(
    private val pool: ConnectionPool,
    private val coroutineScope: CoroutineScope,
    private val toggleableModuleImpl: ToggleableModule
) : EventSubscriber, ToggleableModule by toggleableModuleImpl {
    constructor(
        pool: ConnectionPool,
        coroutineScope: CoroutineScope,
        toggleableModuleFactory: ToggleableModuleFactory = PersistedModule.Factory
    ) : this(pool, coroutineScope, toggleableModuleFactory(pool, "role-categorizer"))

    override fun registerEvents(listener: EventListener) {
        listener.subscribe<ReadyEvent> { updateGuilds(it.jda.guilds) }
        listener.subscribe<ReconnectedEvent> { updateGuilds(it.jda.guilds) }
        listener.subscribeIfEnabled<GuildJoinEvent>(this) { launchPruning(it.guild) }
        listener.subscribeIfEnabled<RoleUpdateNameEvent>(this) { renameRole(it) }
        listener.subscribeIfEnabled<RoleDeleteEvent>(this) { forgetRole(it.role.idTyped) }
    }

    override suspend fun enableIn(guild: Guild): ModuleStateChanged = toggleableModuleImpl.enableIn(guild).also {
        if (it == ModuleStateChanged.NO) return@also
        pruneOldRolesIn(guild)
    }

    fun getCategories(guild: Snowflake<Guild>): List<Category> =
        pool.read(request({ "ListAssignableRoleCategories(guild=$guild) " }) { connection ->
            val statement = connection.prepare(
                "SELECT category_name, description FROM assignable_role_categories" +
                    " WHERE guild_id = ? ORDER BY category_name"
            )
            statement.bind()(guild)
            statement.mapRows(startingCapacity = 8) { Category(getString(1), getString(2)) }
        })

    fun getRolesInCategory(guild: Snowflake<Guild>, categoryName: String): RoleResult<List<String>> =
        pool.read(request({ "ListAssignableRoles(guild=$guild, category='$categoryName')" }) { connection ->
            val category = findCategory(guild, categoryName, connection) ?: return@request RoleResult.CategoryNotFound
            RoleResult.Result(category.getRoleNames())
        })

    suspend fun defineCategory(guild: Snowflake<Guild>, categoryName: String, description: String) {
        logger.info { "Creating role category $categoryName in guild $guild" }
        pool.write(request<Unit>({ "CreateRoleCategory(guild=$guild, category='$categoryName', …)" }) { connection ->
            val statement = connection.prepare(
                "INSERT OR FAIL INTO assignable_role_categories(guild_id, category_name, description)" +
                    " VALUES (?, ?, ?) ON CONFLICT (guild_id, category_name)" +
                    " DO UPDATE SET description = excluded.description"
            )
            statement.bind()(guild)(categoryName)(description)
            statement.executeUpdate()
        })
    }

    suspend fun renameCategory(guild: Snowflake<Guild>, categoryName: String, newName: String): Boolean {
        logger.info { "Renaming role category $categoryName in guild $guild to $newName" }
        return pool.write(request(
            { "RenameRoleCategory(guild=$guild, category='$categoryName', newName='$newName')" }
        ) { connection ->
            val statement = connection.prepare(
                "UPDATE assignable_role_categories SET category_name = ? WHERE guild_id = ? AND category_name = ?"
            )
            statement.bind()(newName)(guild)(categoryName)
            statement.executeUpdate() > 0
        })
    }

    suspend fun removeCategory(guild: Snowflake<Guild>, categoryName: String): Boolean {
        logger.info { "Removing role category $categoryName in guild $guild" }
        return pool.write(request({ "RemoveRoleCategory(guild=$guild, category='$categoryName')" }) { connection ->
            val statement =
                connection.prepare("DELETE FROM assignable_role_categories WHERE guild_id = ? AND category_name = ?")
            statement.bind()(guild)(categoryName)
            statement.executeUpdate() > 0
        })
    }

    fun findRoleInGuild(guild: Snowflake<Guild>, roleName: String): List<Snowflake<Role>> =
        pool.read(request({ "FindRoleInGuild(guild=$guild, role='$roleName')" }) { connection ->
            val statement = connection.prepare(
                "SELECT DISTINCT role_id FROM assignable_roles" +
                    " JOIN assignable_role_categories USING (category_id) WHERE guild_id = ? AND role_name = ?"
            )
            statement.bind()(guild)(roleName)
            statement.mapRows { getSnowflake(1) }
        })

    fun findRoleInCategory(
        guild: Snowflake<Guild>, categoryName: String, roleName: String
    ): RoleResult<Snowflake<Role>?> = pool.read(request(
        { "FindRoleInCategory(guild=$guild, category='$categoryName', role='$roleName')" }
    ) { connection ->
        val category = findCategory(guild, categoryName, connection) ?: return@request RoleResult.CategoryNotFound
        RoleResult.Result(category.findRole(roleName))
    })

    suspend fun importRole(guild: Snowflake<Guild>, categoryName: String, role: Role): RoleResult<Unit> {
        require(role.guild.idTyped == guild) { "Tried to import a role ($role) into the wrong guild ($guild)!" }
        val roleName = role.name
        val roleId = role.idTyped
        logger.info { "Importing role $roleName ($roleId) into category $categoryName in guild $guild" }
        return pool.write(request(
            { "ImportRole(guild=$guild, category='$categoryName', role=('$roleName', $roleId))" }
        ) { connection ->
            val category = findCategory(guild, categoryName, connection) ?: return@request RoleResult.CategoryNotFound
            RoleResult.Result(category.importRole(roleName, roleId))
        })
    }

    suspend fun forgetRole(guild: Snowflake<Guild>, categoryName: String, roleName: String): RoleResult<Boolean> {
        logger.info { "Forgetting role $roleName in category $categoryName in guild $guild" }
        return pool.write(request(
            { "ForgetAssignableRole(guild=$guild, category='$categoryName', role='$roleName')" }
        ) { connection ->
            val category = findCategory(guild, categoryName, connection) ?: return@request RoleResult.CategoryNotFound
            RoleResult.Result(category.forgetRole(roleName))
        })
    }

    data class Category(val name: String, val description: String)

    sealed class RoleResult<out T> {
        object CategoryNotFound : RoleResult<Nothing>()
        class Result<T>(val value: T) : RoleResult<T>()
    }

    private fun renameRole(event: RoleUpdateNameEvent) {
        val oldName = event.oldName
        val newName = event.newName
        val role = event.role.idTyped
        logger.info { "Renaming role $oldName ($role) to $newName" }
        coroutineScope.launchUndispatched {
            pool.write(request<Unit>(
                { "RenameAssignableRole(role=$role, oldName='$oldName', newName='$newName')" }
            ) { connection ->
                // Delete the role with the name equal to newName first if one
                // exists, to ensure we don't try having multiple roles with
                // the same name in one category.
                val deleteStmt = connection.prepare(
                    "DELETE FROM assignable_roles WHERE role_name = ?" +
                        " AND category_id IN (SELECT category_id FROM assignable_roles WHERE role_id = ?)"
                )
                deleteStmt.bind()(newName)(role)
                deleteStmt.executeUpdate()
                val updateStmt = connection.prepare("UPDATE assignable_roles SET role_name = ? WHERE role_id = ?")
                updateStmt.bind()(newName)(role)
                updateStmt.executeUpdate()
            })
        }
    }

    private fun forgetRole(role: Snowflake<Role>) {
        logger.info { "Forgetting role $role" }
        coroutineScope.launchUndispatched {
            pool.write(request({ "ForgetAssignableRole(role=$role)" }) { connection ->
                val statement = connection.prepare("DELETE FROM assignable_roles WHERE role_id = ?")
                statement.bind()(role)
                statement.executeUpdate()
            })
        }
    }

    private suspend fun pruneOldRolesIn(guild: Guild) {
        val guildId = guild.idTyped
        logger.info { "Pruning old roles in guild $guildId" }
        val existingRoles = guild.roles.map(Role::idTyped)
        val removedRoleCount = pool.write(request({ "PruneOldAssignableRoles(guild=$guildId)" }) { connection ->
            TemporaryIntegerTable(connection).use { existingRolesTempTable ->
                existingRoles.forEach { existingRolesTempTable += it.rawValue }
                val sql = "DELETE FROM assignable_roles" +
                    " WHERE category_id IN (SELECT category_id FROM assignable_role_categories WHERE guild_id = ?)" +
                    " AND role_id NOT IN (SELECT value FROM ${existingRolesTempTable.name})"
                connection.prepareUncached(sql).use { statement ->
                    statement.bind()(guildId)
                    statement.executeUpdate()
                }
            }
        })
        logger.info { "Finished pruning guild $guildId, pruned $removedRoleCount old roles" }
    }

    private fun launchPruning(guild: Guild) {
        coroutineScope.launchUndispatched { pruneOldRolesIn(guild) }
    }

    private fun updateGuilds(guilds: List<Guild>) {
        val enabledGuilds = guilds.mapNotNull { guild -> guild.takeIf { isEnabledIn(it.idTyped) } }
        coroutineScope.launchSupervisor { enabledGuilds.forEach { launchUndispatched { pruneOldRolesIn(it) } } }
    }

    // Search for the category first, in a separate SQL statement, to be able
    // to tell apart a non-existing *role* and a non-existing *category*.
    private fun findCategory(guild: Snowflake<Guild>, categoryName: String, connection: RawConnection): DBCategory? {
        val statement = connection.prepare(
            "SELECT category_id FROM assignable_role_categories" +
                " WHERE guild_id = ? AND category_name = ?"
        )
        statement.bind()(guild)(categoryName)
        return statement.mapSingleOrNull { DBCategory(getLong(1), connection) }
    }

    // This class should *never* be exposed to the outside world because it
    // contains a RawConnection.
    private class DBCategory(private val categoryId: Long, private val connection: RawConnection) {
        fun getRoleNames(): List<String> {
            val statement =
                connection.prepare("SELECT role_name FROM assignable_roles WHERE category_id = ? ORDER BY role_name")
            statement.bind()(categoryId)
            return statement.mapRows(startingCapacity = 16) { getString(1) }
        }

        fun findRole(roleName: String): Snowflake<Role>? {
            val statement =
                connection.prepare("SELECT role_id FROM assignable_roles WHERE category_id = ? AND role_name = ?")
            statement.bind()(categoryId)(roleName)
            return statement.mapSingleOrNull { getSnowflake(1) }
        }

        fun importRole(roleName: String, roleId: Snowflake<Role>) {
            val statement =
                connection.prepare("INSERT INTO assignable_roles(category_id, role_name, role_id) VALUES (?, ?, ?)")
            statement.bind()(categoryId)(roleName)(roleId)
            statement.executeUpdate()
        }

        fun forgetRole(roleName: String): Boolean {
            val statement = connection.prepare("DELETE FROM assignable_roles WHERE category_id = ? AND role_name = ?")
            statement.bind()(categoryId)(roleName)
            return statement.executeUpdate() > 0
        }
    }
}

private val logger = makeLogger<RoleCategorizer>()

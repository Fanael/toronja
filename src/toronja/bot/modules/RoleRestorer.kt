// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.modules

import kotlinx.coroutines.CoroutineScope
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Role
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.events.ReadyEvent
import net.dv8tion.jda.api.events.ReconnectedEvent
import net.dv8tion.jda.api.events.guild.GuildBanEvent
import net.dv8tion.jda.api.events.guild.GuildJoinEvent
import net.dv8tion.jda.api.events.guild.member.GuildMemberRoleAddEvent
import net.dv8tion.jda.api.events.guild.member.GuildMemberRoleRemoveEvent
import net.dv8tion.jda.api.events.role.RoleDeleteEvent
import toronja.bot.db.ConnectionPool
import toronja.bot.db.RawConnection
import toronja.bot.db.TemporaryIntegerTable
import toronja.bot.db.request
import toronja.bot.db.utils.bind
import toronja.bot.db.utils.getSnowflake
import toronja.bot.db.utils.mapRows
import toronja.bot.events.EventListener
import toronja.bot.events.EventSubscriber
import toronja.bot.modules.base.ModuleStateChanged
import toronja.bot.modules.base.PersistedModule
import toronja.bot.modules.base.ToggleableModule
import toronja.bot.modules.base.ToggleableModuleFactory
import toronja.bot.modules.base.subscribeIfEnabled
import toronja.utils.Snowflake
import toronja.utils.idTyped
import toronja.utils.launchSupervisor
import toronja.utils.launchUndispatched
import toronja.utils.makeLogger

internal class RoleRestorer private constructor(
    private val pool: ConnectionPool,
    private val coroutineScope: CoroutineScope,
    private val toggleableModuleImpl: ToggleableModule
) : EventSubscriber, ToggleableModule by toggleableModuleImpl {
    constructor(
        pool: ConnectionPool,
        coroutineScope: CoroutineScope,
        toggleableModuleFactory: ToggleableModuleFactory = PersistedModule.Factory
    ) : this(pool, coroutineScope, toggleableModuleFactory(pool, "role-restorer"))

    override fun registerEvents(listener: EventListener) {
        listener.subscribe<ReadyEvent> { updateGuilds(it.jda.guilds) }
        listener.subscribe<ReconnectedEvent> { updateGuilds(it.jda.guilds) }
        listener.subscribeIfEnabled<GuildJoinEvent>(this) { launchGuildSnapshotUpdate(it.guild) }
        listener.subscribeIfEnabled<RoleDeleteEvent>(this) { forgetRole(it) }
        listener.subscribeIfEnabled<GuildMemberRoleAddEvent>(this) { addMemberRoles(it) }
        listener.subscribeIfEnabled<GuildMemberRoleRemoveEvent>(this) { removeMemberRoles(it) }
        listener.subscribeIfEnabled<GuildBanEvent>(this) { forgetMember(it) }
        // Note that the join event *cannot* be handled here because it has
        // to be ordered before auto-role assignment and EventListener doesn't
        // offer any way of ensuring ordering.
    }

    override suspend fun enableIn(guild: Guild): ModuleStateChanged = toggleableModuleImpl.enableIn(guild).also {
        if (it == ModuleStateChanged.NO) return@also
        updateGuildSnapshot(guild)
    }

    override suspend fun disableIn(guild: Guild): ModuleStateChanged = toggleableModuleImpl.disableIn(guild).also {
        if (it == ModuleStateChanged.NO) return@also
        val guildId = guild.idTyped
        logger.info { "Forgetting saved roles in guild $guildId" }
        pool.write(request<Unit>({ "ForgetGuildSavedRoles(guild=$guildId)" }) { connection ->
            val statement = connection.prepare("DELETE FROM saved_roles WHERE guild_id = ?")
            statement.bind()(guildId)
            statement.executeUpdate()
        })
    }

    fun getSavedRoles(guild: Snowflake<Guild>, user: Snowflake<User>): List<Snowflake<Role>> =
        pool.read(request({ "GetSavedRoles(guild=$guild, user=$user)" }) { connection ->
            val statement = connection.prepare("SELECT role_id FROM saved_roles WHERE guild_id = ? AND user_id = ?")
            statement.bind()(guild)(user)
            statement.mapRows(startingCapacity = 32) { getSnowflake(1) }
        })

    private fun forgetRole(event: RoleDeleteEvent) {
        val guild = event.guild.idTyped
        val role = event.role.idTyped
        logger.info { "Forgetting role $role in guild $guild" }
        coroutineScope.launchUndispatched {
            pool.write(request<Unit>({ "ForgetSavedRole(guild=$guild, role=$role)" }) { connection ->
                val statement = connection.prepare("DELETE FROM saved_roles WHERE guild_id = ? AND role_id = ?")
                statement.bind()(guild)(role)
                statement.executeUpdate()
            })
        }
    }

    private fun addMemberRoles(event: GuildMemberRoleAddEvent) {
        val guild = event.guild.idTyped
        val user = event.user.idTyped
        val roles = event.roles.map(Role::idTyped)
        logger.info { "Adding saved roles $roles for user $user in guild $guild" }
        coroutineScope.launchUndispatched {
            pool.write(request({ "AddMemberRoles(guild=$guild, user=$user, …)" }) { connection ->
                val saver = MemberRoleSaver(connection)
                roles.forEach { role -> saver(guild, user, role) }
            })
        }
    }

    private fun removeMemberRoles(event: GuildMemberRoleRemoveEvent) {
        val guild = event.guild.idTyped
        val user = event.member.user.idTyped
        val roles = event.roles.map(Role::idTyped)
        logger.info { "Removing saved roles $roles for user $user in guild $guild" }
        coroutineScope.launchUndispatched {
            pool.write(request({ "RemoveMemberRoles(guild=$guild, user=$user, …)" }) { connection ->
                val statement =
                    connection.prepare("DELETE FROM saved_roles WHERE guild_id = ? AND user_id = ? AND role_id = ?")
                for (role in roles) {
                    statement.bind()(guild)(user)(role)
                    statement.executeUpdate()
                }
            })
        }
    }

    private fun forgetMember(event: GuildBanEvent) {
        val guild = event.guild.idTyped
        val user = event.user.idTyped
        logger.info { "Forgetting roles of user $user in guild $guild" }
        coroutineScope.launchUndispatched {
            pool.write(request<Unit>({ "ForgetMemberRoles(guild=$guild, user=$user)" }) { connection ->
                val statement = connection.prepare("DELETE FROM saved_roles WHERE guild_id = ? AND user_id = ?")
                statement.bind()(guild)(user)
                statement.executeUpdate()
            })
        }
    }

    private suspend fun updateGuildSnapshot(guild: Guild) {
        val guildId = guild.idTyped
        logger.info { "Starting guild snapshot of $guildId" }
        val existingRoles = guild.roles.map(Role::idTyped)
        val memberRoles = guild.members.map { member ->
            MemberRoles(member.user.idTyped, member.roles.map(Role::idTyped))
        }
        val removedRoleCount = pool.write(request({ "SaveRoleSnapshot(guild=$guildId, …)" }) { connection ->
            val removedRoleCount = forgetOldRolesInGuild(connection, guildId, existingRoles)
            updateMemberRolesInGuild(connection, guildId, memberRoles)
            removedRoleCount
        })
        logger.info { "Finished guild snapshot of $guildId, pruned $removedRoleCount old roles" }
    }

    private fun launchGuildSnapshotUpdate(guild: Guild) {
        coroutineScope.launchUndispatched { updateGuildSnapshot(guild) }
    }

    private fun updateGuilds(guilds: List<Guild>) {
        val enabledGuilds = guilds.mapNotNull { guild -> guild.takeIf { isEnabledIn(it.idTyped) } }
        coroutineScope.launchSupervisor { enabledGuilds.forEach { launchUndispatched { updateGuildSnapshot(it) } } }
    }

    private fun forgetOldRolesInGuild(
        connection: RawConnection, guild: Snowflake<Guild>, existingRoles: List<Snowflake<Role>>
    ): Int = TemporaryIntegerTable(connection).use { existingRolesTempTable ->
        existingRoles.forEach { role -> existingRolesTempTable += role.rawValue }
        val sql = "DELETE FROM saved_roles WHERE guild_id = ?" +
            " AND role_id NOT IN (SELECT value FROM ${existingRolesTempTable.name})"
        connection.prepareUncached(sql).use { statement ->
            statement.bind()(guild)
            statement.executeUpdate()
        }
    }

    private fun updateMemberRolesInGuild(
        connection: RawConnection,
        guild: Snowflake<Guild>,
        memberRoles: List<MemberRoles>
    ) {
        val deleteMemberRoles = connection.prepare("DELETE FROM saved_roles WHERE guild_id = ? AND user_id = ?")
        val saver = MemberRoleSaver(connection)
        memberRoles.forEach { (member, roles) ->
            deleteMemberRoles.bind()(guild)(member)
            deleteMemberRoles.executeUpdate()
            roles.forEach { role -> saver(guild, member, role) }
        }
    }

    private class MemberRoleSaver(rawConnection: RawConnection) {
        operator fun invoke(guild: Snowflake<Guild>, user: Snowflake<User>, role: Snowflake<Role>) {
            statement.bind()(guild)(user)(role)
            statement.executeUpdate()
        }

        private val statement = rawConnection.prepare(
            "INSERT OR FAIL INTO saved_roles(guild_id, user_id, role_id)" +
                " VALUES (?, ?, ?) ON CONFLICT(guild_id, user_id, role_id) DO NOTHING"
        )
    }
}

private data class MemberRoles(val user: Snowflake<User>, val roles: List<Snowflake<Role>>)

private val logger = makeLogger<RoleRestorer>()

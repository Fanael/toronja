// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.modules.base

import net.dv8tion.jda.api.entities.Guild
import toronja.bot.db.ConnectionPool
import toronja.bot.db.request
import toronja.bot.db.utils.bind
import toronja.bot.db.utils.mapSingle
import toronja.utils.PerGuildCache
import toronja.utils.Snowflake
import toronja.utils.idTyped
import toronja.utils.makeLogger

internal class PersistedModule(private val pool: ConnectionPool, override val moduleName: String) : ToggleableModule {
    override fun getCachedState(guild: Snowflake<Guild>): Boolean? = moduleStateCache[guild, moduleName]

    override fun isEnabledIn(guild: Snowflake<Guild>): Boolean =
        moduleStateCache.getOrPut(guild, moduleName) { queryDatabaseForModuleState(guild) }

    override suspend fun enableIn(guild: Guild): ModuleStateChanged {
        val guildId = guild.idTyped
        return enableModuleInDatabase(guildId).also {
            if (it == ModuleStateChanged.NO) return@also
            moduleStateCache.invalidateGuild(guildId)
        }
    }

    override suspend fun disableIn(guild: Guild): ModuleStateChanged {
        val guildId = guild.idTyped
        return disableModuleInDatabase(guildId).also {
            if (it == ModuleStateChanged.NO) return@also
            moduleStateCache.invalidateGuild(guildId)
        }
    }

    object Factory : ToggleableModuleFactory {
        override fun invoke(pool: ConnectionPool, moduleName: String): PersistedModule =
            PersistedModule(pool, moduleName)
    }

    private fun queryDatabaseForModuleState(guild: Snowflake<Guild>): Boolean =
        pool.read(request({ "IsModuleEnabled(moduleName='$moduleName', guild=$guild)" }) { connection ->
            val statement = connection.prepare(
                "SELECT EXISTS (SELECT 1 FROM enabled_modules WHERE guild_id=? AND module_name=?)"
            )
            statement.bind()(guild)(moduleName)
            statement.mapSingle { getBoolean(1) }
        })

    private suspend fun enableModuleInDatabase(guild: Snowflake<Guild>): ModuleStateChanged {
        logger.info { "Enabling module $moduleName in guild $guild" }
        return pool.write(request({ "EnableModule(moduleName='$moduleName', guild=$guild)" }) { connection ->
            val statement = connection.prepare(
                "INSERT OR FAIL INTO enabled_modules(guild_id, module_name) VALUES (?, ?)" +
                    " ON CONFLICT (guild_id, module_name) DO NOTHING"
            )
            statement.bind()(guild)(moduleName)
            ModuleStateChanged.valueOf(statement.executeUpdate() > 0)
        })
    }

    private suspend fun disableModuleInDatabase(guild: Snowflake<Guild>): ModuleStateChanged {
        logger.info { "Disabling module $moduleName in guild $guild" }
        return pool.write(request({ "DisableModule(moduleName='$moduleName', guild=$guild)" }) { connection ->
            val statement = connection.prepare("DELETE FROM enabled_modules WHERE guild_id=? AND module_name=?")
            statement.bind()(guild)(moduleName)
            ModuleStateChanged.valueOf(statement.executeUpdate() > 0)
        })
    }
}

// Cache the module state to reduce database traffic.
private val moduleStateCache = PerGuildCache<String, Boolean>()

private val logger = makeLogger<PersistedModule>()

// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.modules.base

import net.dv8tion.jda.api.entities.Guild
import toronja.bot.settings.GuildSetting
import toronja.bot.settings.GuildSettings
import toronja.utils.Snowflake

internal class SettingControlledModule(
    override val moduleName: String, private val guildSettings: GuildSettings, private val setting: GuildSetting<*>
) : ToggleableModule {
    override fun isEnabledIn(guild: Snowflake<Guild>): Boolean = guildSettings[guild, setting] != null
    override fun getCachedState(guild: Snowflake<Guild>): Boolean? = guildSettings.isSetInCache(guild, setting)
    override suspend fun enableIn(guild: Guild): ModuleStateChanged = throwStateChangeAttemptError()
    override suspend fun disableIn(guild: Guild): ModuleStateChanged = throwStateChangeAttemptError()

    private fun throwStateChangeAttemptError(): Nothing = throw UnsupportedOperationException(errorMessage)

    private val errorMessage = "The state of this module is controlled through the `${setting.name}` *setting*," +
        " it cannot be changed directly."
}

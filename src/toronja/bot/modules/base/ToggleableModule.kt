// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.modules.base

import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.events.GenericEvent
import toronja.bot.db.ConnectionPool
import toronja.bot.events.EventCallback
import toronja.bot.events.EventFilter
import toronja.bot.events.EventListener
import toronja.utils.Snowflake
import toronja.utils.idTyped
import java.lang.invoke.LambdaMetafactory
import java.lang.invoke.MethodHandles
import java.lang.invoke.MethodType

internal fun interface ToggleableModuleFactory {
    operator fun invoke(pool: ConnectionPool, moduleName: String): ToggleableModule
}

internal interface ToggleableModule : Module {
    /**
     * Return the state of this module according to the cache.
     *
     * If this module's state isn't cached yet, or there is no cache to begin
     * with, or if accessing the cache requires blocking, etc., return null.
     *
     * This method is called from [EventFilter.shouldHandleFastPath], so it
     * mustn't block or access the database ever.
     */
    fun getCachedState(guild: Snowflake<Guild>): Boolean? = null

    val moduleName: String

    suspend fun enableIn(guild: Guild): ModuleStateChanged

    suspend fun disableIn(guild: Guild): ModuleStateChanged
}

internal enum class ModuleStateChanged {
    NO,
    YES;

    companion object {
        fun valueOf(boolean: Boolean): ModuleStateChanged = if (boolean) YES else NO
    }
}

internal inline fun <reified E : GenericEvent> EventListener.subscribeIfEnabled(
    module: ToggleableModule, callback: EventCallback<E>
): Unit = subscribe(makeEventFilter(module), callback)

internal inline fun <reified E : GenericEvent> makeEventFilter(module: ToggleableModule): EventFilter<E> {
    val guildGetter = EventGuildGetterFactory(E::class.java)
    return object : EventFilter<E> {
        override fun shouldHandleFastPath(event: E): Boolean? = module.getCachedState(guildGetter(event).idTyped)
        override fun shouldHandle(event: E): Boolean = module.isEnabledIn(guildGetter(event).idTyped)
    }
}

// There's no common base class or interface for all JDA events that have
// a guild, so we have to resort to reflection instead.
// This is an object only to avoid polluting the file-local scope with
// variables used only in one function.
internal object EventGuildGetterFactory {
    interface Getter<in E : GenericEvent> {
        operator fun invoke(event: E): Guild
    }

    operator fun <E : GenericEvent> invoke(clazz: Class<E>): Getter<E> = cache.get(clazz)

    private fun generateGetter(clazz: Class<*>): Getter<*> {
        val dynamicInvokeType = MethodType.methodType(Guild::class.java, clazz)
        val callSite = LambdaMetafactory.metafactory(
            lookup,
            "invoke",
            interfaceType,
            erasedInvokeType,
            lookup.findVirtual(clazz, "getGuild", getGuildMethodType),
            dynamicInvokeType
        )
        @Suppress("Unchecked_Cast")
        return callSite.target.invokeExact() as Getter<*>
    }

    private val cache = object : ClassValue<Getter<GenericEvent>>() {
        @Suppress("Unchecked_Cast")
        override fun computeValue(type: Class<*>): Getter<GenericEvent> = generateGetter(type) as Getter<GenericEvent>
    }
    private val lookup = MethodHandles.lookup()
    private val interfaceType = MethodType.methodType(Getter::class.java)
    private val getGuildMethodType = MethodType.methodType(Guild::class.java)
    private val erasedInvokeType = MethodType.methodType(Guild::class.java, GenericEvent::class.java)
}

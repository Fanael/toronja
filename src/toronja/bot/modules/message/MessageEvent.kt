// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.modules.message

import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.User
import toronja.utils.Snowflake
import toronja.utils.idTyped
import java.time.Instant
import java.util.regex.Pattern

internal data class MessageEvent(
    val messageId: Snowflake<Message>,
    val timestamp: Instant,
    val type: Type,
    val content: Content?
) {
    data class Content(val author: Snowflake<User>, val authorTag: String, val text: String) {
        companion object {
            fun fromMessage(message: Message): Content {
                val author = message.author
                val content = message.contentDisplay
                val attachments = message.attachments.joinToString(separator = "\n") {
                    "< attachment: ${it.url.changeMediaUri()} >"
                }
                val embeds = message.embeds.joinToString(separator = "\n") { embed ->
                    val attributes = listOfNotNull(
                        embed.url?.let { "url = ${it.changeMediaUri()}" },
                        embed.title?.let { "title = '$it'" },
                        embed.description?.let { "description = '$it'" },
                        embed.footer?.text?.let { "footer = '$it'" }
                    ).joinToString(separator = " ")
                    "<embed: $attributes>"
                }
                val text = buildString(capacity = 1_000) {
                    append(content)
                    if (attachments.isNotEmpty()) {
                        append('\n')
                        append(attachments)
                    }
                    if (embeds.isNotEmpty()) {
                        append('\n')
                        append(embeds)
                    }
                }
                return Content(author.idTyped, author.asTag, text)
            }
        }
    }

    enum class Type(val databaseRepresentation: Int) {
        // Avoid changing the database representation because that's obviously
        // schema-incompatible.
        CREATE(0),
        UPDATE(1),
        DELETE(2);

        companion object {
            // There are few enough event types that linear search is fast.
            fun fromDatabaseRepresentation(value: Int): Type =
                requireNotNull(values().find { it.databaseRepresentation == value }) {
                    "Invalid message event type: $value"
                }
        }
    }
}

private fun String.changeMediaUri(): String = replaceFirst(discordCdnUriRegex, "https://media.discordapp.net/")

private val discordCdnUriRegex = Regex('^' + Pattern.quote("https://cdn.discordapp.com/"))

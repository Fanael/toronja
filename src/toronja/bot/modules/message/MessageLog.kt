// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.modules.message

import kotlinx.coroutines.CoroutineScope
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.events.message.MessageBulkDeleteEvent
import net.dv8tion.jda.api.events.message.guild.GuildMessageDeleteEvent
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent
import net.dv8tion.jda.api.events.message.guild.GuildMessageUpdateEvent
import toronja.bot.db.ConnectionPool
import toronja.bot.db.RawConnection
import toronja.bot.db.request
import toronja.bot.db.utils.bind
import toronja.bot.db.utils.getGeneratedRowid
import toronja.bot.db.utils.getSnowflake
import toronja.bot.db.utils.getSnowflakeOrNull
import toronja.bot.db.utils.mapRows
import toronja.bot.db.utils.mapSingleOrNull
import toronja.bot.events.EventListener
import toronja.bot.events.EventSubscriber
import toronja.bot.modules.base.ModuleStateChanged
import toronja.bot.modules.base.PersistedModule
import toronja.bot.modules.base.ToggleableModule
import toronja.bot.modules.base.ToggleableModuleFactory
import toronja.bot.modules.base.subscribeIfEnabled
import toronja.bot.settings.GuildSettings
import toronja.bot.settings.MessageLogChannel
import toronja.utils.Snowflake
import toronja.utils.idTyped
import toronja.utils.launchUndispatched
import toronja.utils.makeLogger
import java.sql.ResultSet
import java.time.Instant

internal class MessageLog private constructor(
    private val guildSettings: GuildSettings,
    private val pool: ConnectionPool,
    private val coroutineScope: CoroutineScope,
    private val toggleableModuleImpl: ToggleableModule
) : EventSubscriber, ToggleableModule by toggleableModuleImpl {
    constructor(
        guildSettings: GuildSettings,
        pool: ConnectionPool,
        coroutineScope: CoroutineScope,
        toggleableModuleFactory: ToggleableModuleFactory = PersistedModule.Factory
    ) : this(guildSettings, pool, coroutineScope, toggleableModuleFactory(pool, "message-log"))

    override fun registerEvents(listener: EventListener) {
        listener.subscribeIfEnabled<GuildMessageReceivedEvent>(this) { logNewMessage(it.message) }
        listener.subscribeIfEnabled<GuildMessageUpdateEvent>(this) { logUpdatedMessage(it.message) }
        listener.subscribeIfEnabled<GuildMessageDeleteEvent>(this) {
            logDeletedMessage(it.channel, Snowflake(it.messageIdLong))
        }
        listener.subscribeIfEnabled<MessageBulkDeleteEvent>(this) { logBulkDeletion(it.channel, it.messageIds) }
    }

    override suspend fun disableIn(guild: Guild): ModuleStateChanged = toggleableModuleImpl.disableIn(guild).also {
        if (it == ModuleStateChanged.NO) return@also
        val guildId = guild.idTyped
        logger.info { "Clearing message log in guild $guildId" }
        pool.write(request<Unit>({ "ClearMessageLog(guild=$guildId)" }) { connection ->
            val statement = connection.prepare("DELETE FROM message_events WHERE guild_id = ?")
            statement.bind()(guildId)
            statement.executeUpdate()
        })
    }

    fun retrieve(messageRange: MessageRange): List<MessageEvent> {
        @Suppress("MagicNumber") // Column indices aren't magic.
        fun ResultSet.getEventContent(): MessageEvent.Content? {
            val authorId = getSnowflakeOrNull<User>(4) ?: return null
            val authorTag = getString(5) ?: return null
            val text = getString(6) ?: return null
            return MessageEvent.Content(authorId, authorTag, text)
        }

        @Suppress("MagicNumber") // Column indices aren't magic.
        fun ResultSet.getEvent(): MessageEvent = MessageEvent(
            getSnowflake(1),
            Instant.ofEpochSecond(getLong(2)),
            MessageEvent.Type.fromDatabaseRepresentation(getInt(3)),
            getEventContent()
        )

        val guild = messageRange.channel.guild.idTyped
        return pool.read(request({ "GetMessageEvents(guild=$guild, range=$messageRange)" }) { connection ->
            val statement = connection.prepare(
                "SELECT" +
                    " message_id, event_timestamp, event_type, author_id, author_tag, message_text" +
                    " FROM message_events LEFT JOIN message_event_contents USING (event_id)" +
                    " WHERE guild_id = ? AND message_id IN (SELECT message_id FROM message_events" +
                    " WHERE guild_id = ?1 AND channel_id = ? AND message_id BETWEEN ? AND ? AND event_type = ?)" +
                    " ORDER BY message_id, event_id"
            )
            val channel = messageRange.channel.idTyped
            val eventType = MessageEvent.Type.CREATE.databaseRepresentation.toLong()
            statement.bind()(guild)(channel)(messageRange.start)(messageRange.endInclusive)(eventType)
            statement.mapRows(startingCapacity = 1000, function = ResultSet::getEvent)
        })
    }

    private fun logNewMessage(message: Message) {
        coroutineScope.launchUndispatched {
            logEventWithContent(message, message.timeCreated.toInstant(), MessageEvent.Type.CREATE)
        }
    }

    private fun logUpdatedMessage(message: Message) {
        val timestamp = Instant.now()
        sendUpdateEmbed(message, timestamp)
        coroutineScope.launchUndispatched { logEventWithContent(message, timestamp, MessageEvent.Type.UPDATE) }
    }

    private fun logDeletedMessage(channel: TextChannel, message: Snowflake<Message>) {
        val timestamp = Instant.now()
        val guild = channel.guild
        sendDeleteEmbed(guild, channel, message, timestamp)
        val guildId = guild.idTyped
        val channelId = channel.idTyped
        coroutineScope.launchUndispatched {
            logEvent(MessageEvent(message, timestamp, MessageEvent.Type.DELETE, null), guildId, channelId)
        }
    }

    private fun logBulkDeletion(channel: TextChannel, messageIdStrings: List<String>) {
        val timestamp = Instant.now()
        val guild = channel.guild
        sendBulkDeleteEmbed(guild, channel, messageIdStrings.size, timestamp)
        val events = messageIdStrings.map { messageIdString ->
            val message = Snowflake<Message>(messageIdString.toLong())
            MessageEvent(message, timestamp, MessageEvent.Type.DELETE, null)
        }
        val guildId = guild.idTyped
        val channelId = channel.idTyped
        coroutineScope.launchUndispatched { logEvents(events, guildId, channelId) }
    }

    private suspend fun logEventWithContent(message: Message, timestamp: Instant, type: MessageEvent.Type) {
        val content = MessageEvent.Content.fromMessage(message)
        val event = MessageEvent(message.idTyped, timestamp, type, content)
        logEvent(event, message.guild.idTyped, message.textChannel.idTyped)
    }

    private suspend fun logEvents(
        events: List<MessageEvent>, guild: Snowflake<Guild>, channel: Snowflake<TextChannel>
    ): Unit = pool.write(request({ "LogMessageEvents(count=${events.size}, …)" }) { connection ->
        events.forEach { storeEvent(connection, it, guild, channel) }
    })

    private suspend fun logEvent(event: MessageEvent, guild: Snowflake<Guild>, channel: Snowflake<TextChannel>): Unit =
        pool.write(request({ "LogMessageEvent(…)" }) { connection -> storeEvent(connection, event, guild, channel) })

    private fun storeEvent(
        connection: RawConnection, event: MessageEvent, guild: Snowflake<Guild>, channel: Snowflake<TextChannel>
    ) {
        val addEventStatement = connection.prepare(
            "INSERT OR FAIL INTO message_events" +
                "(guild_id, channel_id, message_id, event_type, event_timestamp) VALUES (?, ?, ?, ?, ?)"
        )
        val eventType = event.type.databaseRepresentation.toLong()
        addEventStatement.bind()(guild)(channel)(event.messageId)(eventType)(event.timestamp.epochSecond)
        addEventStatement.executeUpdate()
        val content = event.content ?: return
        val eventId = addEventStatement.getGeneratedRowid()
        val addContentStatement = connection.prepare(
            "INSERT OR FAIL INTO message_event_contents" +
                "(event_id, author_id, author_tag, message_text) VALUES (?, ?, ?, ?)"
        )
        addContentStatement.bind()(eventId)(content.author)(content.authorTag)(content.text)
        addContentStatement.executeUpdate()
    }

    private fun sendUpdateEmbed(message: Message, timestamp: Instant) {
        val messageChannel = message.textChannel
        val guild = messageChannel.guild
        val destination = getChannelForEmbeds(guild) ?: return
        val messageId = message.idTyped
        val guildId = guild.idTyped
        val previousText = getPreviousMessageText(guildId, messageId)?.limitLengthTo(MAX_MESSAGE_LENGTH) ?: "(unknown)"
        val newText = message.contentRaw.limitLengthTo(MAX_MESSAGE_LENGTH)
        val embed = with(EmbedBuilder()) {
            setAuthor(message.author)
            val header = "Message ${messageId.rawValue} edited in ${messageChannel.asMention}"
            val messageLink = "**[Jump to message](${buildMessageUri(guildId, messageChannel.idTyped, messageId)})**"
            setDescription("$header\n$messageLink\n**Before**: $previousText\n**After**: $newText")
            setTimestamp(timestamp)
            build()
        }
        destination.sendMessageEmbeds(embed).queue()
    }

    private fun sendDeleteEmbed(guild: Guild, channel: TextChannel, message: Snowflake<Message>, timestamp: Instant) {
        val destination = getChannelForEmbeds(guild) ?: return
        // Don't log deletions from the log channel, the nested serialized
        // embeds look funny but not very useful.
        if (channel.idTyped == destination.idTyped) return
        val previousMessageState = getPreviousMessageAuthorAndText(guild.idTyped, message)
        val author = previousMessageState?.author?.let { guild.getMemberById(it.rawValue)?.user }
        val previousText = previousMessageState?.text ?: "(unknown)"
        val embed = with(EmbedBuilder()) {
            author?.let { setAuthor(it) }
            setDescription("Message ${message.rawValue} deleted in ${channel.asMention}\n**Content**: $previousText")
            setTimestamp(timestamp)
            build()
        }
        destination.sendMessageEmbeds(embed).queue()
    }

    private fun sendBulkDeleteEmbed(guild: Guild, channel: TextChannel, count: Int, timestamp: Instant) {
        val destination = getChannelForEmbeds(guild) ?: return
        val embed = with(EmbedBuilder()) {
            setDescription("Bulk deleted $count messages in ${channel.asMention}")
            setTimestamp(timestamp)
            build()
        }
        destination.sendMessageEmbeds(embed).queue()
    }

    private fun getPreviousMessageText(guild: Snowflake<Guild>, message: Snowflake<Message>): String? =
        getPreviousMessageAuthorAndText(guild, message)?.text

    private fun getPreviousMessageAuthorAndText(
        guild: Snowflake<Guild>, message: Snowflake<Message>
    ): MessageAuthorAndText? =
        pool.read(request({ "GetPreviousMessageText(message=$message)" }) { connection ->
            val statement = connection.prepare(
                "SELECT author_id, message_text" +
                    " FROM message_event_contents WHERE event_id = (SELECT MAX(event_id) FROM message_events" +
                    " WHERE guild_id = ? AND message_id = ? AND event_type <> ?)"
            )
            statement.bind()(guild)(message)(MessageEvent.Type.DELETE.databaseRepresentation.toLong())
            statement.mapSingleOrNull { MessageAuthorAndText(getSnowflake(1), getString(2)) }
        })

    private fun getChannelForEmbeds(guild: Guild): TextChannel? =
        guildSettings[guild.idTyped, MessageLogChannel]?.let { guild.getTextChannelById(it.rawValue) }
}

private class MessageAuthorAndText(val author: Snowflake<User>, val text: String)

private val logger = makeLogger<MessageLog>()

// Leave some space for our formatting.
private const val MAX_MESSAGE_LENGTH = (MessageEmbed.TEXT_MAX_LENGTH - 550) / 2

private fun buildMessageUri(
    guild: Snowflake<Guild>, channel: Snowflake<TextChannel>, message: Snowflake<Message>
): String = "https://discordapp.com/channels/${guild.rawValue}/${channel.rawValue}/${message.rawValue}"

private fun EmbedBuilder.setAuthor(user: User) {
    setAuthor("${user.asTag} (${user.idLong})", null, user.effectiveAvatarUrl)
}

private fun String.limitLengthTo(targetLength: Int): String = if (length <= targetLength) this else {
    "${substring(0, targetLength - 1)}…"
}

// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.modules.message

import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.TextChannel
import toronja.utils.Snowflake

internal class MessageRange(
    val channel: TextChannel, start: Snowflake<Message>, endInclusive: Snowflake<Message>
) : ClosedRange<Snowflake<Message>> {
    override val start: Snowflake<Message> get() = Snowflake(rawStart)

    override val endInclusive: Snowflake<Message> get() = Snowflake(rawEndInclusive)

    override fun isEmpty(): Boolean = rawStart > rawEndInclusive

    override fun contains(value: Snowflake<Message>): Boolean = value.rawValue in rawStart..rawEndInclusive

    override fun toString(): String = "MessageRange($channel, $rawStart .. $rawEndInclusive)"

    // NB: These are stored directly as longs to avoid a compiler bug that
    // causes the byte-code of start and endInclusive getters to box them into
    // java.lang.Long instead of Snowflake.
    private val rawStart = start.rawValue
    private val rawEndInclusive = endInclusive.rawValue
}

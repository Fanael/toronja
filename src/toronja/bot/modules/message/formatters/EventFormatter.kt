// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.modules.message.formatters

import toronja.bot.modules.message.MessageEvent

/**
 * A formatter for sequences of events, that outputs the formatted message log
 * into some generic [T], typically a stream.
 */
internal interface EventFormatter<T> {
    fun formatNextEvent(event: MessageEvent)

    fun isEmpty(): Boolean

    fun writeResult(destination: T)
}

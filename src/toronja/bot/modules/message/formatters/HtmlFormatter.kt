// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.modules.message.formatters

import net.dv8tion.jda.api.entities.Message
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.w3c.dom.Node
import toronja.bot.modules.message.MessageEvent
import toronja.utils.Snowflake
import java.io.OutputStream
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.OutputKeys
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult

internal class HtmlFormatter : EventFormatter<OutputStream> {
    override fun formatNextEvent(event: MessageEvent) {
        if (lastMessage == event.messageId) {
            addEventToCurrentMessage(event)
        } else {
            newMessage(event)
        }
    }

    override fun isEmpty(): Boolean = lastMessage == null

    override fun writeResult(destination: OutputStream) {
        val transformer = TransformerFactory.newInstance().newTransformer()
        transformer.setOutputProperty(OutputKeys.METHOD, "xml")
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8")
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes")
        transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "about:legacy-compat")
        transformer.setOutputProperty(OutputKeys.INDENT, "no")
        transformer.transform(DOMSource(document), StreamResult(destination))
    }

    private fun addEventToCurrentMessage(event: MessageEvent) {
        lastMessageBuilder!!.div(cssClass = "message ${eventTypeToCssClass(event.type)}") {
            time {
                val offset = event.timestamp.atOffset(ZoneOffset.UTC)
                this["datetime"] = offset.format(DateTimeFormatter.ISO_DATE_TIME)
                this += offset.format(DateTimeFormatter.RFC_1123_DATE_TIME)
            }
            val (author, authorTag, text) = event.content ?: return@div
            div(cssClass = "author") {
                span(cssClass = "snowflake") { this += "(${author.rawValue})" }
                span(cssClass = "tag") { this += authorTag }
            }
            span(cssClass = "messageContent") { this += ": $text" }
        }
    }

    private fun newMessage(event: MessageEvent) {
        val messageId = event.messageId
        lastMessage = messageId
        eventListBuilder.li {
            val labelName = "msg${messageId.rawValue}"
            this["id"] = labelName
            lastMessageBuilder = this
            a {
                this["class"] = "snowflake"
                this["href"] = "#$labelName"
                this += messageId.rawValue.toString()
            }
        }
        addEventToCurrentMessage(event)
    }

    private var lastMessage: Snowflake<Message>? = null
    private var lastMessageBuilder: BlockElementBuilder? = null
    private val document: Document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument()
    private val eventListBuilder: ListBuilder = document.html {
        head {
            meta { this["charset"] = "UTF-8" }
            title { this += "Message log" }
            style { this += CSS }
        }
        body {
            ul {
                this["id"] = "messages"
                this
            }
        }
    }
}

private fun eventTypeToCssClass(eventType: MessageEvent.Type): String = when (eventType) {
    MessageEvent.Type.CREATE -> "created"
    MessageEvent.Type.UPDATE -> "updated"
    MessageEvent.Type.DELETE -> "deleted"
}

private inline fun <T> Document.html(block: HtmlBuilder.() -> T): T = block(HtmlBuilder(this))

@HtmlBuilderMarker
private open class TagBuilder(protected val document: Document, protected val element: Element) {
    operator fun set(name: String, value: String) {
        element.setAttribute(name, value)
    }
}

private class TextTagBuilder(document: Document, element: Element) : TagBuilder(document, element) {
    operator fun plusAssign(string: String) {
        element.appendChild(document.createTextNode(string))
    }
}

private class HtmlBuilder(parent: Document) : TagBuilder(parent, parent.appendNewElement(parent, "html")) {
    inline fun <T> head(block: HeadBuilder.() -> T): T =
        block(HeadBuilder(document, element.appendNewElement(document, "head")))

    inline fun <T> body(block: BodyBuilder.() -> T): T =
        block(BodyBuilder(document, element.appendNewElement(document, "body")))
}

private class HeadBuilder(document: Document, element: Element) : TagBuilder(document, element) {
    inline fun <T> meta(block: TagBuilder.() -> T): T =
        block(TagBuilder(document, element.appendNewElement(document, "meta")))

    inline fun <T> title(block: TextTagBuilder.() -> T): T =
        block(TextTagBuilder(document, element.appendNewElement(document, "title")))

    inline fun <T> style(block: TextTagBuilder.() -> T): T =
        block(TextTagBuilder(document, element.appendNewElement(document, "style")))
}

private class BodyBuilder(document: Document, element: Element) : TagBuilder(document, element) {
    inline fun <T> ul(block: ListBuilder.() -> T): T =
        block(ListBuilder(document, element.appendNewElement(document, "ul")))
}

private class ListBuilder(document: Document, element: Element) : TagBuilder(document, element) {
    inline fun <T> li(block: BlockElementBuilder.() -> T): T =
        block(BlockElementBuilder(document, element.appendNewElement(document, "li")))
}

private class BlockElementBuilder(document: Document, element: Element) : TagBuilder(document, element) {
    inline fun <T> div(cssClass: String, block: BlockElementBuilder.() -> T): T =
        block(BlockElementBuilder(document, element.appendNewElement(document, "div")).also { it["class"] = cssClass })

    inline fun <T> a(block: TextTagBuilder.() -> T): T =
        block(TextTagBuilder(document, element.appendNewElement(document, "a")))

    inline fun <T> time(block: TextTagBuilder.() -> T): T =
        block(TextTagBuilder(document, element.appendNewElement(document, "time")))

    inline fun <T> span(cssClass: String, block: TextTagBuilder.() -> T): T =
        block(TextTagBuilder(document, element.appendNewElement(document, "span")).also { it["class"] = cssClass })
}

private fun Node.appendNewElement(document: Document, tagName: String): Element =
    document.createElement(tagName).also { appendChild(it) }

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.SOURCE)
@DslMarker
private annotation class HtmlBuilderMarker

private const val CSS: String = """body {
    margin: 0 auto;
    width: 100em;
    min-width: 50%;
    max-width: 100%;
    font-family: sans-serif;
    color: #111;
    background-color: #eed;
}

#messages {
    list-style-type: none;
    margin: 0;
    padding: 1em;
}

#messages li {
    padding: 0.1em 0;
    border-bottom: 1px dashed #444;
}

#messages li div {
    padding: 0.2em 0;
    white-space: pre-wrap;
}

.snowflake {
    font-size: x-small;
    display: block;
    color: #555;
}

.author {
    display: inline-block;
    border: 1px dotted #444;
    padding: 0.1em;
    margin-right: 0.2em;
}

.author .tag {
    font-size: 75%;
    display: block;
}

.updated {
    background-color: #cce;
}

.deleted {
    background-color: #ecc;
}

time {
    font-size: small;
    display: block;
    color: #555;
}

.deleted time {
    font-size: larger;
}

.deleted time::before {
    content: "Deleted at: ";
}"""

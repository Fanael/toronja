// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.settings

import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Role
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.events.GenericEvent
import toronja.bot.db.utils.ParameterBinder
import toronja.bot.db.utils.getSnowflake
import toronja.utils.Snowflake
import toronja.utils.collections.persistent.BTreeMap
import java.sql.ResultSet

internal enum class SettingChangeType {
    MODIFICATION,
    REMOVAL,
}

internal sealed class GuildSetting<T> {
    abstract val name: String

    abstract val description: String

    // Intended primarily so that setting-controlled modules can actually react
    // to the change of their settings.
    open fun makeChangeEvent(changeType: SettingChangeType, guild: Guild): SettingChangeEvent? = null

    abstract fun fromResultSet(resultSet: ResultSet, columnIndex: Int): T

    abstract fun bindValue(binder: ParameterBinder, newValue: T)
}

internal abstract class SettingChangeEvent(val changeType: SettingChangeType, val guild: Guild) : GenericEvent {
    // These methods are a part of GenericEvent, but aren't actually necessary
    // for the synthesized events we're creating here.
    final override fun getJDA(): JDA = throw UnsupportedOperationException()
    final override fun getResponseNumber(): Long = throw UnsupportedOperationException()
}

internal object Registry {
    // Use a B-tree map to have the settings sorted by name.
    val settingMap: BTreeMap<String, GuildSetting<*>>

    init {
        val settings = arrayOf<GuildSetting<*>>(
            CommandPrefix,
            ArrivalChannel,
            AutoRole,
            WelcomeMessage,
            WelcomeBackMessage,
            LeaveMessage,
            BanMessage,
            InhibitUnknownCommandMessage,
            MessageLogChannel,
            InviteTrackingChannel
        )
        settingMap = settings.fold(BTreeMap()) { map, setting -> map + Pair(setting.name, setting) }
    }
}

internal abstract class ChannelSetting : GuildSetting<Snowflake<TextChannel>>() {
    final override fun bindValue(binder: ParameterBinder, newValue: Snowflake<TextChannel>) {
        binder(newValue)
    }

    final override fun fromResultSet(resultSet: ResultSet, columnIndex: Int): Snowflake<TextChannel> =
        resultSet.getSnowflake(columnIndex)
}

internal abstract class RoleSetting : GuildSetting<Snowflake<Role>>() {
    final override fun bindValue(binder: ParameterBinder, newValue: Snowflake<Role>) {
        binder(newValue)
    }

    final override fun fromResultSet(resultSet: ResultSet, columnIndex: Int): Snowflake<Role> =
        resultSet.getSnowflake(columnIndex)
}

internal abstract class StringSetting : GuildSetting<String>() {
    final override fun bindValue(binder: ParameterBinder, newValue: String) {
        binder(newValue)
    }

    final override fun fromResultSet(resultSet: ResultSet, columnIndex: Int): String = resultSet.getString(columnIndex)
}

internal abstract class UnitSetting : GuildSetting<Unit>() {
    final override fun bindValue(binder: ParameterBinder, newValue: Unit) {
        binder(0)
    }

    final override fun fromResultSet(resultSet: ResultSet, columnIndex: Int): Unit = Unit
}

internal object CommandPrefix : StringSetting() {
    override val name: String = "command-prefix"

    override val description: String = "The prefix used by the bot in this server."
}

internal object ArrivalChannel : ChannelSetting() {
    override val name: String = "arrival-channel"

    override val description: String = "The channel to send welcome and leaves message to." +
        "\n\nWhen setting, pass a channel *mention*."
}

internal object AutoRole : RoleSetting() {
    override val name: String = "auto-role"

    override val description: String = "A role automatically assigned to users who joined and had no saved roles." +
        "\n\nWhen setting, pass a role *name*. The role name must be unambiguous, that is, only one role" +
        " with that name may exist on the server."
}

internal object WelcomeMessage : StringSetting() {
    override val name: String = "welcome-message"

    override val description: String = "A message automatically sent in the arrival channel whenever someone joins." +
        "\n\nThe magic string \${USER} will be replaced with a mention of the user who just joined."
}

internal object WelcomeBackMessage : StringSetting() {
    override val name: String = "welcome-back-message"

    override val description: String = "A message automatically sent in the arrival channel whenever someone rejoins." +
        "\n\nThe magic string \${USER} will be replaced with a mention of the user who just joined."
}

internal object LeaveMessage : StringSetting() {
    override val name: String = "leave-message"

    override val description: String =
        "A message automatically sent in the arrival channel whenever someone leaves the server." +
            "\n\nThe magic string \${USER} will be replaced with the nickname of the user who left."
}

internal object BanMessage : StringSetting() {
    override val name: String = "ban-message"

    override val description: String =
        "A message automatically sent in the arrival channel whenever someone is banned." +
            "\n\nThe magic string \${USER} will be replaced with the nickname of the user who was banned."
}

internal object InhibitUnknownCommandMessage : UnitSetting() {
    override val name: String = "inhibit-unknown-command-message"

    override val description: String = "If *present*, the unknown command message will not be sent." +
        "\n\nWhen setting, the value is ignored, only the *existence* of this setting matters."
}

internal object MessageLogChannel : ChannelSetting() {
    override val name: String = "message-log-channel"

    override val description: String = "The channel to send message edit/deletion events to." +
        "\nThe `message-log` module must be enabled for this to work." +
        "\n\nWhen setting, pass a channel *mention*."
}

internal object InviteTrackingChannel : ChannelSetting() {
    override val name: String = "invite-tracking-channel"

    override val description: String = "The channel to send invite tracking information to whenever someone joins." +
        "\n\nWhen setting, pass a channel *mention*." +
        "\nDo note that this functionality requires the bot to have the manage server permission."

    override fun makeChangeEvent(changeType: SettingChangeType, guild: Guild): ChangeEvent =
        ChangeEvent(changeType, guild)

    class ChangeEvent(changeType: SettingChangeType, guild: Guild) : SettingChangeEvent(changeType, guild)
}

// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.settings

import net.dv8tion.jda.api.entities.Guild
import toronja.bot.db.ConnectionPool
import toronja.bot.db.request
import toronja.bot.db.utils.ParameterBinder
import toronja.bot.db.utils.bind
import toronja.bot.db.utils.mapSingleOrNull
import toronja.bot.events.EventListener
import toronja.bot.modules.base.Module
import toronja.utils.PerGuildCache
import toronja.utils.Snowflake
import toronja.utils.idTyped
import toronja.utils.makeLogger

internal class GuildSettings(
    private val pool: ConnectionPool, private val eventListener: EventListener
) : Module.AlwaysEnabled() {
    /**
     * Return true if the [setting] is set to some value in [guild] according
     * the cache.
     * Return false if it's unset according to the cache.
     * Return null if it's not cached.
     *
     * This function is guaranteed to be wait-free, and thus meets the
     * requirements of EventFilter::shouldHandleFastPath.
     */
    fun <T> isSetInCache(guild: Snowflake<Guild>, setting: GuildSetting<T>): Boolean? =
        settingCache[guild, setting.name]?.let { it.getChecked(setting) != null }

    operator fun <T> get(guild: Snowflake<Guild>, setting: GuildSetting<T>): T? =
        settingCache.getOrPut(guild, setting.name) { getFromDatabase(guild, setting) }.getChecked(setting)

    suspend fun <T> set(guild: Guild, setting: GuildSetting<T>, newValue: T) {
        val guildId = guild.idTyped
        logger.info { "Changing guild setting ${setting.name} in guild $guildId" }
        pool.write(request<Unit>({ "SetSettingValue(guild=$guildId, setting='${setting.name}', …)" }) { connection ->
            val statement = connection.prepare(
                "INSERT OR FAIL INTO guild_settings(guild_id, name, value) VALUES (?, ?, ?)" +
                    " ON CONFLICT (guild_id, name) DO UPDATE SET value = excluded.value"
            )
            statement.bind()(guildId)(setting.name)(BoundSetting(setting, newValue))
            statement.executeUpdate()
        })
        settingCache.invalidateGuild(guildId)
        signalEventIfNecessary(setting, SettingChangeType.MODIFICATION, guild)
    }

    suspend fun <T> removeSetting(guild: Guild, setting: GuildSetting<T>) {
        val guildId = guild.idTyped
        logger.info { "Removing setting ${setting.name} in guild $guildId" }
        pool.write(request<Unit>({ "RemoveSetting(guild=$guildId, setting='${setting.name}')" }) { connection ->
            val statement = connection.prepare("DELETE FROM guild_settings WHERE guild_id = ? AND name = ?")
            statement.bind()(guildId)(setting.name)
            statement.executeUpdate()
        })
        settingCache.invalidateGuild(guildId)
        signalEventIfNecessary(setting, SettingChangeType.REMOVAL, guild)
    }

    private fun <T> getFromDatabase(guild: Snowflake<Guild>, setting: GuildSetting<T>): CachedSetting<T> {
        return pool.read(request({ "GetSettingValue(guild=$guild, setting='${setting.name}')" }) { connection ->
            val statement = connection.prepare("SELECT value FROM guild_settings WHERE guild_id = ? AND name = ?")
            statement.bind()(guild)(setting.name)
            CachedSetting(setting, statement.mapSingleOrNull { setting.fromResultSet(this, 1) })
        })
    }

    private fun signalEventIfNecessary(setting: GuildSetting<*>, changeType: SettingChangeType, guild: Guild) {
        val event = setting.makeChangeEvent(changeType, guild) ?: return
        eventListener.handle(event)
    }

    // Cache the setting values to reduce database traffic.
    private val settingCache = PerGuildCache<String, CachedSetting<*>>()

    private class CachedSetting<T>(private val setting: GuildSetting<T>, private val value: T?) {
        fun <U> getChecked(actualSetting: GuildSetting<U>): U? {
            if (setting !== actualSetting) {
                throw AssertionError(
                    "Attempted to retrieve cached value of setting ${setting.name} using ${actualSetting.name}"
                )
            }
            @Suppress("Unchecked_Cast")
            return value as U?
        }
    }
}

private class BoundSetting<T>(val setting: GuildSetting<T>, val value: T)

private val logger = makeLogger<GuildSettings>()

private operator fun <T> ParameterBinder.invoke(setting: BoundSetting<T>): ParameterBinder {
    setting.setting.bindValue(this, setting.value)
    return this
}

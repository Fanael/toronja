// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.utils

import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.async
import kotlinx.coroutines.future.asDeferred
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.time.delay
import net.dv8tion.jda.api.requests.RestAction
import java.time.Duration

internal fun <T> RestAction<T>.submitAsync(): Deferred<T> = submit().asDeferred()
internal suspend fun <T> RestAction<T>.completeSuspending(): T = submitAsync().await()

/**
 * Launch a new supervisor job, without dispatching it to another thread until
 * the first suspension.
 */
@OptIn(ExperimentalCoroutinesApi::class)
internal fun CoroutineScope.launchSupervisor(block: suspend CoroutineScope.() -> Unit): Job =
    launch(SupervisorJob(coroutineContext[Job]), CoroutineStart.UNDISPATCHED, block)

/**
 * Launch a new coroutine, without dispatching it to another thread until
 * the first suspension.
 *
 * Our coroutines are launched from event thread pool already most of the time,
 * so this lets us avoid unnecessary packaging and dispatch if the coroutine
 * never suspends.
 */
@OptIn(ExperimentalCoroutinesApi::class)
internal fun CoroutineScope.launchUndispatched(block: suspend CoroutineScope.() -> Unit): Job =
    launch(start = CoroutineStart.UNDISPATCHED, block = block)

/**
 * Launch a new coroutine, without dispatching it to another thread until
 * the first suspension, and return its future result as a [Deferred].
 */
@OptIn(ExperimentalCoroutinesApi::class)
internal fun <T> CoroutineScope.undispatchedAsync(block: suspend CoroutineScope.() -> T): Deferred<T> =
    async(start = CoroutineStart.UNDISPATCHED, block = block)

/**
 * Run a coroutine periodically with a fixed delay of [duration], catching
 * all [Exception]s except [InterruptedException] and [CancellationException],
 * to let the periodic process try again in the future, without breaking thread
 * interruption and coroutine cancellation.
 */
internal suspend fun CoroutineScope.runWithFixedDelay(duration: Duration, block: suspend () -> Unit) {
    while (isActive) {
        delay(duration)
        try {
            block()
        } catch (@Suppress("TooGenericExceptionCaught") e: Exception) {
            if (e is InterruptedException || e is CancellationException) throw e
        }
    }
}

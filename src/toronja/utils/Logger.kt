// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.utils

import org.slf4j.LoggerFactory

/**
 * Create a logger named after the class [T].
 */
internal inline fun <reified T> makeLogger(): Logger = makeLogger(T::class.java)

internal fun <T> makeLogger(clazz: Class<T>): Logger = makeLogger(clazz.name)

/**
 * Create a logger with the given [name].
 */
internal fun makeLogger(name: String): Logger = Logger(LoggerFactory.getLogger(name))

@JvmInline
internal value class Logger(val underlying: org.slf4j.Logger) {
    val isDebugEnabled: Boolean get() = underlying.isDebugEnabled
    val isInfoEnabled: Boolean get() = underlying.isInfoEnabled
    val isWarnEnabled: Boolean get() = underlying.isWarnEnabled
    val isErrorEnabled: Boolean get() = underlying.isErrorEnabled

    inline fun debug(message: () -> String) {
        if (!isDebugEnabled) return
        underlying.debug(message())
    }

    inline fun info(message: () -> String) {
        if (!isInfoEnabled) return
        underlying.info(message())
    }

    inline fun warn(message: () -> String) {
        if (!isWarnEnabled) return
        underlying.warn(message())
    }

    inline fun warn(throwable: Throwable?, message: () -> String) {
        if (!isWarnEnabled) return
        underlying.warn(message(), throwable)
    }

    inline fun error(message: () -> String) {
        if (!isErrorEnabled) return
        underlying.error(message())
    }

    inline fun error(throwable: Throwable?, message: () -> String) {
        if (!isErrorEnabled) return
        underlying.error(message(), throwable)
    }
}

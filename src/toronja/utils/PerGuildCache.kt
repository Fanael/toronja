// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.utils

import net.dv8tion.jda.api.entities.Guild
import toronja.utils.collections.persistent.BTreeMap

/**
 * A thread-safe per-guild key-value map.
 *
 * Since it doesn't synchronize readers (it uses [ReadMostlySyncReference]
 * internally) and uses a persistent map, the key and value types need to be
 * thread-safe themselves, or at least care should be taken to not use them
 * in a thread-unsafe manner.
 */
internal class PerGuildCache<K : Comparable<K>, V> {
    operator fun get(guild: Snowflake<Guild>, key: K): V? = map.get()[guild]?.get(key)

    fun getOrPut(guild: Snowflake<Guild>, key: K, block: () -> V): V =
        get(guild, key) ?: getOrPutSlowPath(guild, key, block)

    fun invalidateGuild(guild: Snowflake<Guild>) {
        map.update { it - guild }
    }

    private fun getOrPutSlowPath(guild: Snowflake<Guild>, key: K, block: () -> V): V {
        // The entry wasn't found in the map, so we need to add it.
        // Do the computation of the value outside of the underlying lock,
        // and just let the first writer win.
        val newValue = block()
        var result = newValue
        map.update { actualMap ->
            val mapForGuild = actualMap[guild] ?: emptyLowerMap
            when (val value = mapForGuild[key]) {
                null -> // This thread won the race, update the map.
                    actualMap + Pair(guild, mapForGuild + Pair(key, newValue))
                else -> // Another thread won the race, use the value from the map.
                    actualMap.also { result = value }
            }
        }
        return result
    }

    private val map = ReadMostlySyncReference(BTreeMap<Snowflake<Guild>, BTreeMap<K, V>>())
    private val emptyLowerMap = BTreeMap<K, V>()
}

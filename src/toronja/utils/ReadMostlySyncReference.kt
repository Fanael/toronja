// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.utils

import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

/**
 * A thread-safe reference intended for read-mostly uses.
 *
 * Readers don't block, and aren't blocked by, other readers or writers.
 * Writers, on the other hand, are synchronized with one another using a lock,
 * mostly to prevent starvation that could occur if compare-and-swap was used
 * instead.
 *
 * Note that this doesn't synchronize the underlying type! Since this class's
 * primary intended use is with persistent data structures, which do not mutate
 * in place, this is not a concern.
 */
internal class ReadMostlySyncReference<T>(@Volatile private var reference: T) {
    fun get(): T = reference

    fun update(updater: (T) -> T) {
        lock.withLock {
            reference = updater(reference)
        }
    }

    private val lock = ReentrantLock(/* fair = */ true)
}

// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.utils

import net.dv8tion.jda.api.entities.ISnowflake

// Note: T is a phantom parameter here, it only exists to allow the type system
// to distinguish snowflakes of different objects.
@JvmInline
internal value class Snowflake<T>(val rawValue: Long) : Comparable<Snowflake<T>> {
    override fun compareTo(other: Snowflake<T>): Int = rawValue.compareTo(other.rawValue)

    override fun toString(): String = "Snowflake($rawValue)"
}

internal val <T : ISnowflake> T.idTyped: Snowflake<T> get() = Snowflake(idLong)

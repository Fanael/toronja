// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.utils

import kotlinx.atomicfu.atomic
import java.security.SecureRandom

/**
 * Generate a temporary identifier primarily for temporary database objects.
 *
 * This function can be safely called from multiple threads.
 *
 * The generated sequence is guaranteed to be a permutation of the set of all
 * possible values of a [Long]. It is *not* guaranteed however to stay the same
 * across executions.
 */
@Suppress("MagicNumber")
internal fun generateTemporaryId(): Long {
    var id = counter.getAndIncrement()
    id += mixConstant
    id = id xor (id ushr 30)
    id *= 0x4be98134a5976fd3
    id = id xor (id ushr 29)
    id *= 0x3bc0993a5ad19a13
    id = id xor (id ushr 31)
    id -= mixConstant
    return id
}

private val counter = atomic(0L)
private val mixConstant = SecureRandom().nextLong()

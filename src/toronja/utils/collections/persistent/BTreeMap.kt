// Copyright (C) 2019-2020 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.utils.collections.persistent

/**
 * A persistent, B-tree based map.
 *
 * It's a pretty naive, path-copying based implementation, but because it's
 * a B-tree, it performs well enough in practice and has *predictable* runtime
 * behavior unlike more complicated hash-mapped tree schemes.
 */
internal class BTreeMap<K : Comparable<K>, V> private constructor(
    private val rootNode: Node<K, V>
) : Iterable<Pair<K, V>> {
    constructor() : this(getSharedEmptyLeaf())

    fun isEmpty(): Boolean = rootNode.keys.isEmpty()

    override fun iterator(): Iterator<Pair<K, V>> = IteratorImpl(rootNode)

    operator fun get(key: K): V? {
        tailrec fun impl(node: Node<K, V>): V? {
            val (index, found) = findFirstNotSmallerThan(node.keys, key)
            @Suppress("Unchecked_Cast")
            return when {
                found -> node.values[index] as V
                node is Inner -> impl(node.children[index])
                else -> null
            }
        }

        return impl(rootNode)
    }

    operator fun plus(pair: Pair<K, V>): BTreeMap<K, V> {
        // Deconstruct the pair immediately to help JIT escape analysis.
        val (key, value) = pair

        fun impl(node: Node<K, V>): MaybeSplit<K, V> {
            val (index, found) = findFirstNotSmallerThan(node.keys, key)
            if (found) {
                // NB: we can immediately return here because this operation
                // never changes size.
                return node.withChangedValue(index, value)
            }
            val newNode: Node<K, V> = when (node) {
                is Leaf -> node.withAddedEntry(index, key, value)
                is Inner -> when (val result = impl(node.children[index])) {
                    is Node -> node.withChangedChild(index, result)
                    is Split -> node.withAddedChild(index, result)
                }
            }
            return if (newNode.keys.size > MAX_ELEMENTS_PER_NODE) newNode.split() else newNode
        }

        return BTreeMap(impl(rootNode).unpackIntoNode())
    }

    operator fun minus(key: K): BTreeMap<K, V> {
        fun rebalanceAfterDeletion(parentNode: Inner<K, V>, childIndex: Int, child: Node<K, V>): Node<K, V> {
            if (child.keys.size >= MIN_ELEMENTS_PER_NODE) {
                return parentNode.withChangedChild(childIndex, child)
            }
            // The new child is too small, we need to borrow some elements
            // from one of its siblings, or if that still leaves it too small,
            // merge it with one of its siblings.
            val leftSiblingIndex = childIndex - 1
            val rightSiblingIndex = childIndex + 1
            val leftSibling = parentNode.children.getOrNull(leftSiblingIndex)
            val rightSibling = parentNode.children.getOrNull(rightSiblingIndex)
            // Prefer to use the longer sibling for merging to make rotations
            // more common than merges.
            val longerSibling = maxOf(leftSibling, rightSibling, { x, y ->
                (x?.keys?.size ?: 0).compareTo(y?.keys?.size ?: 0)
            })!!
            val longerSiblingIndex = if (longerSibling === leftSibling) leftSiblingIndex else rightSiblingIndex
            // Now we're only interested in which child is smaller/bigger in
            // the key order, we no longer need other information.
            val smallerChildIndex = minOf(childIndex, longerSiblingIndex)
            val smallerChild = if (childIndex == smallerChildIndex) child else longerSibling
            val biggerChild = if (childIndex == smallerChildIndex) longerSibling else child
            val (separatorKey, separatorValue) = parentNode.entryAt(smallerChildIndex)
            val mergedChildren = smallerChild.mergeWith(separatorKey, separatorValue, biggerChild)
            return if (mergedChildren.keys.size > MAX_ELEMENTS_PER_NODE) {
                // Rotate the elements between the two children, keeping both
                // above MIN_ELEMENTS_PER_NODE. We can reuse node merging and
                // splitting code to do it easily.
                parentNode.withRotatedChildren(smallerChildIndex, mergedChildren.split())
            } else {
                // Merge two children into one. This can make the tree
                // shallower if the parent node we're working on is the root
                // that has just two children.
                parentNode.withMergedChildren(smallerChildIndex, mergedChildren)
            }
        }

        fun replaceWithPredecessor(node: Inner<K, V>, index: Int): Node<K, V> {
            // The key to delete was found in an inner node, need to replace
            // the value in that node with its in-order predecessor.
            fun impl(node: Node<K, V>): Triple<K, V, Node<K, V>> = when (node) {
                is Inner -> {
                    val lastIndex = node.children.lastIndex
                    val child = node.children[lastIndex]
                    val (newKey, newValue, newChild) = impl(child)
                    Triple(newKey, newValue, rebalanceAfterDeletion(node, lastIndex, newChild))
                }
                is Leaf -> {
                    val lastIndex = node.keys.lastIndex
                    val (extractedKey, extractedValue) = node.entryAt(lastIndex)
                    Triple(extractedKey, extractedValue, node.withRemovedEntry(lastIndex))
                }
            }

            val (newKey, newValue, newChild) = impl(node.children[index])
            val newNode = node.withChangedEntry(index, newKey, newValue)
            return rebalanceAfterDeletion(newNode, index, newChild)
        }

        fun impl(node: Node<K, V>): Node<K, V> {
            val (index, found) = findFirstNotSmallerThan(node.keys, key)
            return when {
                found -> when (node) {
                    is Leaf -> node.withRemovedEntry(index)
                    is Inner -> replaceWithPredecessor(node, index)
                }
                node is Inner -> {
                    val child = node.children[index]
                    val newChild = impl(child)
                    if (child === newChild) node else rebalanceAfterDeletion(node, index, newChild)
                }
                else -> node
            }
        }

        val newRoot = impl(rootNode)
        return if (newRoot === rootNode) this else BTreeMap(newRoot)
    }

    private class IteratorImpl<K : Comparable<K>, V>(treeRoot: Node<K, V>) : Iterator<Pair<K, V>> {
        override fun hasNext(): Boolean = indexInLeaf in currentLeaf.keys.indices || ancestorStack.isNotEmpty()

        override fun next(): Pair<K, V> {
            if (indexInLeaf in currentLeaf.keys.indices) {
                return currentLeaf.entryAt(indexInLeaf).also { indexInLeaf += 1 }
            }
            if (ancestorStack.isEmpty()) {
                throw NoSuchElementException()
            }
            // Back up to the last saved ancestor because its entry at index is
            // the in-order successor.
            val (ancestor, index) = ancestorStack.last()
            ancestorStack.removeAt(ancestorStack.lastIndex)
            val nextIndex = index + 1
            if (nextIndex in ancestor.keys.indices) {
                ancestorStack += SavedAncestor(ancestor, nextIndex)
            }
            currentLeaf = descendLeft(ancestor.children[nextIndex])
            indexInLeaf = 0
            return ancestor.entryAt(index)
        }

        private fun descendLeft(subtreeRoot: Node<K, V>): Leaf<K, V> {
            var node = subtreeRoot
            while (node is Inner) {
                ancestorStack += SavedAncestor(node, 0)
                node = node.children[0]
            }
            return node as Leaf
        }

        private var currentLeaf: Leaf<K, V>
        private var indexInLeaf = 0
        private val ancestorStack = ArrayList<SavedAncestor<K, V>>(@Suppress("MagicNumber")/* initialCapacity = */ 8)

        init {
            currentLeaf = descendLeft(treeRoot)
        }

        private data class SavedAncestor<K : Comparable<K>, V>(val node: Inner<K, V>, val index: Int)
    }
}

private const val MAX_ELEMENTS_PER_NODE = 45
private const val MIN_ELEMENTS_PER_NODE = MAX_ELEMENTS_PER_NODE / 2

private sealed class Node<K : Comparable<K>, V>(
    val keys: Array<out Comparable<K>>, val values: Array<out Any?>
) : MaybeSplit<K, V>() {
    init {
        assert(keys.size == values.size)
    }

    override fun unpackIntoNode(): Node<K, V> = this

    @Suppress("Unchecked_Cast")
    fun entryAt(index: Int): Pair<K, V> = Pair(keys[index] as K, values[index] as V)

    abstract fun withChangedValue(index: Int, value: V): Node<K, V>
    abstract fun split(): Split<K, V>

    // Note: rightSibling must be of the same type.
    abstract fun mergeWith(key: K, value: V, rightSibling: Node<K, V>): Node<K, V>
}

private class Leaf<K : Comparable<K>, V>(
    keys: Array<out Comparable<K>>, values: Array<out Any?>
) : Node<K, V>(keys, values) {
    override fun withChangedValue(index: Int, value: V): Leaf<K, V> = Leaf(keys, values.update(index, value))

    override fun split(): Split<K, V> {
        val size = keys.size
        val halfSize = size / 2
        val leftKeys = keys.copyOfRange(0, halfSize)
        val leftValues = values.copyOfRange(0, halfSize)
        val (middleKey, middleValue) = entryAt(halfSize)
        val rightKeys = keys.copyOfRange(halfSize + 1, size)
        val rightValues = values.copyOfRange(halfSize + 1, size)
        return Split(
            Leaf(leftKeys, leftValues),
            middleKey, middleValue,
            Leaf(rightKeys, rightValues)
        )
    }

    override fun mergeWith(key: K, value: V, rightSibling: Node<K, V>): Leaf<K, V> {
        rightSibling as Leaf
        return Leaf(
            keys.mergeWithSeparator(key, rightSibling.keys),
            values.mergeWithSeparator(value, rightSibling.values)
        )
    }

    fun withAddedEntry(index: Int, key: K, value: V): Leaf<K, V> = Leaf(
        keys.insert(index, key),
        values.insert(index, value)
    )

    fun withRemovedEntry(index: Int): Leaf<K, V> = Leaf(keys.remove(index), values.remove(index))
}

@Suppress("Unchecked_Cast")
private fun <K : Comparable<K>, V> getSharedEmptyLeaf(): Leaf<K, V> = sharedEmptyLeaf as Leaf<K, V>

private val sharedEmptyLeaf = Leaf<Nothing, Nothing>(emptyArray<Comparable<*>>(), emptyArray())

private class Inner<K : Comparable<K>, V>(
    keys: Array<out Comparable<K>>, values: Array<out Any?>, val children: Array<out Node<K, V>>
) : Node<K, V>(keys, values) {
    init {
        assert(keys.size == children.size - 1)
    }

    override fun withChangedValue(index: Int, value: V): Inner<K, V> =
        Inner(keys, values.update(index, value), children)

    override fun split(): Split<K, V> {
        val size = keys.size
        val halfSize = size / 2
        val leftKeys = keys.copyOfRange(0, halfSize)
        val leftValues = values.copyOfRange(0, halfSize)
        val leftChildren = children.copyOfRange(0, halfSize + 1)
        assert(leftChildren.size == leftKeys.size + 1)
        val (middleKey, middleValue) = entryAt(halfSize)
        val rightKeys = keys.copyOfRange(halfSize + 1, size)
        val rightValues = values.copyOfRange(halfSize + 1, size)
        val rightChildren = children.copyOfRange(halfSize + 1, size + 1)
        assert(rightChildren.size == rightKeys.size + 1)
        return Split(
            Inner(leftKeys, leftValues, leftChildren),
            middleKey, middleValue,
            Inner(rightKeys, rightValues, rightChildren)
        )
    }

    @Suppress("Unchecked_Cast")
    override fun mergeWith(key: K, value: V, rightSibling: Node<K, V>): Inner<K, V> {
        rightSibling as Inner
        return Inner(
            keys.mergeWithSeparator(key, rightSibling.keys),
            values.mergeWithSeparator(value, rightSibling.values),
            // Overload resolution gets confused if this cast is not present.
            (children as Array<Node<K, V>>) + rightSibling.children
        )
    }

    fun withChangedChild(index: Int, child: Node<K, V>): Inner<K, V> =
        Inner(keys, values, children.update(index, child))

    fun withAddedChild(index: Int, split: Split<K, V>): Inner<K, V> {
        val newKeys = keys.insert(index, split.separatorKey)
        val newValues = values.insert(index, split.separatorValue)
        val newChildren = children.insert(index, split.leftNode)
        newChildren[index + 1] = split.rightNode
        return Inner(newKeys, newValues, newChildren)
    }

    fun withChangedEntry(index: Int, key: K, value: V): Inner<K, V> = Inner(
        keys.update(index, key),
        values.update(index, value),
        children
    )

    fun withRotatedChildren(index: Int, split: Split<K, V>): Inner<K, V> {
        val newKeys = keys.update(index, split.separatorKey)
        val newValues = values.update(index, split.separatorValue)
        val newChildren = children.update(index, split.leftNode)
        newChildren[index + 1] = split.rightNode
        return Inner(newKeys, newValues, newChildren)
    }

    fun withMergedChildren(index: Int, mergedChild: Node<K, V>): Node<K, V> {
        val newKeys = keys.remove(index)
        // If there are no more entries, we can just return the child
        // directly, making the tree one level shallower.
        if (newKeys.isEmpty()) return mergedChild
        val newValues = values.remove(index)
        val newChildren = children.remove(index + 1)
        newChildren[index] = mergedChild
        return Inner(newKeys, newValues, newChildren)
    }
}

private sealed class MaybeSplit<K : Comparable<K>, V> {
    abstract fun unpackIntoNode(): Node<K, V>
}

private class Split<K : Comparable<K>, V>(
    val leftNode: Node<K, V>, val separatorKey: K, val separatorValue: V, val rightNode: Node<K, V>
) : MaybeSplit<K, V>() {
    override fun unpackIntoNode(): Node<K, V> = Inner(
        arrayOf<Comparable<K>>(separatorKey),
        arrayOf<Any?>(separatorValue),
        arrayOf(leftNode, rightNode)
    )
}

private data class FirstNotSmaller(val index: Int, val isEqual: Boolean)

/**
 * Find the index of the first entry not smaller than the given [key].
 *
 * The first component of the returned pair is the index of the element, or
 * the size of [keys] if not found.

 * The second component is true if and only if the element at that index is
 * equal to [key].
 */
private fun <K : Comparable<K>> findFirstNotSmallerThan(keys: Array<out Comparable<K>>, key: K): FirstNotSmaller {
    val index = keys.binarySearch(key)
    return when {
        index >= 0 -> FirstNotSmaller(index, true)
        else -> FirstNotSmaller(index.inv(), false)
    }
}

private inline fun <reified T> Array<out T>.insert(index: Int, element: T): Array<T> {
    val newArray = arrayOfNulls<T>(size + 1)
    return copyInsertingInto(newArray, index, element)
}

private fun <T> Array<out T>.copyInsertingInto(newArray: Array<T?>, index: Int, element: T): Array<T> {
    copyInto(newArray, destinationOffset = 0, startIndex = 0, endIndex = index)
    copyInto(newArray, destinationOffset = index + 1, startIndex = index, endIndex = size)
    newArray[index] = element
    // We know that this array cannot have any nulls anymore.
    @Suppress("Unchecked_Cast")
    return newArray as Array<T>
}

private inline fun <reified T> Array<out T>.remove(index: Int): Array<T> {
    val newArray = arrayOfNulls<T>(size - 1)
    return copyRemovingInto(newArray, index)
}

private fun <T> Array<out T>.copyRemovingInto(newArray: Array<T?>, index: Int): Array<T> {
    copyInto(newArray, destinationOffset = 0, startIndex = 0, endIndex = index)
    copyInto(newArray, destinationOffset = index, startIndex = index + 1, endIndex = size)
    // We know that this array cannot have any nulls anymore.
    @Suppress("Unchecked_Cast")
    return newArray as Array<T>
}

private fun <T> Array<out T>.update(index: Int, element: T): Array<T> {
    @Suppress("Unchecked_Cast")
    val newArray = copyOf() as Array<T>
    newArray[index] = element
    return newArray
}

@Suppress("Unchecked_Cast")
private fun <T> Array<out T>.mergeWithSeparator(separator: T, right: Array<out T>): Array<T> {
    val newArray = (this as Array<T>).copyOf(size + right.size + 1)
    right.copyInto(newArray, destinationOffset = size + 1, startIndex = 0, endIndex = right.size)
    newArray[size] = separator
    // We know that this array cannot have any nulls anymore.
    return newArray as Array<T>
}

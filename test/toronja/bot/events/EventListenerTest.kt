// Copyright (C) 2019 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package toronja.bot.events

import io.mockk.every
import io.mockk.mockk
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.events.Event
import net.dv8tion.jda.api.events.guild.member.GenericGuildMemberEvent
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent
import net.dv8tion.jda.api.events.guild.member.GuildMemberRemoveEvent
import java.util.concurrent.Executor
import kotlin.test.Test
import kotlin.test.assertEquals

class EventListenerTest {
    @Test
    fun `invokes all matching observers`() {
        var counter = 0
        val listener = EventListener(ImmediateExecutor)
        listener.subscribe<GuildMemberJoinEvent> { counter += 1 }
        listener.subscribe<GenericGuildMemberEvent> { counter += 1 }
        listener.subscribe<Event> { counter += 1 }
        listener.handle(GuildMemberJoinEvent(mockJda, 0, mockMember))
        assertEquals(3, counter)
    }

    @Test
    fun `does not invoke non-matching observers`() {
        var counter = 0
        val listener = EventListener(ImmediateExecutor)
        listener.subscribe<GuildMemberJoinEvent> { counter += 1 }
        listener.subscribe<GuildMemberRemoveEvent> { counter += 1 }
        listener.handle(GuildMemberJoinEvent(mockJda, 0, mockMember))
        assertEquals(1, counter)
    }

    @Test
    fun `does not invoke filtered out events`() {
        var counter = 0
        val listener = EventListener(ImmediateExecutor)
        listener.subscribeFiltered<GuildMemberJoinEvent>({ false }) { counter += 1 }
        listener.subscribeFiltered<GenericGuildMemberEvent>({ true }) { counter += 1 }
        listener.handle(GuildMemberJoinEvent(mockJda, 0, mockMember))
        assertEquals(1, counter)
    }

    @Test
    fun `invokes observers through executor`() {
        var counter = 0
        val listener = EventListener(NullExecutor)
        listener.subscribe<GuildMemberJoinEvent> { counter += 1 }
        listener.handle(GuildMemberJoinEvent(mockJda, 0, mockMember))
        assertEquals(0, counter)
    }

    @Test
    fun `invokes shouldCallHandler through executor`() {
        var counter = 0
        val listener = EventListener(NullExecutor)
        listener.subscribeFiltered<Event>({ true.also { counter += 1 } }) { counter += 1 }
        listener.handle(GuildMemberJoinEvent(mockJda, 0, mockMember))
        assertEquals(0, counter)
    }

    @Test
    fun `invokes shouldHandleFastPath on the calling context`() {
        var counter = 0
        val listener = EventListener(NullExecutor)
        listener.subscribe(object : EventFilter<GuildMemberJoinEvent> {
            override fun shouldHandleFastPath(event: GuildMemberJoinEvent): Boolean? = false.also { counter += 1 }
            override fun shouldHandle(event: GuildMemberJoinEvent): Boolean = false
        }) { counter += 1 }
        listener.handle(GuildMemberJoinEvent(mockJda, 0, mockMember))
        assertEquals(1, counter)
    }

    private object ImmediateExecutor : Executor {
        override fun execute(command: Runnable) = command.run()
    }

    private object NullExecutor : Executor {
        override fun execute(command: Runnable) {}
    }

    private inline fun <reified E : Event> EventListener.subscribeFiltered(
        crossinline filter: (E) -> Boolean, noinline function: (E) -> Unit
    ) {
        subscribe(
            object : EventFilter<E> {
                override fun shouldHandleFastPath(event: E): Boolean? = null
                override fun shouldHandle(event: E): Boolean = filter(event)
            },
            function
        )
    }

    private companion object {
        val mockJda = mockk<JDA>()
        val mockMember = mockk<Member>()
        val mockGuild = mockk<Guild>()

        init {
            every { mockMember.guild } returns mockGuild
        }
    }
}

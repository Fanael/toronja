// Copyright (C) 2019 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.modules

import net.dv8tion.jda.api.entities.Guild
import toronja.bot.db.ConnectionPool
import toronja.bot.modules.base.ModuleStateChanged
import toronja.bot.modules.base.ToggleableModule
import toronja.bot.modules.base.ToggleableModuleFactory
import toronja.utils.Snowflake

/**
 * A dummy toggleable module implementation that always "yes" to everything,
 * to simplify the test code by eliminating the database-level setup required
 * for an actual PersistedModule.
 */
internal class DummyToggleableModule(override val moduleName: String) : ToggleableModule {
    override fun getCachedState(guild: Snowflake<Guild>): Boolean? = true

    override fun isEnabledIn(guild: Snowflake<Guild>): Boolean = true

    override suspend fun enableIn(guild: Guild): ModuleStateChanged = ModuleStateChanged.YES

    override suspend fun disableIn(guild: Guild): ModuleStateChanged = ModuleStateChanged.YES

    object Factory : ToggleableModuleFactory {
        override fun invoke(pool: ConnectionPool, moduleName: String): DummyToggleableModule =
            DummyToggleableModule(moduleName)
    }
}

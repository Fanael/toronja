// Copyright (C) 2019 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.

package toronja.bot.modules

import kotlinx.coroutines.runBlocking
import net.dv8tion.jda.api.entities.Guild
import toronja.bot.db.TestDatabase
import toronja.bot.db.request
import toronja.utils.Snowflake
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.assertTrue

class LibraryTest {
    @BeforeTest
    fun prepareLibraryState() {
        runBlocking {
            TestDatabase.pool.write(request<Unit>({ "ClearLibrary" }) { connection ->
                connection.prepare("DELETE FROM library_topics").executeUpdate()
                connection.prepare("DELETE FROM library_categories").executeUpdate()
            })
            library.defineCategory(FIRST_GUILD, FIRST_CATEGORY_NAME)
            library.defineCategory(FIRST_GUILD, SECOND_CATEGORY_NAME)
            library.defineCategory(SECOND_GUILD, FIRST_CATEGORY_NAME)
            library.updateTopic(FIRST_GUILD, FIRST_CATEGORY_NAME, FIRST_TOPIC_NAME, LOREM_IPSUM)
            library.updateTopic(FIRST_GUILD, FIRST_CATEGORY_NAME, SECOND_TOPIC_NAME, "")
        }
    }

    @Test
    fun `lists category names correctly`() {
        assertEqualElements(listOf(FIRST_CATEGORY_NAME, SECOND_CATEGORY_NAME), library.getCategoryNames(FIRST_GUILD))
        assertEqualElements(listOf(FIRST_CATEGORY_NAME), library.getCategoryNames(SECOND_GUILD))
        assertEqualElements(emptyList(), library.getCategoryNames(THIRD_GUILD))
    }

    @Test
    fun `lists topic names correctly`() {
        assertEqualElements(
            listOf(FIRST_TOPIC_NAME, SECOND_TOPIC_NAME),
            listTopicNamesOrNull(FIRST_GUILD, FIRST_CATEGORY_NAME)!!
        )
        assertEqualElements(listOf(), listTopicNamesOrNull(FIRST_GUILD, SECOND_CATEGORY_NAME)!!)
        assertEqualElements(listOf(), listTopicNamesOrNull(SECOND_GUILD, FIRST_CATEGORY_NAME)!!)
        assertNull(listTopicNamesOrNull(SECOND_GUILD, SECOND_CATEGORY_NAME))
        assertNull(listTopicNamesOrNull(THIRD_GUILD, FIRST_CATEGORY_NAME))
    }

    @Test
    fun `retrieves topics correctly`() {
        assertEquals(
            Library.TopicResult.Result(LOREM_IPSUM),
            library.getTopic(FIRST_GUILD, FIRST_CATEGORY_NAME, FIRST_TOPIC_NAME)
        )
        assertEquals(
            Library.TopicResult.Result(""),
            library.getTopic(FIRST_GUILD, FIRST_CATEGORY_NAME, SECOND_TOPIC_NAME)
        )
        assertEquals(
            Library.TopicResult.Result(null),
            library.getTopic(SECOND_GUILD, FIRST_CATEGORY_NAME, FIRST_TOPIC_NAME)
        )
        assertEquals(
            Library.TopicResult.NoSuchCategory,
            library.getTopic(THIRD_GUILD, FIRST_CATEGORY_NAME, FIRST_TOPIC_NAME)
        )
    }

    @Test
    fun `removing topics works`() {
        assertEquals(
            Library.TopicResult.Result(true),
            runBlocking { library.removeTopic(FIRST_GUILD, FIRST_CATEGORY_NAME, SECOND_TOPIC_NAME) }
        )
        assertEqualElements(listOf(FIRST_TOPIC_NAME), listTopicNamesOrNull(FIRST_GUILD, FIRST_CATEGORY_NAME)!!)
    }

    @Test
    fun `renaming topics works`() {
        val newName = "qux"
        assertEquals(
            Library.TopicResult.Result(true),
            runBlocking { library.renameTopic(FIRST_GUILD, FIRST_CATEGORY_NAME, FIRST_TOPIC_NAME, newName) }
        )
        assertEqualElements(
            listOf(SECOND_TOPIC_NAME, newName),
            listTopicNamesOrNull(FIRST_GUILD, FIRST_CATEGORY_NAME)!!
        )
    }

    @Test
    fun `removing categories works`() {
        assertTrue(runBlocking { library.removeCategory(SECOND_GUILD, FIRST_CATEGORY_NAME) })
        assertNull(listTopicNamesOrNull(SECOND_GUILD, FIRST_CATEGORY_NAME))
    }

    @Test
    fun `renaming categories works`() {
        assertTrue(runBlocking { library.renameCategory(SECOND_GUILD, FIRST_CATEGORY_NAME, SECOND_CATEGORY_NAME) })
        assertNotNull(listTopicNamesOrNull(SECOND_GUILD, SECOND_CATEGORY_NAME))
    }

    private val library = Library(TestDatabase.pool, DummyToggleableModule.Factory)

    private fun listTopicNamesOrNull(guild: Snowflake<Guild>, categoryName: String): List<String>? =
        when (val result = library.getTopicNamesInCategory(guild, categoryName)) {
            is Library.TopicResult.NoSuchCategory -> null
            is Library.TopicResult.Result -> result.value
        }
}

private fun <T> assertEqualElements(expected: Iterable<T>, actual: Iterable<T>) {
    fun Iterable<T>.countOccurrences(): Map<T, Int> = groupingBy { it }.eachCount()

    assertEquals(expected.countOccurrences(), actual.countOccurrences())
}

private val FIRST_GUILD = Snowflake<Guild>(1)
private val SECOND_GUILD = Snowflake<Guild>(2)
private val THIRD_GUILD = Snowflake<Guild>(3)
private const val FIRST_CATEGORY_NAME = "foo"
private const val SECOND_CATEGORY_NAME = "bar"
private const val FIRST_TOPIC_NAME = "foo"
private const val SECOND_TOPIC_NAME = "bar"
private const val LOREM_IPSUM = "Lorem ipsum dolor sit amet."

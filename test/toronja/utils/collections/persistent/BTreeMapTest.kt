// Copyright (C) 2019 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package toronja.utils.collections.persistent

import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotSame
import kotlin.test.assertNull
import kotlin.test.assertSame
import kotlin.test.assertTrue
import kotlin.test.asserter

class BTreeMapTest {
    @Test
    fun `can retrieve inserted data`() {
        val map = (MAP_SIZE downTo 1).fold(BTreeMap<Int, Int>()) { m, i -> m + Pair(i, i * 10) }
        for (i in 1..MAP_SIZE) {
            assertEquals(i * 10, map[i])
        }
    }

    @Test
    fun `retrieving nonexistent keys returns null`() {
        var map = BTreeMap<Int, Int>()
        assertEquals(null, map[0])
        map += Pair(0, 5)
        map += Pair(2, 10)
        assertEquals(null, map[1])
    }

    @Test
    fun `insertion is persistent`() {
        val original = BTreeMap<Int, Int>()
        val modified = original + Pair(1, 1)
        assertEquals(null, original[1])
        assertEquals(1, modified[1])
        assertNotSame(modified, original)
    }

    @Test
    fun `adding existing key overwrites`() {
        val map = (1..MAP_SIZE).fold(BTreeMap<Int, Int>()) { m, i -> m + Pair(i, i) }.let { tree ->
            (MAP_SIZE downTo 1).fold(tree) { m, i -> m + Pair(i, i * 2) }
        }
        for (i in 1..MAP_SIZE) {
            assertEquals(i * 2, map[i])
        }
    }

    @Test
    fun `overwriting is persistent`() {
        val original = (1..MAP_SIZE).fold(BTreeMap<Int, Int>()) { map, i -> map + Pair(i, i) }
        val modified = (1..MAP_SIZE).fold(original) { map, i -> map + Pair(i, i * 10) }
        assertNotSame(modified, original)
        for (i in 1..MAP_SIZE) {
            assertEquals(i, original[i])
        }
        for (i in 1..MAP_SIZE) {
            assertEquals(i * 10, modified[i])
        }
    }

    @Test
    fun `can handle random inserts`() {
        val ints = (1..MAP_SIZE).shuffled(Random.Default)
        val map = ints.fold(BTreeMap<Int, Int>()) { m, i -> m + Pair(i, i) }
        for (i in 1..MAP_SIZE) {
            assertEquals(i, map[i])
        }
    }

    @Test
    fun `can handle random deletions`() {
        val ints = (1..MAP_SIZE).shuffled(Random.Default)
        val original = ints.fold(BTreeMap<Int, Int>()) { m, i -> m + Pair(i, i) }
        val modified = ints.shuffled(Random.Default).fold(original, BTreeMap<Int, Int>::minus)
        assertTrue(modified.isEmpty())
    }

    @Test
    fun `deleting nonexistent keys returns original unchanged`() {
        val original = (1..MAP_SIZE).fold(BTreeMap<Int, Int>()) { map, i -> map + Pair(i, i) }
        val modified = (MAP_SIZE + 1..MAP_SIZE * 2).fold(original, BTreeMap<Int, Int>::minus)
        assertSame(original, modified)
    }

    @Test
    fun `deletion is persistent`() {
        val original = (1..MAP_SIZE).fold(BTreeMap<Int, Int>()) { map, i -> map + Pair(i, i) }
        val modified = (1..MAP_SIZE step 2).fold(original, BTreeMap<Int, Int>::minus)
        assertNotSame(original, modified)
        for (i in 1..MAP_SIZE) {
            assertEquals(i, original[i])
        }
        for (i in 1..MAP_SIZE step 2) {
            assertNull(modified[i])
        }
        for (i in 2..MAP_SIZE step 2) {
            assertEquals(i, modified[i])
        }
    }

    @Test
    fun `iteration lists all elements`() {
        val map = (1..MAP_SIZE).shuffled(Random.Default).fold(BTreeMap<Int, Int>()) { m, i -> m + Pair(i, i * 5) }
        val elements = map.toList().sortedBy(Pair<Int, Int>::first)
        for ((i, pair) in (1..MAP_SIZE).zip(elements)) {
            assertEquals(i, pair.first)
            assertEquals(i * 5, pair.second)
        }
    }

    @Test
    fun `iteration is in key order`() {
        (1..MAP_SIZE)
            .shuffled(Random.Default)
            .fold(BTreeMap<Int, Int>()) { map, i -> map + Pair(i, i) }
            .asSequence()
            .zipWithNext { (x, _), (y, _) -> asserter.assertTrue({ "$x < $y is false" }, x < y) }
            .last()
    }
}

private const val MAP_SIZE = 3000
